﻿/*
===========================================================================

  Copyright (c) 2010-2014 Darkstar Dev Teams

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

  This file is part of DarkStar-server source code.

===========================================================================
*/

#ifndef _STATUSEFFECTCONTAINER_H
#define _STATUSEFFECTCONTAINER_H

#include "../common/cbasetypes.h"
#include "../common/taskmgr.h"

#include "status_effect.h"
#include "entities/battleentity.h"

/************************************************************************
*                                                                       *
*  Status Effect Container                                              *
*                                                                       *
************************************************************************/

class CBattleEntity;

class CStatusEffectContainer
{
public:

	uint64	m_Flags;											// биты переполнения байтов m_StatusIcons (по два бита на каждый эффект) Overflow bits bytes m_StatusIcons (two bits for each effect)
    uint8 m_StatusIcons[32];                                    // иконки статус-эффектов Icon status effects

    bool ApplyBardEffect(CStatusEffect* PStatusEffect, uint8 maxSongs);     // Returns true if you can apply a Bard Song
    bool CanGainStatusEffect(EFFECT statusEffect, uint16 power);            // Returns true if the status effect will take effect
    bool AddStatusEffect(CStatusEffect* StatusEffect, bool silent = false); // Add a status effect without the message
    bool DelStatusEffect(EFFECT StatusID);                                  // Delete status effect by ID
    bool DelStatusEffectSilent(EFFECT StatusID);                            // Delete status effect by ID without the message
    bool DelStatusEffect(EFFECT StatusID, uint16 SubID);                    // Delete status effect by ID and SubID
    void DelStatusEffectsByFlag(uint16 flag, bool silent = false);          // удаляем все эффекты с указанным типом Remove all the effects of a specified type
    void DelStatusEffectsByIcon(uint16 IconID);                             // удаляем все эффекты с указанной иконкой Remove all the effects of a specified icon
    void DelStatusEffectsByType(uint16 Type);                               // Remove all the effects of a specified type
    bool DelStatusEffectByTier(EFFECT StatusID, uint16 power);              // Remove status effects by specified Tier
    void KillAllStatusEffect();                                             // Remove ALL status effects

    bool HasStatusEffect(EFFECT StatusID);                                  // проверяем наличие эффекта Check for the status effect by ID
    bool HasStatusEffect(EFFECT StatusID, uint16 SubID);                    // проверяем наличие эффекта с уникальным subid Check for the status effect by unique SubID
    bool HasStatusEffectByFlag(uint16 flag);                                // Check for status effects with the specified flag

    EFFECT EraseStatusEffect();                                 // удаляем первый отрицательный эффект Removes the first negative effect
    EFFECT HealingWaltz();                                      // Dancers healing waltz
    uint8 EraseAllStatusEffect();                               // Erases all status effects
    EFFECT DispelStatusEffect(EFFECTFLAG flag);                 // Removes the first positive effect
    uint8 DispelAllStatusEffect(EFFECTFLAG flag);               // Dispels all status effects
    CStatusEffect* StealStatusEffect();                         // Dispels one effect and returns it
    CStatusEffect* CopyStatusEffect();                          // Copy one beneficial effect and returns it

    CStatusEffect* GetStatusEffect(EFFECT StatusID);               // Return status effect by ID
    CStatusEffect* GetStatusEffect(EFFECT StatusID, uint32 SubID); // Return status effect by ID and SubID

    void UpdateStatusIcons();                                      // пересчитываем иконки эффектов Recalculate status effects
    void CheckEffects(uint32 tick);                                // Check status effect OnTick action
    void CheckRegen(uint32 tick);                                  // Check Regen OnTick

    void LoadStatusEffects();                                      // загружаем эффекты персонажа Load the status effects of the character
    void SaveStatusEffects();                                      // сохраняем эффекты персонажа Save the status effects of the character

    uint8 GetEffectsCount(uint16 SubID);                           // получаем количество эффектов с указанным subid Get the number of effects with this subid

    bool ApplyCorsairEffect(CStatusEffect* PStatusEffect, uint8 maxRolls, uint8 bustDuration); // Apply a Corsair Roll
    bool CheckForElevenRoll();                                                                 // Check for Roll of 11 with Corsair
    bool HasBustEffect(uint16 id);                                                             // Check for Corsair Bust effect
    bool HasCorsairEffect(uint32 charid);                                                      // Check for active Corsair Roll effects
    void Fold(uint32 charid);                                                                  // Erases 1 Roll or Bust effect

    void WakeUp();                 // remove sleep effects
    bool IsAsleep();               // Returns true if asleep
    bool HasPreventActionEffect(); // checks if owner has an effect that prevents actions, like stun, petrify, sleep etc

	CStatusEffectContainer(CBattleEntity* PEntity);
	~CStatusEffectContainer();

private:

	CBattleEntity* m_POwner;

    // void ReplaceStatusEffect(EFFECT effect);              // This needs to be implemented
	void RemoveStatusEffect(uint32 id, bool silent = false); // Remove the effect on its ID
	void SetEffectParams(CStatusEffect* StatusEffect);       // Set the parameters of the status effect

    void OverwriteStatusEffect(CStatusEffect* StatusEffect); // Overwrite a status effect with another

	uint32 m_EffectCheckTime;                                // Time to check for OnTick effect
	uint32 m_RegenCheckTime;                                 // Time to check for OnTick Regen

	std::vector<CStatusEffect*>	m_StatusEffectList;
};

/************************************************************************
*                                                                       *
*                                                                       *
*                                                                       *
************************************************************************/

namespace effects
{
    void LoadEffectsParameters();           // Load the parameters of a status effect
    uint16 GetEffectElement(uint16 effect); // Return the element of the status effect
};

#endif

