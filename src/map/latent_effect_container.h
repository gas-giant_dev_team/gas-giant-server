﻿/*
===========================================================================

  Copyright (c) 2010-2014 Darkstar Dev Teams

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

  This file is part of DarkStar-server source code.

===========================================================================
*/

#ifndef _LATENTEFFECTCONTAINER_H
#define _LATENTEFFECTCONTAINER_H

#include "../common/cbasetypes.h"
#include "../common/taskmgr.h"

#include "latent_effect.h"
#include "entities/petentity.h"

/************************************************************************
*                                                                       *
*  Latent Effect Container                                              *
*                                                                       *
************************************************************************/

class CCharEntity;

class CLatentEffectContainer
{
public:

	void CheckLatentsHP(int32 hp);                            // Check latents based on HP
	void CheckLatentsTP(int16 tp);                            // Check latents based on TP
	void CheckLatentsMP(int32 mp);                            // Check latents based on MP
	void CheckLatentsEquip(uint8 slot);                       // Check latents based on Equipment
	void CheckLatentsWeaponDraw(bool drawn);                  // Check latents based on having a weapon drawn
	void CheckLatentsStatusEffect();                          // Check latents based on Status Effects
	void CheckLatentsFoodEffect();                            // Check latents based on Food Effects
	void CheckLatentsRollSong(bool active);                   // Check latents based on Songs or Rolls active
	void CheckLatentsDay();                                   // Check latents based on Vana'diel Day
	void CheckLatentsMoonPhase();                             // Check latents based on Moon Phase
	void CheckLatentsHours();                                 // Check latents based on HP
	void CheckLatentsWeekDay();                               // Check latents based on Vana'diel day of the week
	void CheckLatentsPartyMembers(uint8 members);             // Check latents based on # of Party Members
	void CheckLatentsPartyJobs();                             // Check latents based on Party Member's Jobs
	void CheckLatentsPartyAvatar();                           // Check latents based on Avatars in the party
	void CheckLatentsJobLevel();                              // Check latents based on Job Level
	void CheckLatentsPetType(PETTYPE petID);                  // Check latents based on Pet present
	void CheckLatentsTime();                                  // Check latents based on Vana'diel time of day
	void CheckLatentsWeaponBreak(uint8 slot);                 // Check latents based on Weapon Points
	void CheckLatentsZone();                                  // Check latents based on Zone
	void CheckLatentsMulti();                                 // Check latents based on Multiple Conditions

	void AddLatentEffect(CLatentEffect* LatentEffect);        // Add the latent effect
	void AddLatentEffects(std::vector<CLatentEffect*> *latentList, uint8 reqLvl, uint8 slot);
	void DelLatentEffects(uint8 reqLvl, uint8 slot);          // Remove the latent effect

    CLatentEffect* GetLatentEffect(uint8 slot, uint16 modId); // Return the latent effect

	 CLatentEffectContainer(CCharEntity* PEntity);
	~CLatentEffectContainer();

private:

	CCharEntity* m_POwner;

	std::vector<CLatentEffect*>	m_LatentEffectList;
};

#endif

