﻿/*
===========================================================================

  Copyright (c) 2010-2014 Darkstar Dev Teams

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

  This file is part of DarkStar-server source code.

===========================================================================
*/

#ifndef _CITEMCONTAINER_H
#define _CITEMCONTAINER_H

#include "../common/cbasetypes.h"

enum CONTAINER_ID
{
	LOC_INVENTORY		= 0,
	LOC_MOGSAFE			= 1,
	LOC_STORAGE			= 2,
	LOC_TEMPITEMS		= 3,
	LOC_MOGLOCKER		= 4,
	LOC_MOGSATCHEL		= 5,
	LOC_MOGSACK			= 6,
	LOC_MOGCASE			= 7,
    LOC_WARDROBE        = 8
};

#define MAX_CONTAINER_ID	  9
#define MAX_CONTAINER_SIZE	120
#define ERROR_SLOTID		255

/************************************************************************
*																		*
*																		*
*																		*
************************************************************************/

class CItem;

class CItemContainer
{
public:

	CItemContainer(uint16 LocationID);
   ~CItemContainer();

	uint16	GetID();
    uint16  GetBuff();                              // The planned amount of storage (unlimited size)
	uint8	GetSize();
	uint8	GetFreeSlotsCount();					// Amount of free space in storage
    uint8   AddBuff(int8 buff);                     // The planned amount of storage (unlimited size)
    uint8   AddSize(int8 size);                     // Increase / decrease the size of the container
	uint8	SetSize(uint8 size);
	uint8	SearchItem(uint16 ItemID);				// Find the items in the store
    uint8   SearchItemWithSpace(uint16 ItemID, uint32 quantity); // Search for item that has space to accomodate x items added

	uint8	InsertItem(CItem* PItem);				// Add a previously created object in free cell
	uint8	InsertItem(CItem* PItem, uint8 slotID);	// Add a previously created object to the selected cell

    uint32  SortingPacket;                          // Number of requests for sorting per cycle
    uint32  LastSortingTime;                        // The last sorting container

	CItem*	GetItem(uint8 slotID);					// Get a pointer to the object at the specified cell. 
	void	Clear();								// Remove all items from container

private:

	uint16	m_id;
    uint16  m_buff;
	uint8	m_size;
    uint8   m_count;

	CItem*	m_ItemList[MAX_CONTAINER_SIZE+1];
};

#endif