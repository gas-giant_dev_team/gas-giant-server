﻿/*
===========================================================================

  Copyright (c) 2010-2014 Darkstar Dev Teams

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

  This file is part of DarkStar-server source code.

===========================================================================
*/

#ifndef _CENMITYCONTAINER_H
#define _CENMITYCONTAINER_H

#include "../common/cbasetypes.h"
#include <map>

struct EnmityObject_t
{
	CBattleEntity* PEnmityOwner;	// Enmity Target
	int16 CE;						// Cumulative Enmity
	int16 VE;						// Volatile Enmity
	uint8 maxTH;                    // Maximum Treasure Hunter level of this Enmity Owner
};

typedef std::map<uint32,EnmityObject_t*> EnmityList_t;

class CBattleEntity;
class CCharEntity;

class CEnmityContainer
{
public:

    CEnmityContainer(CBattleEntity* holder);
   ~CEnmityContainer();

    CBattleEntity*	GetHighestEnmity();			// Decays VE and gets target with highest enmity

	float   CalculateEnmityBonus(CBattleEntity* PEntity);
	void	Clear(uint32 EntityID = 0);			// Removes Entries from list
    void	AddBaseEnmity(CBattleEntity* PEntity);                                                       // Add enmity to the entity
	void	UpdateEnmity(CBattleEntity* PEntity, int16 CE, int16 VE, bool withMaster = true);            // Update the enmity list
	void	UpdateEnmityFromDamage(CBattleEntity* PEntity, uint16 Damage);                               // Add enmity from damage
	void	UpdateEnmityFromCure(CBattleEntity* PEntity, uint16 level, uint16 CureAmount, bool isCureV); // Add enmity from Cure magic
	void	UpdateEnmityFromAttack(CBattleEntity* PEntity,uint16 Damage);                                // Add enmity from an attack
    void    AddLinkEnmity(CBattleEntity* PEntity);                                                       // Add enmity from linking mobs
	void	AddPartyEnmity(CCharEntity* PChar);                                                          // Add enmity from entire party
	bool    HasTargetID(uint32 TargetID); // True if ID is in the container with non-zero enmity level
	void    LowerEnmityByPercent(CBattleEntity* PEntity, uint8 percent, CBattleEntity* HateReceiver); // Lower % of hate or transfer it
	void	DecayEnmity();                                                                            // Dissapate enmity
    bool    IsWithinEnmityRange(CBattleEntity* PEntity);                                              // Check range of enemy
    uint8   GetHighestTH();                                                                           // Check Highest TH value of those on MOB hatelist.
    EnmityList_t* GetEnmityList();

private:
	
	EnmityList_t	m_EnmityList;
    CBattleEntity*  m_EnmityHolder; // Usually a monster
};

#endif