/*
===========================================================================

  Copyright (c) 2010-2014 Darkstar Dev Teams

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

  This file is part of DarkStar-server source code.

===========================================================================
*/

#ifndef _LUASTATUSEFFECT_H
#define _LUASTATUSEFFECT_H

#include "../../common/cbasetypes.h"
#include "../../common/lua/lunar.h"

#include "../status_effect.h"


class CLuaStatusEffect
{
	CStatusEffect *m_PLuaStatusEffect;

public:

	static const int8 className[];
	static Lunar<CLuaStatusEffect>::Register_t methods[];

	CLuaStatusEffect(lua_State*);
	CLuaStatusEffect(CStatusEffect*);

	CStatusEffect* GetStatusEffect()const
	{
		return m_PLuaStatusEffect;
	}

	int32 getType(lua_State*);				// Return the ID of the status effect
	int32 getSubType(lua_State*);			// Return the sub ID of the status effect
	int32 getPower(lua_State*);				// Return the power of the status effect
    int32 getSubPower(lua_State*);			// Return the sub power of the status effect
    int32 getTier(lua_State*);				// Return the tier of the status effect
	int32 getDuration(lua_State*);			// Return the duration of the status effect
	int32 getStartTime(lua_State*);			// Return the start time of the status effect
    int32 getLastTick(lua_State*);			// Return the time the last tick of the status effect occured
    int32 getTimeRemaining(lua_State*);		// Return how long is left until the effect expires (miliseconds)
	int32 getTickCount(lua_State*);			// Return the number of ticks since the status effect started
    int32 getTick(lua_State*);				// Return the tick value of the status effect

    int32 setIcon(lua_State*);				// Set the icon of the status effect
	int32 setPower(lua_State*);				// Set the power of the status effect
    int32 setSubPower(lua_State*);			// Set the sub power of the status effect
    int32 setTier(lua_State*);				// Set the tier of the status effect
	int32 setDuration(lua_State*);			// Set the duration of the status effect
    int32 setTick(lua_State*);				// Set the tick count of the status effect
    int32 setStartTime(lua_State*);			// Set the start time of the status effect

    int32 resetStartTime(lua_State*);		// Set the time to restart of the status effect

	int32 addMod(lua_State*);				// Add a modifier to an entity
    int32 setFlag(lua_State*);				// Set the flag of the status effect
};

#endif