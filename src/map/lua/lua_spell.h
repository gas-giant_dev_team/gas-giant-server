/*
===========================================================================

  Copyright (c) 2010-2014 Darkstar Dev Teams

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

  This file is part of DarkStar-server source code.

===========================================================================
*/

#ifndef _LUASPELL_H
#define _LUASPELL_H

#include "../../common/cbasetypes.h"
#include "../../common/lua/lunar.h"

#include "../spell.h"


class CLuaSpell
{
	CSpell *m_PLuaSpell;
public:

	static const int8 className[];
	static Lunar<CLuaSpell>::Register_t methods[];

	CLuaSpell(lua_State*);
	CLuaSpell(CSpell*);

	CSpell* GetSpell() const
	{
		return m_PLuaSpell;
	}

    int32 setMsg(lua_State*);               // Sets the Spell's Message
	int32 setAoE(lua_State*);               //
	int32 setFlag(lua_State*);              //
	int32 setRadius(lua_State*);            //
	int32 setAnimation(lua_State*);         //
	int32 setMPCost(lua_State*);            //
    int32 canTargetEnemy(lua_State*);       // Make sure the Spell can target enemies
    int32 isAoE(lua_State*);                // Return if Spell is AoE
    int32 getTotalTargets(lua_State*);      // Return the number of targets for a Spell
    int32 getMagicBurstMessage(lua_State*); // Return the Spell's Magic Burst Message
	int32 getElement(lua_State*);           // Return the Spell's Element
	int32 getID(lua_State*);                // Return the Spell's ID
    int32 getSpellGroup(lua_State*);        // Return the Spell's Group
	int32 getFlag(lua_State*);
    int32 getSkillType(lua_State*);         // Return the Spell's Skill Type
    int32 getMPCost(lua_State*);            // Return the Spell's MP Cost
    int32 getCE(lua_State*);                // Return the Spell's CE Enmity
    int32 getVE(lua_State*);                // Return the Spell's VE Enmity
};

#endif