﻿/*
===========================================================================

  Copyright (c) 2010-2014 Darkstar Dev Teams

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

  This file is part of DarkStar-server source code.

===========================================================================
*/

#ifndef _MAP_H
#define _MAP_H

#include "../common/cbasetypes.h"

#include "../common/blowfish.h"
#include "../common/kernel.h"
#include "../common/socket.h"
#include "../common/sql.h"
#include "../common/mmo.h"
#include "../common/taskmgr.h"
#include "../common/dsprand.h"

#include <map>
#include <list>

#include "zone.h"
#include "commandhandler.h"

enum SKILLUP_STYLE
{
    NEWSTYLE_NONE  = 0,
    NEWSTYLE_PARRY = 1,
    NEWSTYLE_BLOCK = 2,
    NEWSTYLE_GUARD = 4,
    NEWSTYLE_ALL   = 7
};

/************************************************************************
*																		*
*  system options of map server											*
*  not for use common, only for system control							*
*																		*
************************************************************************/

struct map_config_t
{
	uint32 buffer_size;                 // Max size of recv buffer -> default 1800 bytes

	uint16 usMapPort;                   // Port of map server      -> xxxxx
	uint32 uiMapIp;                     // IP of map server        -> INADDR_ANY

	const int8 *mysql_host;             // MYSQL addr     -> localhost:3306
	uint16 mysql_port;                  // MYSQL port     -> 3306
	const int8 *mysql_login;            // MYSQL login    -> default root
	const int8 *mysql_password;         // MYSQL pass     -> default NULL
	const int8 *mysql_database;         // MYSQL database -> default dspdb

    string_t server_message;            // English server message
    string_t server_message_fr;         // French server message

	uint32 max_time_lastupdate;		// Max interval wait of last update player character
    int32  vanadiel_time_offset;    // смещение игрового времени относительно реального времени Displacement of playing time on the real-time
    int32  lightluggage_block;      // если значение отлично от нуля, то персонажи с lightluggage будут удаляться с сервера автоматически If the value is non-zero, then the characters with lightluggage will be deleted from the server automatically
    float  exp_rate;                // множитель получаемого опыта Multiplier experience
	float  exp_loss_rate;			// Same as exp rate but applies when player dies
	uint8  exp_party_gap_penalties;	// If 1 Party Gap Penalties will apply
	uint8  fov_party_gap_penalties;	// If 1 FOV Pages Party Gap Penalties will apply
	uint8  fov_allow_alliance;		// If 1 allow alliance to farm FoV pages
	float  exp_retain;				// Percentage of normally lost experience to retain upon death
	int8   exp_loss_level;			// Minimum main job level at which a character may lose experience points.
    bool   level_sync_enable;       // Enable/disable Level Sync
    bool   all_jobs_widescan;       // Enable/disable jobs other than BST and RNG having widescan.
    uint8  widescan_bonus;          // Extra widescan radius (adds to, not multiplies, the total).
	int8   speed_mod;				// Modifier to add to player speed
    int8   chocobo_speed_mod;       // Modifier to add to chocobo speed
	int8   mob_speed_mod;			// Modifier to add to monster speed
	float  skillup_chance_multiplier;		// Constant used in the skillup formula that has a strong effect on skill-up rates
	float  craft_chance_multiplier;			// Constant used in the crafting skill-up formula that has a strong effect on skill-up rates
	float  skillup_amount_multiplier;		// Used to increase the amount of skill gained during skill up
	float  craft_amount_multiplier;			// Used to increase the amount of skill gained during skill up
    int16  craft_break_rate;        // Reduction to break rate.
    int16  craft_hq_rate;           // Boost to HQ synth rate.
    int8   craft_skill_onfail;      // Allows same skillup on chance for failures as crafting successes have. Default is 0 (off).
    bool   craft_day_matters;       // Enable/disable Element day factor in synthesis
    bool   craft_moonphase_matters; // Enable/disable Moon phase factor in synthesis
    bool   craft_direction_matters; // Enable/disable Compass direction factor in synthesis
    float  nm_wdmg_multiplier;      // Modifies weapon base power of NMs
	float  mob_tp_multiplier;		// Multiplies the amount of TP mobs gain on any effect that would grant TP
	float  player_tp_multiplier;	// Multiplies the amount of TP players gain on any effect that would grant TP
    float  nm_hp_multiplier;        // Multiplier for max HP of NM.
	float  mob_hp_multiplier;		// Multiplier for max HP pool of mob
    float  pre60_player_hp_multiplier;  // Multiplier for max HP pool of player
    float  post60_player_hp_multiplier; // Multiplier for max HP pool of player
    float  nm_mp_multiplier;        // Multiplier for max MP of NM.
	float  mob_mp_multiplier;		// Multiplier for max MP pool of mob
	float  player_mp_multiplier;	// Multiplier for max MP pool of player
    float  pre60_player_mp_multiplier;  // Multiplier for max MP pool of player
    float  post60_player_mp_multiplier; // Multiplier for max MP pool of player
    float  sj_mp_divisor;           // Divisor to use on subjob max MP
    int8   SJ_CAP;                  // Modifies level cap of support jobs
    float  nm_stat_multiplier;      // Multiplier for str/vit/etc of NMs
    float  mob_stat_multiplier;     // Multiplier for str/vit/etc of mobs
    float  player_stat_multiplier;  // Multiplier for str/vit/etc. of NMs of player
	float  drop_rate_multiplier;	// Multiplier for drops. Has less effect on low drop % items.
    uint16 extra_drop_rate;         // Adds to drop rate. Set to 25 to add additional %2.5 drop rate on all drops. Note the decimal point.
    uint32 all_mobs_gil_bonus;      // Sets the amount of bonus gil (per level) all mobs will drop.
    uint32 max_gil_bonus;           // Maximum total bonus gil that can be dropped. Default 99999 gil.
    uint8  newstyle_skillups;       // Allows failed parries and blocks to trigger skill up chance.
    float  engage_dist_mod;         // Default is 1. Multiplies the distance that you can engage a target. Change to 3 for 3x the distance.
    float  magic_dist_mod;          // Default is 1. Multiplies the distance that you can cast magic on a target. Change to 3 for 3x the distance.
    float  ranged_dist_mod;         // Default is 1. Multiplies the distance that you can used ranged attack on a target. Change to 3 for 3x the distance.
    float  engage_delay_mod;        // Default is 1.00 This Acts as a multiplier for delay for engaging a mob.
    float  magic_movement;          // Default is 0. Allows movement while magic casting.
    int16  seal_crest_bonus;        // Increases chance of exp mobs dropping seals or crests. Set from 0 - 60.
    int8   always_seal_crest;       // Allows non exp beastman mobs to drop seals and crests. Default is 0. Set to 1 for non exp beastman mobs to drop seals and crests.
    int8   chain_on_easy;           // Allows exp chain on any exp mob
    int8   chain_bonus;             // Increases the time frame allowed to get an exp chain. Default is 1.
    float  conquest_bonus;          // Not yet implemented? - Multiplies the amount of conquest points earned on an exp mob kill. Default is 1.
    float  region_bonus;            // Not yet implemented - will be used to boost region influence earned on an exp mob kill.
    uint32 valor_points;            // Max # of Valor points - Default 50000
    float  imperial_bonus;          // Multiplies the amount of Imperial Standing earned on an exp mob kill. Default is 1.
    int8   AN_on_kill;              // Default is 0. Allows earning Allied Notes by killing beastmen in Wings of the Goddess zones.
    float  AN_kill_bonus;           // Multiplies the amount of Allied Notes earned while AN_on_kill is set to ON. Default is 1.
    int8   Battle_cap_tweak;        // Default is 0. Globally adjust the level of level capped fights.
    int8   CoP_Battle_cap;          // Default is 0. Disable/enable old lv caps on Chains of Promathia mission battles.
    int8   Other_Battle_cap;        // Default is 0. Disable/enable old lv caps on other Quest/Mission content.
	uint8  max_merit_points;		// global variable, amount of merit points players are allowed
    bool   world_wide_yell;         // Set to 1 to make /yell hit all zones instead of just cities.
    uint16 yell_cooldown;           // Minimum time between uses of yell command (in seconds).
	bool   audit_chat;              // On / Off for the chat audit functions
	bool   audit_say;
	bool   audit_shout;
	bool   audit_tell;
	bool   audit_yell;
	bool   audit_linkshell;
	bool   audit_party;
};

/************************************************************************
*																		*
*  Map's working session												*
*																		*
************************************************************************/

struct map_session_data_t
{
	uint32		client_addr;
	uint16		client_port;
	uint16		client_packet_id;			// id последнего пакета, пришедшего от клиента ID of the last packet passed to the client
	uint16		server_packet_id;			// id последнего пакета, отправленного сервером ID of the last packet sent by the server
	int8*		server_packet_data; 		// указатель на собранный пакет, который был ранее отправлен клиенту Pointer to the assembled package that was previously sent to the client
	size_t		server_packet_size;			// размер пакета, который был ранее отправлен клиенту Size of the package that was previously sent to the client
	time_t		last_update;				// Time of last packet recv
	blowfish_t  blowfish;					// Unique decypher keys
	CCharEntity *PChar;						// Game character
    bool        shuttingDown;               // Prevents double session closing

    map_session_data_t()
    {
        shuttingDown = false;
    }
};

extern map_config_t map_config;
extern uint32 map_amntplayers;
extern int32 map_fd;
extern Sql_t* SqlHandle;
extern CCommandHandler CmdHandler;

typedef std::map<uint64,map_session_data_t*> map_session_list_t;
extern map_session_list_t map_session_list;

extern inline map_session_data_t* mapsession_getbyipp(uint64 ipp);
extern inline map_session_data_t* mapsession_createsession(uint32 ip,uint16 port);

//=======================================================================

int32 recv_parse(int8 *buff,size_t* buffsize,sockaddr_in *from,map_session_data_t*);	// Main function to parse recv packets
int32 parse(int8 *buff,size_t* buffsize,sockaddr_in *from,map_session_data_t*);			// Main function parsing the packets
int32 send_parse(int8 *buff,size_t* buffsize, sockaddr_in *from,map_session_data_t*);	// Main function is building big packet

void  map_helpscreen(int32 flag);														// Map-Server Version Screen [venom]
void  map_versionscreen(int32 flag);													// Map-Server Version Screen [venom]

int32 map_config_read(const int8 *cfgName);												// Map-Server Config [venom]
int32 map_config_default();

int32 map_cleanup(uint32 tick,CTaskMgr::CTask *PTask);									// Clean up timed out players
int32 map_close_session(uint32 tick,CTaskMgr::CTask *PTask);							// завершение сессии End of session

int32 map_garbage_collect(uint32 tick, CTaskMgr::CTask* PTask);                         // LUA garbace collection

#endif //_MAP_H
