﻿/*
===========================================================================

  Copyright (c) 2010-2014 Darkstar Dev Teams

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

  This file is part of DarkStar-server source code.

===========================================================================
*/

#ifndef _CAIPETDUMMY_H
#define _CAIPETDUMMY_H

#include "../../common/cbasetypes.h"

#include "ai_general.h"

/************************************************************************
*																		*
*																		*
*																		*
************************************************************************/

#define PET_ROAM_DISTANCE 2.1f

class CPetEntity;

class CAIPetDummy : public CAIGeneral
{
public:

	virtual void CheckCurrentAction(uint32 tick);               // Check what the current action is
	virtual void WeatherChange(WEATHER weather, uint8 element); // Change the zone's weather

	CAIPetDummy(CPetEntity* PPet);

	uint16	m_MasterCommand;                                    // Used for avatars/wyverns atm
    bool  m_queueSic;

protected:
	virtual void TransitionBack(bool skipWait = false);

	CPetEntity* m_PPet;
    int16 m_skillTP;

	void preparePetAbility(CBattleEntity* PTarg);          // Prepare a pet ability
	void ActionRoaming();                                  // Roam around
	void ActionDisengage();                                // Put away weapon and diengage enemy
	void ActionEngage();                                   // Prepare for attack
	void ActionAttack();                                   // Attacking
	void ActionSleep();                                    // Sleeping state
	void ActionFall();                                     // Fall to the ground
	void ActionDeath();                                    // Death
	void ActionSpawn();                                    // Spawn
	void ActionAbilityStart();                             // Start an ability
	void ActionAbilityUsing();                             // Working on an ability
	void ActionAbilityInterrupt();                         // Interrupt an ability
	void ActionAbilityFinish();                            // Finish an ability
	bool WyvernIsHealing();                                // True if wyvern is healing

	void ActionMagicStart();                               // Start casting magic
	void ActionMagicCasting();                             // Working on casting magic
	void ActionMagicFinish();                              // Complete magic casting
	void ActionMagicInterrupt();                           // Interrupt magic casting

    void SendTooFarInterruptMessage(CBattleEntity* PTarg); // Send the message you are too far from target


private:

};

#endif
