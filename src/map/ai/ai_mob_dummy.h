﻿/*
===========================================================================

  Copyright (c) 2010-2014 Darkstar Dev Teams

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

  This file is part of DarkStar-server source code.

===========================================================================
*/

#ifndef _CAIMOBDUMMY_H
#define _CAIMOBDUMMY_H

#include "../../common/cbasetypes.h"
#include "../../common/mmo.h"

#include <vector>

#include "../entities/battleentity.h"
#include "ai_general.h"

#define MOB_SPELL_MAX_RANGE 26.8f

// mobs will deaggro if player is out of range for this long
#define MOB_DEAGGRO_TIME 25000

// time a mob is neutral after disengaging
#define MOB_NEUTRAL_TIME 10000

/************************************************************************
*																		*
*  Первая версия поведения монстров, базовая. Монстры наносят только	*
*  физический урон														*
*																		*
*  The first version of the behavior of monsters base. Monsters are     *
*  applied only physical damage                                         *
*                                                                       *
************************************************************************/

class CMobEntity;

class CAIMobDummy : public CAIGeneral
{
public:

	// Checks the mobs current action
	virtual void CheckCurrentAction(uint32 tick);

	CAIMobDummy(CMobEntity* PMob);

	// Change the weather in the zone
	virtual void WeatherChange(WEATHER weather, uint8 element);

    // checks if the given target can be aggroed by this mob
    bool CanAggroTarget(CBattleEntity* PTarget);

	// Ready a Mobskill attack
    void setMobSkillAttack(bool);

	// Return if a Mobskill is ready
    bool getMobSkillAttack();

    bool isActionQueueAttack();
	void Stun(uint32 stunTime);

    // Time of day change, update mobs
    // TODO:
    //void TOTDChange();


protected:

	virtual void TransitionBack(bool skipWait = false);

	CMobEntity* m_PMob;
	CMobSkill* m_PSpecialSkill;
	bool   m_firstSpell;            // Check if this is the first spell cast
	bool   m_checkDespawn;          // Check and despawn if i'm too far from spawn
	uint32 m_SpawnTime;             // Time since spawning
	uint32 m_LastSpecialTime;       // Time since last special move
	uint32 m_LastStunTime;          // Last time stunned
	uint32 m_StunTime;              // Stun duration
	uint32 m_DeaggroTime;           // Time until deaggro
	uint32 m_NeutralTime;           //
	int16  m_skillTP;               // TP amount for a Mobskill
	bool   m_drawnIn;               // Can use Draw In ability
	bool   m_mobskillattack;        // Is the mob using a Mobskill?
	bool   m_actionqueueability;

	bool   m_CanStandback;         // Can the mob stand back
	uint32 m_LastStandbackTime;    // Last time the mob stood back

	void ActionRoaming();           // Roam
	void ActionEngage();            // Engage Enemy
	void ActionDisengage();         // Disengage enemy

	void ActionFall();              // Drop to the ground
	void ActionDropItems();         // Drop loot
	void ActionDeath();             // Mob death
	void ActionFadeOut();           // Dematerialize
	void ActionSpawn();             // Spawn to the zone

	void ActionDespawn();           // Despawn and go to action none

	void ActionAbilityStart();      // Start a Mob ability
	void ActionAbilityUsing();      // In the process of using a Mob ability
	void ActionAbilityFinish();     // Finish a Mob ability
	void ActionAbilityInterrupt();  // Interrupt a Mob ability

	void ActionAttack();            // Start attacking an enemy
	void FinishAttack();            // Finish attacking an enemy

	void ActionSleep();             // Under the effect of Sleep
	void ActionStun();              // Under the effect of Stun

	void ActionMagicStart();        // Start magic casting
	void ActionMagicCasting();      // In the process of magic casting
	void ActionMagicInterrupt();    // Interrupt magic casting
	void ActionMagicFinish();       // Finish magic casting

	void ActionSpecialSkill();      // Use its special skill, ranged attack, catapult, jump etc

	// helper functions
	bool TryDeaggro();                                             // Is the Mob trying to deaggro
	void TryLink();                                                // Are there any Mobs around to link with
	bool CanCastSpells();                                          // Is the Mob able to cast spells
	bool TryCastSpell();                                           // Logic for spell casting, returns true if found one to cast
	bool TrySpecialSkill();                                        // Is the Mob trying to use a specialability
	void CastSpell(uint16 spellId, CBattleEntity* PTarget = NULL); // Makes the mob cast a spell
	void SetupEngage();                                            // Setup timers and trigger callbacks

	void FollowPath();                                             // Continues moving

	void Deaggro();                                                // Deaggro target
	void OnTick();

private:

};

#endif
