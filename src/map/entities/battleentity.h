﻿/*
===========================================================================

  Copyright (c) 2010-2014 Darkstar Dev Teams

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

  This file is part of DarkStar-server source code.

===========================================================================
*/

#ifndef _BATTLEENTITY_H
#define _BATTLEENTITY_H

#include <vector>

#include "../items/item_weapon.h"

#include "baseentity.h"
#include "../trait.h"
#include "../modifier.h"
#include "../party.h"
#include "../status_effect_container.h"
#include "../map.h"

/************************************************************************
*                                                                       *
*  Entity Ecosystem Enumerators                                         *
*                                                                       *
************************************************************************/

enum ECOSYSTEM
{
	SYSTEM_ERROR			= 0,
	SYSTEM_AMORPH			= 1,
	SYSTEM_AQUAN			= 2,
	SYSTEM_ARCANA			= 3,
	SYSTEM_ARCHAICMACHINE	= 4,
	SYSTEM_AVATAR			= 5,
	SYSTEM_BEAST			= 6,
	SYSTEM_BEASTMEN			= 7,
	SYSTEM_BIRD				= 8,
	SYSTEM_DEMON			= 9,
	SYSTEM_DRAGON			= 10,
	SYSTEM_ELEMENTAL		= 11,
	SYSTEM_EMPTY			= 12,
	SYSTEM_HUMANOID			= 13,
	SYSTEM_LIZARD			= 14,
	SYSTEM_LUMORIAN			= 15,
	SYSTEM_LUMINION			= 16,
	SYSTEM_PLANTOID			= 17,
	SYSTEM_UNCLASSIFIED		= 18,
	SYSTEM_UNDEAD			= 19,
	SYSTEM_VERMIN			= 20,
	SYSTEM_VORAGEAN			= 21,
};

/************************************************************************
*                                                                       *
*  DEFINE Maximum number of Mob Families                                *
*                                                                       *
************************************************************************/

#define MAX_MOB_FAMILY	900

/************************************************************************
*                                                                       *
*  Job Type Enumerators                                                 *
*                                                                       *
************************************************************************/

enum JOBTYPE
{
	JOB_NON				= 0,
	JOB_WAR				= 1,
	JOB_MNK				= 2,
	JOB_WHM				= 3,
	JOB_BLM				= 4,
	JOB_RDM				= 5,
	JOB_THF				= 6,
	JOB_PLD				= 7,
	JOB_DRK				= 8,
	JOB_BST				= 9,
	JOB_BRD				= 10,
	JOB_RNG				= 11,
	JOB_SAM				= 12,
	JOB_NIN				= 13,
	JOB_DRG				= 14,
	JOB_SMN				= 15,
	JOB_BLU				= 16,
	JOB_COR				= 17,
	JOB_PUP				= 18,
	JOB_DNC				= 19,
	JOB_SCH				= 20,
	JOB_GEO				= 21,
	JOB_RUN				= 22
};

/************************************************************************
*                                                                       *
*  DEFINE Maximum Job Types                                             *
*                                                                       *
************************************************************************/

#define MAX_JOBTYPE		23

/************************************************************************
*                                                                       *
*  Skill Types Enumerators                                              *
*                                                                       *
************************************************************************/

enum SKILLTYPE
{
	SKILL_NON			= 0,
	SKILL_H2H			= 1,
	SKILL_DAG			= 2,
	SKILL_SWD			= 3,
	SKILL_GSD			= 4,
	SKILL_AXE			= 5,
	SKILL_GAX			= 6,
	SKILL_SYH			= 7,
	SKILL_POL			= 8,
	SKILL_KAT			= 9,
	SKILL_GKT			= 10,
	SKILL_CLB			= 11,
	SKILL_STF			= 12,

	SKILL_AME			= 22,
	SKILL_ARA			= 23,
	SKILL_AMA			= 24,
	SKILL_ARC			= 25,
	SKILL_MRK			= 26,
	SKILL_THR			= 27,
	SKILL_GRD			= 28,
	SKILL_EVA			= 29,
	SKILL_SHL			= 30,
	SKILL_PAR			= 31,
	SKILL_DIV			= 32,
	SKILL_HEA			= 33,
	SKILL_ENH			= 34,
	SKILL_ENF			= 35,
	SKILL_ELE			= 36,
	SKILL_DRK			= 37,
	SKILL_SUM			= 38,
	SKILL_NIN			= 39,
	SKILL_SNG			= 40,
	SKILL_STR			= 41,
	SKILL_WND			= 42,
	SKILL_BLU			= 43,
	SKILL_GEO			= 44,
	SKILL_HND			= 45,

	SKILL_FSH			= 48,
	SKILL_WDW			= 49,
	SKILL_SMT			= 50,
	SKILL_GLD			= 51,
	SKILL_CLT			= 52,
	SKILL_LTH			= 53,
	SKILL_BON			= 54,
	SKILL_ALC			= 55,
	SKILL_COK			= 56,
	SKILL_SYN			= 57,
	SKILL_RID			= 58
};

/************************************************************************
*                                                                       *
*  DEFINE Maximum Skill Types                                           *
*                                                                       *
************************************************************************/

#define MAX_SKILLTYPE	64

enum SUBSKILLTYPE
{
	SUBSKILL_XBO		= 0,
	SUBSKILL_GUN		= 1,
	SUBSKILL_CNN		= 2,

	SUBSKILL_SHEEP		= 21,
	SUBSKILL_HARE		= 22,
	SUBSKILL_CRAB		= 23,
	SUBSKILL_CARRIE		= 24,
	SUBSKILL_HOMUNCULUS = 25,
	SUBSKILL_FLYTRAP	= 26,
	SUBSKILL_TIGER		= 27,
	SUBSKILL_BILL		= 28,
	SUBSKILL_EFT		= 29,
	SUBSKILL_LIZARD		= 30,
	SUBSKILL_MAYFLY		= 31,
	SUBSKILL_FUNGUAR	= 32,
	SUBSKILL_BEETLE		= 33,
	SUBSKILL_ANTLION	= 34,
	SUBSKILL_MITE		= 35,
	SUBSKILL_MELODIA	= 36,
	SUBSKILL_STEFFI		= 37,
	SUBSKILL_BEN		= 38,
	SUBSKILL_SIRAVARDE  = 39,
	SUBSKILL_COMO		= 40,
	SUBSKILL_OROB		= 41,
	SUBSKILL_AUDREY		= 42,
	SUBSKILL_ALLIE		= 43,
	SUBSKILL_LARS		= 44,
	SUBSKILL_GALAHAD	= 45,
	SUBSKILL_CHUCKY		= 46,
	SUBSKILL_SABOTENDER = 47,
	SUBSKILL_CLYVONNE   = 49,
	SUBSKILL_SHASRA		= 50,
	SUBSKILL_LULUSH		= 51,
	SUBSKILL_FARGANN	= 52,
	SUBSKILL_LOUISE		= 53,
	SUBSKILL_SIEGHARD	= 54,
	SUBSKILL_YULY		= 55,
	SUBSKILL_MERLE		= 56,
	SUBSKILL_NAZUNA		= 57,
	SUBSKILL_CETAS		= 58,
	SUBSKILL_ANNA		= 59,
	SUBSKILL_JULIO		= 60,
	SUBSKILL_BRONCHA	= 61,
	SUBSKILL_GERARD		= 62,
	SUBSKILL_HOBS		= 63,
	SUBSKILL_FALCORR	= 64,
	SUBSKILL_RAPHIE		= 65,
	SUBSKILL_MAC		= 66,
	SUBSKILL_SILAS		= 67,
	SUBSKILL_TOLOI		= 68
};

/************************************************************************
*                                                                       *
*  Equipment Slot Enumerators                                           *
*  (Monsters use only the first four, the character uses all)           *
*                                                                       *
************************************************************************/

enum SLOTTYPE
{
	SLOT_MAIN	= 0x00,
	SLOT_SUB	= 0x01,
	SLOT_RANGED	= 0x02,
	SLOT_AMMO	= 0x03,
	SLOT_HEAD	= 0x04,
	SLOT_BODY	= 0x05,
	SLOT_HANDS	= 0x06,
	SLOT_LEGS	= 0x07,
	SLOT_FEET	= 0x08,
	SLOT_NECK	= 0x09,
	SLOT_WAIST	= 0x0A,
	SLOT_EAR1	= 0x0B,
	SLOT_EAR2	= 0x0C,
	SLOT_RING1	= 0x0D,
	SLOT_RING2	= 0x0E,
	SLOT_BACK	= 0x0F,
	SLOT_LINK	= 0x10
};

/************************************************************************
*                                                                       *
*  Weapon Damage Type Enumerators                                       *
*  CROSSBOW and GUN - in Piercing, the division made by the same        *
*  skilltype to be able to distinguish these weapons when equipped and  *
*  get rid of the error use bullets with a crossbow and crossbow bolts  *
  with a firearm (only characters)                                      *
*                                                                       *
************************************************************************/

enum DAMAGETYPE
{
	DAMAGE_NONE				= 0,
	DAMAGE_PIERCING			= 1,
	DAMAGE_SLASHING			= 2,
	DAMAGE_IMPACT			= 3,
	DAMAGE_HTH				= 4,
	DAMAGE_CROSSBOW			= 5,
	DAMAGE_GUN				= 6
};

/************************************************************************
*                                                                       *
*  Reaction Types Enumerators                                           *
*                                                                       *
************************************************************************/

enum REACTION
{
	REACTION_NONE			= 0x00,		// Lack of response
	REACTION_MISS			= 0x01,		// Slip
	REACTION_PARRY			= 0x03,		// Locking arms (MISS + PARRY)
	REACTION_BLOCK			= 0x04,		// Block with
	REACTION_HIT			= 0x08,		// Hit
	REACTION_EVADE			= 0x09,		// Evasion (MISS + HIT)
	REACTION_GUARD			= 0x14,		// MNK guard (20 dec)
};

/************************************************************************
*                                                                       *
*  Special Effects Enumerators                                          *
*                                                                       *
************************************************************************/

enum SPECEFFECT
{
	SPECEFFECT_NONE			= 0x00,
	SPECEFFECT_BLOOD		= 0x02,
	SPECEFFECT_HIT			= 0x10,
	SPECEFFECT_RAISE		= 0x11,
	SPECEFFECT_RECOIL		= 0x20,
	SPECEFFECT_CRITICAL_HIT	= 0x22
};

/************************************************************************
*                                                                       *
*  ubeffect Enumerators                                                 *
*                                                                       *
************************************************************************/

enum SUBEFFECT
{
	// ATTACK
	SUBEFFECT_FIRE_DAMAGE		 = 1,	// 110000	 3
	SUBEFFECT_ICE_DAMAGE		 = 2,	// 1-01000	 5
	SUBEFFECT_WIND_DAMAGE		 = 3,	// 111000	 7
	SUBEFFECT_EARTH_DAMAGE		 = 4,	// 1-00100	 9
	SUBEFFECT_LIGHTNING_DAMAGE	 = 5,	// 110100	11
	SUBEFFECT_WATER_DAMAGE		 = 6,	// 1-01100	13
	SUBEFFECT_LIGHT_DAMAGE		 = 7,	// 111100	15
	SUBEFFECT_DARKNESS_DAMAGE	 = 8,	// 1-00010	17
	SUBEFFECT_SLEEP				 = 9,	// 110010	19
	SUBEFFECT_POISON			 = 10,	// 1-01010	21
	SUBEFFECT_PARALYSIS			 = 11,
	SUBEFFECT_BLIND				 = 12,	// 1-00110	25
	SUBEFFECT_SILENCE			 = 13,
	SUBEFFECT_PETRIFY			 = 14,
	SUBEFFECT_PLAGUE			 = 15,
	SUBEFFECT_STUN				 = 16,
	SUBEFFECT_CURSE				 = 17,
	SUBEFFECT_DEFENSE_DOWN		 = 18,	// 1-01001	37
	SUBEFFECT_SHIELD			 = 20,
	SUBEFFECT_HP_DRAIN			 = 21,	// 1-10101	43
	SUBEFFECT_TP_DRAIN			 = 22,
	SUBEFFECT_HASTE				 = 23,

	// SPIKES
	SUBEFFECT_BLAZE_SPIKES		= 1,	// 01-1000	 6
	SUBEFFECT_ICE_SPIKES		= 2,	// 01-0100	10
	SUBEFFECT_DREAD_SPIKES		= 3,	// 01-1100	14
	SUBEFFECT_CURSE_SPIKES		= 4,	// 01-0010	18
	SUBEFFECT_SHOCK_SPIKES		= 5, 	// 01-1010	22
	SUBEFFECT_REPRISAL			= 6,	// 01-0110	26
	SUBEFFECT_WIND_SPIKES		= 7,
	SUBEFFECT_STONE_SPIKES		= 8,
	SUBEFFECT_DELUGE_SPIKES		= 9,
	SUBEFFECT_DARK_SPIKES		= 10,
	// 11-62 = none!
	SUBEFFECT_COUNTER			= 63,

	// SKILLCHAINS
	SUBEFFECT_LIGHT				= 1,
	SUBEFFECT_DARKNESS			= 2,
	SUBEFFECT_GRAVITATION		= 3,
	SUBEFFECT_FRAGMENTATION		= 4,
	SUBEFFECT_DISTORTION		= 5,
	SUBEFFECT_FUSION			= 6,
	SUBEFFECT_COMPRESSION		= 7,
	SUBEFFECT_LIQUEFACATION		= 8,
	SUBEFFECT_INDURATION		= 9,
	SUBEFFECT_REVERBERATION		= 10,
	SUBEFFECT_TRANSFIXION		= 11,
	SUBEFFECT_SCISSION			= 12,
	SUBEFFECT_DETONATION		= 13,
	SUBEFFECT_IMPACTION			= 14,

	SUBEFFECT_NONE				= 0,

	// UNKNOWN
	SUBEFFECT_IMPAIRS_EVASION,
	SUBEFFECT_MP_DRAIN,
	SUBEFFECT_BIND,
	SUBEFFECT_WEIGHT,
	SUBEFFECT_AUSPICE,
	SUBEFFECT_WEAKENS_ATTACKS,
	SUBEFFECT_TERROR,
	SUBEFFECT_ADDLE,
	SUBEFFECT_CHOKE,
	SUBEFFECT_AMNESIA,
	SUBEFFECT_STR_BOOST,
	SUBEFFECT_BLINK,
	SUBEFFECT_FLEE,
	SUBEFFECT_REGEN,
	SUBEFFECT_PIERCING,
	SUBEFFECT_RECOVER,
	SUBEFFECT_DAMAGE,
	SUBEFFECT_TP_DOWN
};

/************************************************************************
*                                                                       *
*  Target Type Enumerators                                              *
*                                                                       *
************************************************************************/

enum TARGETTYPE
{
	TARGET_SELF						= 0x01,
	TARGET_PLAYER_PARTY				= 0x02,
	TARGET_ENEMY					= 0x04,
	TARGET_PLAYER_ALLIANCE			= 0x08,
	TARGET_PLAYER					= 0x10,
	TARGET_PLAYER_DEAD				= 0x20,
	TARGET_NPC						= 0x40,		// Likely meant mob, looking like npc and fighting on the side of the character
    TARGET_PLAYER_PARTY_PIANISSIMO	= 0x80
};

/************************************************************************
*                                                                       *
*  Skillchain Enumerators                                               *
*                                                                       *
************************************************************************/

enum SKILLCHAIN_ELEMENT
{
	SC_NONE			 =  0, // Lv0 None

	SC_TRANSFIXION	 =  1, // Lv1 Light
	SC_COMPRESSION	 =  2, // Lv1 Dark
	SC_LIQUEFACTION	 =  3, // Lv1 Fire
	SC_SCISSION		 =  4, // Lv1 Earth
	SC_REVERBERATION =  5, // Lv1 Water
	SC_DETONATION	 =  6, // Lv1 Wind
	SC_INDURATION	 =  7, // Lv1 Ice
	SC_IMPACTION	 =  8, // Lv1 Thunder

	SC_GRAVITATION	 =  9, // Lv2 Dark & Earth
	SC_DISTORTION	 = 10, // Lv2 Water & Ice
	SC_FUSION		 = 11, // Lv2 Fire & Light
	SC_FRAGMENTATION = 12, // Lv2 Wind & Thunder

	SC_LIGHT		 = 13, // Lv3 Fire, Light, Wind, Thunder
	SC_DARKNESS		 = 14, // Lv3 Dark, Earth, Water, Ice
	SC_LIGHT_II		 = 15, // Lv4 Light
	SC_DARKNESS_II	 = 16, // Lv4 Darkness
};

/************************************************************************
*                                                                       *
*  DEFINES for Skillchains                                              *
*                                                                       *
************************************************************************/

#define MAX_SKILLCHAIN_LEVEL (4)
#define MAX_SKILLCHAIN_COUNT (5)

/************************************************************************
*                                                                       *
*  Immunity Enumerators                                                 *
*                                                                       *
************************************************************************/

enum IMMUNITY : uint16
{
	IMMUNITY_NONE		= 0x00,
	IMMUNITY_SLEEP		= 0x01,
	IMMUNITY_GRAVITY	= 0x02,
	IMMUNITY_BIND		= 0x04,
	IMMUNITY_STUN		= 0x08,
	IMMUNITY_SILENCE	= 0x10,		// 16
	IMMUNITY_PARALYZE	= 0x20, 	// 32
	IMMUNITY_BLIND		= 0x40, 	// 64
	IMMUNITY_SLOW		= 0x80,		// 128
	IMMUNITY_POISON		= 0x100,	// 256
	IMMUNITY_ELEGY		= 0x200,	// 512
	IMMUNITY_REQUIEM	= 0x400,	// 1024
};

/************************************************************************
*                                                                       *
*  Action Structure                                                     *
*                                                                       *
************************************************************************/

struct apAction_t
{
	CBattleEntity*	ActionTarget;		// 32 bits
	REACTION		reaction;			// 5 bits
	uint16			animation;			// 12 bits
	SPECEFFECT		speceffect;			// 7 bits
	uint8			knockback;			// 3 bits
	int32			param;				// 17 bits
	uint16			messageID;			// 10 bits
	SUBEFFECT		additionalEffect;	// 10 bits
    int32           addEffectParam;		// 17 bits
	uint16			addEffectMessage;	// 10 bits
	SUBEFFECT		spikesEffect;		// 10 bits
	uint16			spikesParam;		// 14 bits
	uint16			spikesMessage;		// 10 bits

	apAction_t()
	{
		ActionTarget		= NULL;
		reaction			= REACTION_NONE;
		animation			= 0;
		speceffect			= SPECEFFECT_NONE;
		param				= 0;
		messageID			= 0;
		additionalEffect	= SUBEFFECT_NONE;
		addEffectParam		= 0;
		addEffectMessage	= 0;
		spikesEffect		= SUBEFFECT_NONE;
		spikesParam			= 0;
		spikesMessage		= 0;
		knockback			= 0;
	}

};

/************************************************************************
*																		*
*  TP хранится то пому же принципу, что и skill, т.е. 6,4% = 64			*
*  TP is kept the same principle as the skill, i.e. 6,4% = 64           *
*																		*
************************************************************************/

struct health_t
{
    int16   tp;                 // Present value
    int32   hp, mp;             // Current values
    int32   maxhp, maxmp;       // Maximum
    int32   modhp, modmp;       // Modified maximum
};

typedef std::vector<apAction_t> ActionList_t;

class CBattleEntity : public CBaseEntity
{
public:

	health_t		health;						// HP,MP,TP
	stats_t			stats;						// Attributes STR,DEX,VIT,AGI,INT,MND,CHR
	skills_t		WorkingSkills;				// Structure essentially all available skills, the limited level of
	uint16			m_Immunity;					// Mob immunity
	uint16			m_magicEvasion;				// Store this so it can be removed easily
	uint8			m_enmityRange; 				// Only get enmity from entities this close
	bool			m_unkillable;				// Entity is not able to die (probably until some action removes this flag)

    uint16          STR();
    uint16          DEX();
    uint16          VIT();
    uint16          AGI();
    uint16          INT();
    uint16          MND();
    uint16          CHR();
    uint16          DEF();
    uint16          ATT();
	uint16			ACC(uint8 attackNumber, uint8 offsetAccuracy);
    uint16          EVA();
	uint16          RATT(uint8 skill);

    uint8           GetSpeed();

	uint32			charmTime;					// To hold the time entity is charmed
	bool			isCharmed;					// Is the battle entity charmed?

	bool			isDead();					// Check whether the entity is dead
	bool			isInDynamis();
	bool			hasImmunity(uint32 imID);
	bool			isAsleep();


	JOBTYPE			GetMJob();					// Home occupation
	JOBTYPE			GetSJob();					// Secondary occupation
	uint8			GetMLevel();				// Level of the main occupations
	uint8			GetSLevel();				// Additional level of profession

	void			SetMJob(uint8 mjob);		// Home occupation
	void			SetSJob(uint8 sjob);		// Secondary occupation
	void			SetMLevel(uint8 mlvl);		// Level of the main occupations
	void			SetSLevel(uint8 slvl);		// Additional level of profession

	uint8			GetHPP();					// Percentage of HP
	int32			GetMaxHP();                 // The maximum amount of HP
	uint8			GetMPP();					// Percentage of MP
	int32			GetMaxMP();                 // The maximum number of MP
	void			UpdateHealth();             // Counting the maximum number of HP and MP, as well as the adjustment of their current values

	int16			GetWeaponDelay(bool tp);		// Returns delay of combined weapons
	int16			GetRangedWeaponDelay(bool tp);	// Returns delay of ranged weapon + ammo where applicable
	int16			GetAmmoDelay(bool tp);			// Returns delay of ammo (for cooldown between shots)
	uint16			GetMainWeaponDmg();				// Returns total main hand DMG
	uint16			GetSubWeaponDmg();				// Returns total sub weapon DMG
	uint16			GetRangedWeaponDmg();			// Returns total ranged weapon DMG
	uint16			GetMainWeaponRank();			// Returns total main hand DMG Rank
	uint16			GetSubWeaponRank();				// Returns total sub weapon DMG Rank
	uint16			GetRangedWeaponRank();			// Returns total ranged weapon DMG Rank

	uint16			GetSkill(uint16 SkillID);	// Current value of skills (not the maximum, and the limited level)

	virtual int16	addTP(int16 tp);			// Increase / decrease the number of  TP
	virtual int16	delTP(int16 tp);			// Decrease the number of  TP
	virtual int32	addHP(int32 hp);			// Increase / decrease the number of  HP
	virtual int32 	addMP(int32 mp);			// Increase / decrease the number of  MP

	int16			getMod(uint16 modID);		// Value modifier

	bool			CanRest();					// Checks if able to heal
	bool			Rest(float rate); 			// Heal an amount of HP / MP

	void			addModifier(uint16 type, int16 amount);
	void			setModifier(uint16 type, int16 amount);
	void			delModifier(uint16 type, int16 amount);
	void			addModifiers(std::vector<CModifier*> *modList);
	void			addEquipModifiers(std::vector<CModifier*> *modList, uint8 itemLevel, uint8 slotid);
	void			setModifiers(std::vector<CModifier*> *modList);
	void			delModifiers(std::vector<CModifier*> *modList);
	void			delEquipModifiers(std::vector<CModifier*> *modList, uint8 itemLevel, uint8 slotid);
	void			saveModifiers();			// Save current state of modifiers
	void			restoreModifiers();			// Restore to saved state

	uint8			m_ModelSize;				// Size model of the entity for the calculation range of physical attack
	ECOSYSTEM		m_EcoSystem;				// Eco-system of nature
	CItemWeapon*	m_Weapons[4];				// Four major cell used for the storage of weapons (weapons only)

	TraitList_t		TraitList;					// Fasting list active abilities as pointers

	EntityID_t		m_OwnerID;					// ID attacking entity (after death will store ID essentially nanesschey last hit)

	ActionList_t	m_ActionList;				// List of acts committed in one attack (you need to write a framework that includes ActionList category in which to animate and etc.)

	CParty*			PParty;						// Description of the group, which is the essence
	CBattleEntity*	PPet;						// Pet essence
	CBattleEntity*	PMaster;					// Owner / host entity (applies to all military entity)

	CStatusEffectContainer* StatusEffectContainer;

	CBattleEntity();							// Designer
	virtual ~CBattleEntity();					// Destructor

private:

	JOBTYPE		m_mjob;							// Home occupation
	JOBTYPE		m_sjob;							// Secondary occupation
	uint8		m_mlvl;							// The current level of the main occupations
	uint8		m_slvl;							// The current level of the profession more

	int16		m_modStat[MAX_MODIFIER];		// Array modifiers
	int16		m_modStatSave[MAX_MODIFIER];	// Saved state
};

#endif
