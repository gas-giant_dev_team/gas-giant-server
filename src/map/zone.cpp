﻿/*
===========================================================================

  Copyright (c) 2010-2014 Darkstar Dev Teams

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

  This file is part of DarkStar-server source code.

===========================================================================
*/

// TODO:
// нужно разделить класс czone на базовый и наследников. уже нарисовались: Standard, Rezident, Instance и Dinamis
// у каждой из указанных зон особое поведение
// We have to divide the class czone the base and heirs. already drawn: Standard, Resident, Instance and Dynamis
// In each of the areas of particular behaviour

#include "../common/showmsg.h"
#include "../common/timer.h"
#include "../common/utils.h"

#include <string.h>

#include "utils/battleutils.h"
#include "utils/charutils.h"
#include "enmity_container.h"
#include "utils/itemutils.h"
#include "map.h"
#include "utils/mobutils.h"
#include "entities/npcentity.h"
#include "entities/petentity.h"
#include "utils/petutils.h"
#include "spell.h"
#include "treasure_pool.h"
#include "vana_time.h"
#include "zone.h"
#include "zone_entities.h"
#include "ai/ai_mob_dummy.h"

#include "lua/luautils.h"

#include "packets/action.h"
#include "packets/char.h"
#include "packets/char_sync.h"
#include "packets/char_update.h"
#include "packets/entity_update.h"
#include "packets/inventory_assign.h"
#include "packets/inventory_finish.h"
#include "packets/inventory_item.h"
#include "packets/lock_on.h"
#include "packets/message_basic.h"
#include "packets/server_ip.h"
#include "packets/wide_scan.h"


/************************************************************************
*																		*
*  Cервер для обработки активности сущностей (по серверу на зону) без	*
*  активных областей													*
*																		*
*  Server for processing active entities (the server for the zone)		*
*  without active regions												*
*                                                                       *
************************************************************************/

int32 zone_server(uint32 tick, CTaskMgr::CTask* PTask)
{
	((CZone*)PTask->m_data)->ZoneServer(tick);
	return 0;
}

/************************************************************************
*																		*
*  Cервер для обработки активности сущностей (по серверу на зону) c		*
*  активными областями													*
*																		*
*  Server for processing active entities (the server for the zone)      *
*  of Active regions                                                    *
*                                                                       *
************************************************************************/

int32 zone_server_region(uint32 tick, CTaskMgr::CTask* PTask)
{
	CZone* PZone = (CZone*)PTask->m_data;

	if ((tick - PZone->m_RegionCheckTime) < 1000)
	{
		PZone->ZoneServer(tick);
	}
	else
	{
		PZone->ZoneServerRegion(tick);
		PZone->m_RegionCheckTime = tick;
	}
	return 0;
}

/************************************************************************
*																		*
*  Class CZone															*
*																		*
************************************************************************/

CZone::CZone(ZONEID ZoneID, REGIONTYPE RegionID, CONTINENTTYPE ContinentID)
{
    ZoneTimer = NULL;

    m_zoneID             = ZoneID;
    m_zoneType           = ZONETYPE_NONE;
    m_regionID           = RegionID;
    m_continentID        = ContinentID;
    m_TreasurePool       = 0;
    m_RegionCheckTime    = 0;
    m_BattlefieldHandler = NULL;
    m_Weather            = WEATHER_NONE;
    m_WeatherChangeTime  = 0;
    m_IsWeatherStatic    = 0;
    m_useNavMesh         = false;
    m_navMesh            = NULL;
    m_zoneEntities = new CZoneEntities(this);

    // Settings should load first
    LoadZoneSettings();

    LoadZoneLines();
    LoadZoneWeather();
    LoadNavMesh();
}

CZone::~CZone()
{
	delete m_zoneEntities;
}

/************************************************************************
*                                                                       *
*  Функции доступа к полям класса                                       *
*  Access functions to class fields                                     *
*                                                                       *
************************************************************************/


ZONEID CZone::GetID()
{
	return m_zoneID;
}

/************************************************************************
*                                                                       *
*  Return the type of the current zone.                                 *
*                                                                       *
************************************************************************/

ZONETYPE CZone::GetType()
{
  return m_zoneType;
}

/************************************************************************
*                                                                       *
*  Return the Region ID of the current region.                          *
*                                                                       *
************************************************************************/

REGIONTYPE CZone::GetRegionID()
{
    return m_regionID;
}

/************************************************************************
*                                                                       *
*  Return the Continent ID of the current continent.                    *
*                                                                       *
************************************************************************/

CONTINENTTYPE CZone::GetContinentID()
{
    return m_continentID;
}

/************************************************************************
*                                                                       *
*  Return the LAN IP of the Map Server.                                 *
*                                                                       *
************************************************************************/

uint32 CZone::GetLANIP()
{
    return m_zoneLANIP;
}

/************************************************************************
*                                                                       *
*  Return the WAN IP of the Map Server.                                 *
*                                                                       *
************************************************************************/

uint32 CZone::GetWANIP()
{
    return m_zoneWANIP;
}

/************************************************************************
*                                                                       *
*  Return the Port of the Map Server.                                   *
*                                                                       *
************************************************************************/

uint16 CZone::GetPort()
{
	return m_zonePort;
}

/************************************************************************
*                                                                       *
*  Return the tax on Bazaar sales of the Server.                        *
*                                                                       *
************************************************************************/

uint16 CZone::GetTax()
{
	return m_tax;
}

/************************************************************************
*                                                                       *
*  Return the Weather in the Zone.                                      *
*                                                                       *
************************************************************************/

WEATHER CZone::GetWeather()
{
	return m_Weather;
}

/************************************************************************
*                                                                       *
*  Return the time for next Weather change.                             *
*                                                                       *
************************************************************************/

uint32 CZone::GetWeatherChangeTime()
{
    return m_WeatherChangeTime;
}

/************************************************************************
*                                                                       *
*  Return the name of the zone.                                         *
*                                                                       *
************************************************************************/

const int8* CZone::GetName()
{
	return m_zoneName.c_str();
}

/************************************************************************
*                                                                       *
*  Return the Solo Battle Music of the zone.							*
*                                                                       *
************************************************************************/

uint8 CZone::GetSoloBattleMusic()
{
	return m_zoneMusic.m_bSongS;
}

/************************************************************************
*                                                                       *
*  Return the Party Battle Music of the zone.                           *
*                                                                       *
************************************************************************/

uint8 CZone::GetPartyBattleMusic()
{
	return m_zoneMusic.m_bSongM;
}

/************************************************************************
*                                                                       *
*  Return the Background Music of the zone.                             *
*                                                                       *
************************************************************************/

uint8 CZone::GetBackgroundMusic()
{
	return m_zoneMusic.m_song;
}

/************************************************************************
*                                                                       *
*  Check the Misc Restrictions Flag of the zone.                        *
*  Can use, costume, chocobo, cast spells, pets, etc..                  *
*                                                                       *
************************************************************************/

bool CZone::CanUseMisc(uint16 misc)
{
	return (m_miscMask & misc) == misc;
}

/************************************************************************
*                                                                       *
*  Check if the weather in the zone never changes.                      *
*                                                                       *
************************************************************************/

bool CZone::IsWeatherStatic()
{
    return m_IsWeatherStatic;
}

/************************************************************************
*                                                                       *
*  Return the zone lines of the zone.                                   *
*                                                                       *
************************************************************************/

zoneLine_t* CZone::GetZoneLine(uint32 zoneLineID)
{
	for(zoneLineList_t::const_iterator  i = m_zoneLineList.begin();
		i != m_zoneLineList.end();
		i++ )
	{
		if(	(*i)->m_zoneLineID == zoneLineID )
		{
			return (*i);
		}
	}
	return NULL;
}

/************************************************************************
*																		*
*  Загружаем ZoneLines, необходимые для правильного перемещения	между	*
*  зонами.																*
*																		*
*  Load ZoneLines, necessary for proper movement between zones.         *
*                                                                       *
************************************************************************/

void CZone::LoadZoneLines()
{
	static const int8 fmtQuery[] = "SELECT zoneline, tozone, tox, toy, toz, rotation FROM zonelines WHERE fromzone = %u";

	int32 ret = Sql_Query(SqlHandle, fmtQuery, m_zoneID);

	if( ret != SQL_ERROR && Sql_NumRows(SqlHandle) != 0)
	{
		while(Sql_NextRow(SqlHandle) == SQL_SUCCESS)
		{
			zoneLine_t* zl = new zoneLine_t;

			zl->m_zoneLineID = (uint32)Sql_GetIntData(SqlHandle,0);
			zl->m_toZone  = (uint16)Sql_GetIntData(SqlHandle,1);
			zl->m_toPos.x = Sql_GetFloatData(SqlHandle,2);
			zl->m_toPos.y = Sql_GetFloatData(SqlHandle,3);
			zl->m_toPos.z = Sql_GetFloatData(SqlHandle,4);
			zl->m_toPos.rotation = (uint8)Sql_GetIntData(SqlHandle,5);

			m_zoneLineList.push_back(zl);
		}
	}
}

/************************************************************************
*                                                                       *
*  Загружаем параметры погоды                                           *
*  Load parameters weather                                              *
*                                                                       *
************************************************************************/

void CZone::LoadZoneWeather()
{
    static const int8* Query =
        "SELECT "
          "weather.none,"
          "weather.sunshine,"
          "weather.clouds,"
          "weather.fog,"
          "weather.hot_spell,"
          "weather.heat_wave,"
          "weather.rain,"
          "weather.squall,"
          "weather.dust_storm,"
          "weather.sand_storm,"
          "weather.wind,"
          "weather.gales,"
          "weather.snow,"
          "weather.blizzards,"
          "weather.thunder,"
          "weather.thunder_storms,"
          "weather.auroras,"
          "weather.stellar_glares,"
          "weather.gloom,"
          "weather.darkness "
        "FROM zone_weather as weather "
        "WHERE zoneid = %u "
        "LIMIT 1";

    if (Sql_Query(SqlHandle, Query, m_zoneID) != SQL_ERROR &&
        Sql_NumRows(SqlHandle) != 0 &&
        Sql_NextRow(SqlHandle) == SQL_SUCCESS)
    {
        uint16 Frequency = 0;

        for (uint8 weather = 0; weather < MAX_WEATHER_ID; ++weather)
        {
            m_WeatherFrequency[weather] = (uint8)Sql_GetIntData(SqlHandle,weather);

            if (m_WeatherFrequency[weather] == 100)
            {
                m_IsWeatherStatic = true;
                SetWeather((WEATHER)weather);
            }
            Frequency += m_WeatherFrequency[weather];
        }
        if (Frequency != 100)
        {
            // ShowWarning(CL_YELLOW"Total Weather Frequency is %u for zone %u\n" CL_RESET, Frequency, m_zoneID);
        }
    }
    else
    {
        memset(&m_WeatherFrequency, 0, sizeof(m_WeatherFrequency));
        ShowFatalError(CL_RED"CZone::LoadZoneWeather: Cannot load zone weather (%u)\n" CL_RESET, m_zoneID);
    }
}

/************************************************************************
*																		*
*  Загружаем настройки зоны из базы										*
*  Load zone settings from the database                                 *
*																		*
************************************************************************/

void CZone::LoadZoneSettings()
{
    static const int8* Query =
        "SELECT "
          "zone.name,"
          "zone.lanip,"
          "zone.wanip,"
          "zone.zoneport,"
          "zone.music,"
          "zone.battlesolo,"
          "zone.battlemulti,"
          "zone.tax,"
          "zone.misc,"
          "zone.navmesh,"
          "zone.zonetype,"
          "bcnm.name "
        "FROM zone_settings AS zone "
        "LEFT JOIN bcnm_info AS bcnm "
        "USING (zoneid) "
        "WHERE zoneid = %u "
        "LIMIT 1";

    if (Sql_Query(SqlHandle, Query, m_zoneID) != SQL_ERROR &&
        Sql_NumRows(SqlHandle) != 0 &&
        Sql_NextRow(SqlHandle) == SQL_SUCCESS)
    {
        m_zoneName.insert(0, Sql_GetData(SqlHandle,0));

		m_zoneLANIP   = (uint32)Sql_GetUIntData(SqlHandle,1);
		m_zoneWANIP   = (uint32)Sql_GetUIntData(SqlHandle,2);
		m_zonePort = (uint16)Sql_GetUIntData(SqlHandle,3);
		m_zoneMusic.m_song   = (uint8)Sql_GetUIntData(SqlHandle,4);   // Background music
		m_zoneMusic.m_bSongS = (uint8)Sql_GetUIntData(SqlHandle,5);   // Solo battle music
		m_zoneMusic.m_bSongM = (uint8)Sql_GetUIntData(SqlHandle,6);   // Party battle music
		m_tax = (uint16)(Sql_GetFloatData(SqlHandle,7) * 100);        // Tax for bazaar
		m_miscMask = (uint16)Sql_GetUIntData(SqlHandle,8);
		m_useNavMesh = (bool)Sql_GetIntData(SqlHandle,9);

		m_zoneType = (ZONETYPE)Sql_GetUIntData(SqlHandle, 10);

        if (Sql_GetData(SqlHandle,11) != NULL) // Now we can not use bcnmid, because they start from scratch
        {
			m_BattlefieldHandler = new CBattlefieldHandler(m_zoneID);
		}
        if (m_miscMask & MISC_TREASURE)
		{
            m_TreasurePool = new CTreasurePool(TREASUREPOOL_ZONE);
		}
    }
    else
    {
        ShowFatalError(CL_RED"CZone::LoadZoneSettings: Cannot load zone settings (%u)\n" CL_RESET, m_zoneID);
    }
}

/************************************************************************
*                                                                       *
*  Disable / Enable maps navmesh in zone_settings.sql.					*
*                                                                       *
************************************************************************/

void CZone::LoadNavMesh()
{

    if(!m_useNavMesh) return;

    if(m_navMesh == NULL)
    {
        m_navMesh = new CNavMesh();
    }

    int8 file[255];
    memset(file,0,sizeof(file));
    snprintf(file, sizeof(file), "scripts/zones/%s/NavMesh.nav", GetName());

    if(m_navMesh->load(file))
    {
        // Lets verify it can find proper paths
        if(!m_navMesh->test((int16)GetID()))
        {
            // Test failed, don't use it
            m_useNavMesh = false;
        }
    }
    else
    {
        m_useNavMesh = false;
    }
}

/************************************************************************
*                                                                       *
*  Добавляем в зону MOB                                                 *
*  Add to the zone a MOB                                                *
*                                                                       *
************************************************************************/

void CZone::InsertMOB(CBaseEntity* PMob)
{
	m_zoneEntities->InsertMOB(PMob);
}

/************************************************************************
*																		*
*  Добавляем в зону NPC													*
*																		*
*                                                                       *
************************************************************************/

void CZone::InsertNPC(CBaseEntity* PNpc)
{
	m_zoneEntities->InsertNPC(PNpc);
}

/************************************************************************
*                                                                       *
*  Remove a pet from the zone.											*
*                                                                       *
************************************************************************/

void CZone::DeletePET(CBaseEntity* PPet)
{
	m_zoneEntities->DeletePET(PPet);
}

/************************************************************************
*                                                                       *
*  Добавляем в зону PET (свободные targid 0x700-0x7FF)                  *
*  Add the zone PET (free targid 0x700-0x7FF)							*
*                                                                       *
************************************************************************/

void CZone::InsertPET(CBaseEntity* PPet)
{
	m_zoneEntities->InsertPET(PPet);
}

/************************************************************************
*																		*
*  Добавляем в зону активную область									*
*  Add the zone active area                                             *
*																		*
************************************************************************/

void CZone::InsertRegion(CRegion* Region)
{
	if (Region != NULL)
	{
		m_regionList.push_back(Region);
	}
}

/************************************************************************
*                                                                       *
*  Ищем группу для монстра. Для монстров, объединенных в группу         *
*  работает система взаимопомощи (link)                                 *
*                                                                       *
*  We are looking for a group of monsters. For monsters, united in a    *
*  group mutual aid system works (link)                                 *
*                                                                       *
************************************************************************/

void CZone::FindPartyForMob(CBaseEntity* PEntity)
{
	m_zoneEntities->FindPartyForMob(PEntity);
}

/************************************************************************
*                                                                       *
*  Транспотр отправляется, необходимо собрать пассажиров                *
*  Transport sent, collect passengers                                   *
*                                                                       *
************************************************************************/

void CZone::TransportDepart(uint16 boundary, uint16 zone)
{
	m_zoneEntities->TransportDepart(boundary, zone);
}

/************************************************************************
*                                                                       *
*  Set the active weather in the zone.                                  *
*                                                                       *
************************************************************************/

void CZone::SetWeather(WEATHER weather)
{
    DSP_DEBUG_BREAK_IF(weather >= MAX_WEATHER_ID);

	if (m_Weather == weather)
		return;

	m_zoneEntities->WeatherChange(weather);

    m_Weather = weather;
    m_WeatherChangeTime = CVanaTime::getInstance()->getVanaTime();

	m_zoneEntities->PushPacket(NULL, CHAR_INZONE, new CWeatherPacket(m_WeatherChangeTime, m_Weather));
}

/************************************************************************
*																		*
*  Удаляем персонажа из зоны. Если запущен ZoneServer и персонажей		*
*  в зоне больше не осталось, то останавливаем ZoneServer				*
*																		*
*  Delete the character of the zone. If you are running ZoneServer and	*
*  characters in the area there are no more, then stop ZoneServer		*
*                                                                       *
************************************************************************/

void CZone::DecreaseZoneCounter(CCharEntity* PChar)
{
	m_zoneEntities->DecreaseZoneCounter(PChar);

	if (ZoneTimer && m_zoneEntities->CharListEmpty())
	{
		ZoneTimer->m_type = CTaskMgr::TASK_REMOVE;
		ZoneTimer = NULL;

		m_zoneEntities->HealAllMobs();
	}
	else
	{
		m_zoneEntities->DespawnPC(PChar);
	}

	CharZoneOut(PChar);
}

/************************************************************************
*																		*
*  Добавляем персонажа в зону. Если ZoneServer не запущен то запускам.	*
*  Обязательно проверяем количество персонажей в зоне.					*
*  Максимальное число персонажей в одной зоне - 768                     *
*																		*
*  Add the character to the zone. If it is not running ZoneServer		*
*  startups. Be sure to check the number of characters in the zone. 	*
*  The maximum number of characters in the same area - 768				*
*                                                                       *
************************************************************************/

void CZone::IncreaseZoneCounter(CCharEntity* PChar)
{
	DSP_DEBUG_BREAK_IF(PChar == NULL);
    DSP_DEBUG_BREAK_IF(PChar->loc.zone != NULL);
	DSP_DEBUG_BREAK_IF(PChar->PTreasurePool != NULL);

	PChar->targid = m_zoneEntities->GetNewTargID();

	if (PChar->targid >= 0x700)
	{
		ShowError(CL_RED"CZone::InsertChar : targid is high (03hX)\n" CL_RESET, PChar->targid);
		return;
	}

	m_zoneEntities->InsertPC(PChar);

	if (!ZoneTimer && !m_zoneEntities->CharListEmpty())
	{
		createZoneTimer();
	}

	CharZoneIn(PChar);
}

/************************************************************************
*																		*
*  Проверка видимости монстров персонажем. Дистанцию лучше вынести в	*
*  глобальную переменную (настройки сервера)							*
*  Именно в этой функции будем проверять агрессию мостров, чтобы не		*
*  вычислять distance несколько раз (например в ZoneServer)				*
*																		*
*  Check the sight of monsters character. Distance to make better       *
*  Global variable (server configuration)                               *
*  It is in this function will check the Mostra aggression, not to      *
*  Calculate the distance several times (eg in ZoneServer)              *
*                                                                       *
************************************************************************/

void CZone::SpawnMOBs(CCharEntity* PChar)
{
	m_zoneEntities->SpawnMOBs(PChar);
}

/************************************************************************
*																		*
*  Проверка видимости питомцев персонажем. Для появления питомцев		*
*  используем UPDATE вместо SPAWN. SPAWN используется лишь при вызове	*
*																		*
*  Check visibility pets character. For the appearance of pets          *
*  Use UPDATE instead of SPAWN. SPAWN is only used when calling         *
*                                                                       *
************************************************************************/

void CZone::SpawnPETs(CCharEntity* PChar)
{
	m_zoneEntities->SpawnPETs(PChar);
}

/************************************************************************
*																		*
*  Проверка видимости NPCs персонажем.									*
*  Check the visibility of NPCs character.                              *
*																		*
************************************************************************/

void CZone::SpawnNPCs(CCharEntity* PChar)
{
	m_zoneEntities->SpawnNPCs(PChar);
}

/************************************************************************
*																		*
*  Проверка видимости персонажей. Смысл действий в том, что персонажи	*
*  сами себя обновляют и добавляются в списки других персонажей.        *
*  В оригинальной версии размер списка ограничен и изменяется в			*
*  пределах 25-50 видимых персонажей.									*
*																		*
*  Check the visibility of characters. Meaning of actions that the      *
*  characters themselves updated and added to the lists of the other    *
*  characters. The original version of the list size is limited and     *
*  varies within 25-50 visible characters.                              *
*                                                                       *
************************************************************************/

void CZone::SpawnPCs(CCharEntity* PChar)
{
	m_zoneEntities->SpawnPCs(PChar);
}

/************************************************************************
*																		*
*  Отображаем Moogle в MogHouse											*
*  Display the Moogle in MogHouse                                       *
*																		*
************************************************************************/

void CZone::SpawnMoogle(CCharEntity* PChar)
{
	m_zoneEntities->SpawnMoogle(PChar);
}

/************************************************************************
*                                                                       *
*  Отображаем транспотр в зоне (не хранится в основном списке)          *
*  Displays transport at (not stored in the main list)					*
*                                                                       *
************************************************************************/

void CZone::SpawnTransport(CCharEntity* PChar)
{
	m_zoneEntities->SpawnTransport(PChar);
}

/************************************************************************
*																		*
*  Получаем указатель на любую сущность в зоне по ее targid				*
*  Get a pointer to any entity in the area for its targid               *
*																		*
************************************************************************/

CBaseEntity* CZone::GetEntity(uint16 targid, uint8 filter)
{
	return m_zoneEntities->GetEntity(targid, filter);
}

/************************************************************************
*																		*
*  Oбработка реакции мира на смену времени суток						*
*  Machining World reaction to change the time of day                   *
*																		*
************************************************************************/

void CZone::TOTDChange(TIMETYPE TOTD)
{
	m_zoneEntities->TOTDChange(TOTD);

    luautils::OnTOTDChange(m_zoneID, TOTD);
}

/************************************************************************
*                                                                       *
*  Save a characters playtime to the database.                          *
*                                                                       *
************************************************************************/

void CZone::SavePlayTime()
{
	m_zoneEntities->SavePlayTime();
}

/************************************************************************
*                                                                       *
*  Return the pointer to the character by Name                          *
*                                                                       *
************************************************************************/

CCharEntity* CZone::GetCharByName(int8* name)
{
	return m_zoneEntities->GetCharByName(name);
}

/************************************************************************
*																		*
*  Отправляем глобальные пакеты											*
*  Send the global package                                              *
*																		*
************************************************************************/

void CZone::PushPacket(CBaseEntity* PEntity, GLOBAL_MESSAGE_TYPE message_type, CBasicPacket* packet)
{
	m_zoneEntities->PushPacket(PEntity, message_type, packet);
}

/************************************************************************
*																		*
*  Wide Scan															*
*																		*
************************************************************************/

void CZone::WideScan(CCharEntity* PChar, uint16 radius)
{
	m_zoneEntities->WideScan(PChar, radius);
}

/************************************************************************
*																		*
*  Cервер для обработки активности и статус-эффектов сущностей в зоне.	*
*  При любом раскладе последними должны обрабатываться персонажи		*
*																		*
*  Server for processing activities and the status of entities in the   *
*  area of ​​effect. In any case the latter should be handled characters  *
*                                                                       *
************************************************************************/

void CZone::ZoneServer(uint32 tick)
{
	m_zoneEntities->ZoneServer(tick);

	if (m_BattlefieldHandler != NULL)
	{
		m_BattlefieldHandler->handleBattlefields(tick);
	}
}

/************************************************************************
*																		*
*  Cервер для обработки активности и статус-эффектов сущностей в зоне.	*
*  Дополнительно обрабатывается проверка на вход и выход персонажей из	*
*  активных областей (пока реализован только вход в область).			*
*  При любом раскладе последними должны обрабатываться персонажи		*
*																		*
*  Server for processing activities and the status of entities in the   *
*  area of ​​effect. Additionally, processed check in and out of          *
*  characters active regions (yet realized only entrance to the area).  *
*  In any case the latter should be handled characters                  *
*                                                                       *
************************************************************************/

void CZone::ZoneServerRegion(uint32 tick)
{
	m_zoneEntities->ZoneServerRegion(tick);
}

void CZone::ForEachChar(std::function<void(CCharEntity*)> func)
{
	for (auto PChar : m_zoneEntities->GetCharList())
	{
		func((CCharEntity*)PChar.second);
	}
}

void CZone::ForEachCharInstance(CBaseEntity* PEntity, std::function<void(CCharEntity*)> func)
{
	for (auto PChar : m_zoneEntities->GetCharList())
	{
		func((CCharEntity*)PChar.second);
	}
}

void CZone::ForEachMobInstance(CBaseEntity* PEntity, std::function<void(CMobEntity*)> func)
{
	for (auto PMob : m_zoneEntities->m_mobList)
	{
		func((CMobEntity*)PMob.second);
	}
}

void CZone::createZoneTimer()
{
	ZoneTimer = CTaskMgr::getInstance()->AddTask(
		m_zoneName,
		gettick(),
		this,
		CTaskMgr::TASK_INTERVAL,
		m_regionList.empty() ? zone_server : zone_server_region,
		500);
}

void CZone::CharZoneIn(CCharEntity* PChar)
{
	// ищем свободный targid для входящего в зону персонажа

	PChar->loc.zone = this;
	PChar->loc.zoning = false;
	PChar->loc.destination = 0;
	PChar->m_InsideRegionID = 0;

	PChar->m_PVPFlag = CanUseMisc(MISC_PVP);

	//remove temp items
	charutils::ClearTempItems(PChar);

	if (PChar->animation == ANIMATION_CHOCOBO && !CanUseMisc(MISC_CHOCOBO))
	{
		PChar->animation = ANIMATION_NONE;
		PChar->StatusEffectContainer->DelStatusEffectSilent(EFFECT_CHOCOBO);
	}
	if (PChar->m_Costum != 0)
	{
		PChar->m_Costum = 0;
		PChar->StatusEffectContainer->DelStatusEffect(EFFECT_COSTUME);
	}
	if (PChar->PParty != NULL)
	{
		if (m_TreasurePool != NULL)
		{
			PChar->PTreasurePool = m_TreasurePool;
			PChar->PTreasurePool->AddMember(PChar);
		}
		else
		{
			PChar->PParty->ReloadTreasurePool(PChar);
		}
		if (PChar->PParty->GetSyncTarget() != NULL)
		{
			if (PChar->getZone() == PChar->PParty->GetSyncTarget()->getZone())
			{
				if (PChar->PParty->GetSyncTarget()->StatusEffectContainer->HasStatusEffect(EFFECT_LEVEL_SYNC) &&
					PChar->PParty->GetSyncTarget()->StatusEffectContainer->GetStatusEffect(EFFECT_LEVEL_SYNC)->GetDuration() == 0)
				{
					PChar->pushPacket(new CMessageBasicPacket(PChar, PChar, 0, PChar->PParty->GetSyncTarget()->GetMLevel(), 540));
					PChar->StatusEffectContainer->AddStatusEffect(new CStatusEffect(
						EFFECT_LEVEL_SYNC,
						EFFECT_LEVEL_SYNC,
						PChar->PParty->GetSyncTarget()->GetMLevel(),
						0,
						0), true);
					PChar->StatusEffectContainer->DelStatusEffectsByFlag(EFFECTFLAG_DEATH);
				}
			}
		}
	}
	else
	{
		PChar->PTreasurePool = new CTreasurePool(TREASUREPOOL_SOLO);
		PChar->PTreasurePool->AddMember(PChar);
	}

	if (m_zoneType != ZONETYPE_DUNGEON_INSTANCED)
	{
		PChar->PInstance = NULL;
	}

	PChar->PLatentEffectContainer->CheckLatentsZone();
}

void CZone::CharZoneOut(CCharEntity* PChar)
{
	for (regionList_t::const_iterator region = m_regionList.begin(); region != m_regionList.end(); ++region)
	{
		if ((*region)->GetRegionID() == PChar->m_InsideRegionID)
		{
			luautils::OnRegionLeave(PChar, *region);
			break;
		}
	}

	if (PChar->m_LevelRestriction != 0)
	{
		if (PChar->PParty)
		{
			if (PChar->PParty->GetSyncTarget() == PChar || PChar->PParty->GetLeader() == PChar)
			{
				PChar->PParty->SetSyncTarget(NULL, 551);
			}
			if (PChar->PParty->GetSyncTarget() != NULL)
			{
				uint8 count = 0;
				for (uint32 i = 0; i < PChar->PParty->members.size(); ++i)
				{
					if (PChar->PParty->members.at(i) != PChar && PChar->PParty->members.at(i)->getZone() == PChar->PParty->GetSyncTarget()->getZone())
					{
						count++;
					}
				}
				if (count < 2) //3, because one is zoning out - thus at least 2 will be left
				{
					PChar->PParty->SetSyncTarget(NULL, 552);
				}
			}
		}
		PChar->StatusEffectContainer->DelStatusEffectSilent(EFFECT_LEVEL_SYNC);
		PChar->StatusEffectContainer->DelStatusEffectSilent(EFFECT_LEVEL_RESTRICTION);
	}

	//remove status effects that wear on zone
	PChar->StatusEffectContainer->DelStatusEffectsByFlag(EFFECTFLAG_ON_ZONE, true);

	if (PChar->PTreasurePool != NULL) // TODO: условие для устранения проблем с MobHouse, надо блин решить ее раз и навсегда
	{
		PChar->PTreasurePool->DelMember(PChar);
	}

    if (PChar->isDead())
        charutils::SaveDeathTime(PChar);

	PChar->loc.zone = NULL;
	PChar->loc.prevzone = m_zoneID;

	PChar->SpawnPCList.clear();
	PChar->SpawnNPCList.clear();
	PChar->SpawnMOBList.clear();
	PChar->SpawnPETList.clear();
}

void CZone::CheckRegions(CCharEntity* PChar)
{
	uint32 RegionID = 0;

	for (regionList_t::const_iterator region = m_regionList.begin(); region != m_regionList.end(); ++region)
	{
		if ((*region)->isPointInside(PChar->loc.p))
		{
			RegionID = (*region)->GetRegionID();

			if ((*region)->GetRegionID() != PChar->m_InsideRegionID)
			{
				luautils::OnRegionEnter(PChar, *region);
			}
			if (PChar->m_InsideRegionID == 0) break;
		}
		else if ((*region)->GetRegionID() == PChar->m_InsideRegionID)
		{
			luautils::OnRegionLeave(PChar, *region);
		}
	}
	PChar->m_InsideRegionID = RegionID;
}

//===========================================================

/*
id				CBaseEntity
name			CBaseEntity
pos_rot			CBaseEntity
pos_x			CBaseEntity
pos_y			CBaseEntity
pos_z			CBaseEntity
speed			CBaseEntity
speedsub		CBaseEntity
animation		CBaseEntity
animationsub	CBaseEntity
namevis			npc+mob
status			CBaseEntity
unknown
look			CBaseEntity
name_prefix
*/
