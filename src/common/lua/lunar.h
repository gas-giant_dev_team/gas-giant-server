﻿#pragma once
#include "lua.hpp"
/*Связыватель объектов C++ с объектами Lua*/
/* Binder C ++ objects with objects Lua */
template <typename T> class Lunar {

  /*Структура инкапсулирования объекта C++ в объект Lua*/
  /* Structure C ++ object encapsulation in object Lua */
  struct user_t
  {
	  T *pT;
  };
public:
  typedef int (T::*mfp)(lua_State *L);

  /* Стуктура для склеивания методов объекта Lua с методами объекта C++*/
  /* Organizational Structure for bonding methods Lua object to the methods of the object C ++ */
  struct Register_t
  {
	  const char *name;
	  mfp mfunc;
  };

  static void Register(lua_State *L) {

    /* [1] - таблица методов объекта table object methods */
    lua_newtable(L);
    int methods = lua_gettop(L);

	/* [2] - метатаблица будущих обектов metatable future objec */
    luaL_newmetatable(L, T::className);
    int metatable = lua_gettop(L);

    // Размещение таблицы методов в глобальной переменной
	// для того, чтобы код lua мог добавлять методы.
    // Placement method table in the global variable
    // To lua code could add methods.
    lua_pushvalue(L, methods);
    lua_setglobal(L, T::className);

    // прячем метатаблицу от getmetattable()
    // Hide metatable from getmetattable ()
    lua_pushvalue(L, methods);
    set(L, metatable, "__metatable");

	// Задаем значение поля __index мета таблицы
	// адресом таблицы методов, для того чтобы
	// можно было использовать синтекс obj:method()
    // Set field value __index meta table
    // Address table methods to
    // Be used Syntex obj: method ()
    lua_pushvalue(L, methods);
    set(L, metatable, "__index");

	// Задаем значение поля __tostring мета таблицы,
	// для того, чтобы можно было выводить объект в текстовом виде.
    // Set field value __tostring meta table
    // In order to be able to display the object in a tabular format.
    lua_pushcfunction(L, tostring_T);
    set(L, metatable, "__tostring");

	// Задаем значение поля __gc  мета таблицы Set field value __gc meta table
	// для того, чтобы сборщик мусора мог удалить нанаши объекты
    // To the garbage collector can remove objects nanashi
    lua_pushcfunction(L, gc_T);
    set(L, metatable, "__gc");

	// Добавляем новую таблицу для того, чтобы Add a new table to
	// сделать её метатаблице таблицы методов. Make it metatable method table.
    lua_newtable(L);                // mt
	// Добавление функции сосздания объекта
    // Add soszdaniya function object
    lua_pushcfunction(L, new_T);
    lua_pushvalue(L, -1);           // копирование адреса функции для двух случаев. Copy function address for the two cases.
	// задаем метод new у таблицы, для реализации конструктора
    // Set the new method in the table for the implementation of the constructor
	// Class:new()
    set(L, methods, "new");         //
	// Задаем метод __call у мета таблицы для того, чтобы Set the __call in meta table to
	// можно было использовать метод Class(). Could use the method Class ().
    set(L, -3, "__call");           // mt.__call = new_T
    lua_setmetatable(L, methods);

    // Заполнение таблицы lua, методами из класса.
    // Filling a table lua, the methods of the class.
    for (Register_t *l = T::methods; l->name; ++l) {
	  // добавление имени метода Adding the method name
      lua_pushstring(L, l->name);
	  // добавление склеивающего параметра. Adding gluing parameter.
      lua_pushlightuserdata(L, (void*)l);
	   // добавление специального склеивателя. Adding a special sealer.
      lua_pushcclosure(L, thunk, 1);

      lua_settable(L, methods);
    }

    lua_pop(L, 2);
  }

  // Вызов метода из user_t Calling from user_t
  static int call(lua_State *L, const char *method,
                  int nargs=0, int nresults=LUA_MULTRET, int errfunc=0)
  {
	// Идекс в стеке для user_t Idex stack for user_t
    int base = lua_gettop(L) - nargs;
	// Проверка типа user_t Type checking user_t
    if (!luaL_checkudata(L, base, T::className)) {
      lua_settop(L, base-1);           // Удаление всех данных Delete All Data
      lua_pushfstring(L, "not a valid %s userdata", T::className);
      return -1;
    }

    lua_pushstring(L, method);         // имя метода Method name
    lua_gettable(L, base);             // получить method из userdata Get method of userdata
    if (lua_isnil(L, -1)) {            // Метод найден? method found?
      lua_settop(L, base-1);           // Удаление всех данных Delete All Data
      lua_pushfstring(L, "%s missing method '%s'", T::className, method);
      return -1;
    }
    lua_insert(L, base);               // put method under userdata, args

    int status = lua_pcall(L, 1+nargs, nresults, errfunc);  // Вызов метода Calling
    if (status) {
      const char *msg = lua_tostring(L, -1);
      if (msg == NULL) msg = "(error with no message)";
      lua_pushfstring(L, "%s:%s status = %d\n%s",
                      T::className, method, status, msg);
      lua_remove(L, base);             // remove old message Remove old message
      return -1;
    }
    return lua_gettop(L) - base + 1;   // кол-во возвращенных результатов Number of results returned
  }

  // Добавление в стек пользовательского типа данных, содержащего указатель на
  // Add to stack the custom data type contains a pointer to
  // T *obj
  static int push(lua_State *L, T *obj, bool gc=false) {
    if (!obj)
	{
		lua_pushnil(L);
		return 0;
	}
    luaL_getmetatable(L, T::className);  // поиск мета-таблицы в реестре. Search meta-table in the registry.
    if (lua_isnil(L, -1)) luaL_error(L, "%s missing metatable", T::className);
    int mt = lua_gettop(L);
    subtable(L, mt, "userdata", "v");

	user_t *ud =
      static_cast<user_t*>(pushuserdata(L, obj, sizeof(user_t)));
    if (ud) {
      ud->pT = obj;  // размещение указателя в user_t Accommodation pointer user_t
      lua_pushvalue(L, mt);
      lua_setmetatable(L, -2);
      if (gc == false) {
        lua_checkstack(L, 3);
        subtable(L, mt, "do not trash", "k");
        lua_pushvalue(L, -2);
        lua_pushboolean(L, 1);
        lua_settable(L, -3);
        lua_pop(L, 1);
      }
    }
    lua_replace(L, mt);
    lua_settop(L, mt);
    return mt;  // index  userdata содержит указатель на T *obj Index userdata contains a pointer to T * obj
  }

  // возврат T* из стека Return T * from the stack
  static T *check(lua_State *L, int narg) {
    user_t *ud =
      static_cast<user_t*>(luaL_checkudata(L, narg, T::className));
    if(!ud) {
        luaL_typerror(L, narg, T::className);
        return NULL;
    }
    return ud->pT;  // pointer to T object
  }

private:
  Lunar();  // прячем конструктор по умолчанию. Hide the default constructor.

  // распаковщик функции члена. Extractor member function.
  static int thunk(lua_State *L) {
    // стек содержит user_t, следующим прямо за аргументами.
    // Stack contains user_t, following directly behind the arguments.
    T *obj = check(L, 1);
    lua_remove(L, 1);
    // Получаем связанное с распаковщиком значение registration
    // Get the value associated with the extractor registration
    Register_t *l = static_cast<Register_t*>(lua_touserdata(L, lua_upvalueindex(1)));
    return (obj->*(l->mfunc))(L);  // Вызов метода объекта. Call an object method.
  }

  // Создание нового объекта и добавление его на вершину стека
  // Create a new object and adding it to the top of the stack
  static int new_T(lua_State *L) {
    lua_remove(L, 1);   // удаление 'self' Remove 'self'
    T *obj = new T(L);  // Вызов конструктора Call the constructor
    push(L, obj, true); // gc_T удалит этот объект когда надо Gc_T delete this object when it is necessary
    return 1;
  }

  // сборщик мусора Garbage collector
  static int gc_T(lua_State *L) {
    if (luaL_getmetafield(L, 1, "do not trash")) {
      lua_pushvalue(L, 1);  // dup userdata
      lua_gettable(L, -2);
      if (!lua_isnil(L, -1)) return 0;  // do not delete object
    }
    user_t *ud = static_cast<user_t*>(lua_touserdata(L, 1));
    T *obj = ud->pT;
    if (obj) delete obj;
    return 0;
  }

  static int tostring_T (lua_State *L) {
    char buff[32];
    user_t *ud = static_cast<user_t*>(lua_touserdata(L, 1));
    T *obj = ud->pT;
    sprintf(buff, "%p", (void*)obj);
    lua_pushfstring(L, "%s (%s)", T::className, buff);
    return 1;
  }

  static void set(lua_State *L, int table_index, const char *key) {
    lua_pushstring(L, key);
    lua_insert(L, -2);  // swap value and key
    lua_settable(L, table_index);
  }

  static void weaktable(lua_State *L, const char *mode) {
    lua_newtable(L);
    lua_pushvalue(L, -1);  // table is its own metatable
    lua_setmetatable(L, -2);
    lua_pushliteral(L, "__mode");
    lua_pushstring(L, mode);
    lua_settable(L, -3);   // metatable.__mode = mode
  }

  static void subtable(lua_State *L, int tindex, const char *name, const char *mode) {
    lua_pushstring(L, name);
    lua_gettable(L, tindex);
    if (lua_isnil(L, -1)) {
      lua_pop(L, 1);
      lua_checkstack(L, 3);
      weaktable(L, mode);
      lua_pushstring(L, name);
      lua_pushvalue(L, -2);
      lua_settable(L, tindex);
    }
  }

  static void *pushuserdata(lua_State *L, void *key, size_t sz) {
    void *ud = 0;
    lua_pushlightuserdata(L, key);
    lua_gettable(L, -2);     // lookup[key]
    if (lua_isnil(L, -1)) {
      lua_pop(L, 1);         // drop nil
      lua_checkstack(L, 3);
      ud = lua_newuserdata(L, sz);  // create new userdata
      lua_pushlightuserdata(L, key);
      lua_pushvalue(L, -2);  // dup userdata
      lua_settable(L, -4);   // lookup[key] = userdata
    }
    return ud;
  }
};

#define LUNAR_DECLARE_METHOD(Class, Name) {#Name, &Class::Name}
