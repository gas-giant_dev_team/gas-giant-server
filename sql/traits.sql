/*
MySQL Data Transfer
Source Host: localhost
Source Database: dspdb
Target Host: localhost
Target Database: dspdb
Date: 2/10/2014 12:06:00 AM
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for traits
-- ----------------------------
DROP TABLE IF EXISTS `traits`;
CREATE TABLE `traits` (
  `traitid` tinyint(3) unsigned NOT NULL,
  `name` text NOT NULL,
  `job` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(2) unsigned NOT NULL DEFAULT '99',
  `rank` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `modifier` smallint(5) unsigned NOT NULL DEFAULT '0',
  `value` smallint(5) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Traits Records
-- ----------------------------

-- INSERT INTO `traits` VALUES ('55', 'resist curse', '0', '0', '248', '2');
-- INSERT INTO `traits` VALUES ('56', 'resist stun', '0', '0', '251', '2');
-- INSERT INTO `traits` VALUES ('60', 'resist charm', '0', '0', '252', '2');

-- ------------------------------------------------------------
-- Warrior
-- ------------------------------------------------------------

INSERT INTO `traits` VALUES ('4', 'defense bonus', '1', '10', '1', '1', '10');
INSERT INTO `traits` VALUES ('54', 'resist virus', '1', '15', '1', '245', '2');
INSERT INTO `traits` VALUES ('15', 'double attack', '1', '25', '1', '288', '10');
INSERT INTO `traits` VALUES ('3', 'attack bonus', '1', '30', '1', '23', '10');
INSERT INTO `traits` VALUES ('3', 'attack bonus', '1', '30', '1', '24', '10');
INSERT INTO `traits` VALUES ('54', 'resist virus', '1', '35', '2', '245', '5');
-- INSERT INTO `traits` VALUES ('107', 'fencer', '1', '45', '1', '165', '5'); -- Main Hand 1 Weapon Only
-- INSERT INTO `traits` VALUES ('107', 'fencer', '1', '45', '1', '?', '20'); -- WS TP Bonus
INSERT INTO `traits` VALUES ('54', 'resist virus', '1', '55', '3', '245', '10');
-- INSERT INTO `traits` VALUES ('107', 'fencer', '1', '58', '2', '165', '6'); -- Main Hand 1 Weapon Only
-- INSERT INTO `traits` VALUES ('107', 'fencer', '1', '58', '2', '?', '30'); -- WS TP Bonus
INSERT INTO `traits` VALUES ('54', 'resist virus', '1', '70', '4', '245', '20');
-- INSERT INTO `traits` VALUES ('107', 'fencer', '1', '71', '3', '165', '7'); -- Main Hand 1 Weapon Only
-- INSERT INTO `traits` VALUES ('107', 'fencer', '1', '71', '3', '?', '40'); -- WS TP Bonus
INSERT INTO `traits` VALUES ('71', 'savagery', '1', '75', '1', '0', '0'); -- Merit
INSERT INTO `traits` VALUES ('72', 'aggressive aim', '1', '75', '1', '0', '0'); -- Merit
INSERT INTO `traits` VALUES ('98', 'crit. atk. bonus', '1', '78', '1', '459', '5');
INSERT INTO `traits` VALUES ('102', 'shield def. bonus', '1', '80', '1', '460', '6');
-- INSERT INTO `traits` VALUES ('107', 'fencer', '1', '84', '4', '165', '8'); -- Main Hand 1 Weapon Only
-- INSERT INTO `traits` VALUES ('107', 'fencer', '1', '84', '4', '?', '45'); -- WS TP Bonus
INSERT INTO `traits` VALUES ('4', 'defense bonus', '1', '86', '2', '1', '22');
INSERT INTO `traits` VALUES ('98', 'crit. atk. bonus', '1', '86', '2', '459', '8');
INSERT INTO `traits` VALUES ('3', 'attack bonus', '1', '91', '2', '23', '22');
INSERT INTO `traits` VALUES ('3', 'attack bonus', '1', '91', '2', '24', '22');
-- INSERT INTO `traits` VALUES ('107', 'fencer', '1', '97', '5', '165', '9'); -- Main Hand 1 Weapon Only
-- INSERT INTO `traits` VALUES ('107', 'fencer', '1', '97', '5', '?', '50'); -- WS TP Bonus

-- ------------------------------------------------------------
-- Monk
-- ------------------------------------------------------------

INSERT INTO `traits` VALUES ('23', 'martial arts', '2', '1', '1', '173', '80');
INSERT INTO `traits` VALUES ('67', 'subtle blow', '2', '5', '1', '289', '5');
INSERT INTO `traits` VALUES ('17', 'counter', '2', '10', '1', '291', '10');
INSERT INTO `traits` VALUES ('7', 'max hp boost', '2', '15', '1', '2', '30');
INSERT INTO `traits` VALUES ('23', 'martial arts', '2', '16', '2', '173', '100');
INSERT INTO `traits` VALUES ('67', 'subtle blow', '2', '25', '2', '289', '10');
INSERT INTO `traits` VALUES ('23', 'martial arts', '2', '31', '3', '173', '120');
INSERT INTO `traits` VALUES ('7', 'max hp boost', '2', '35', '2', '2', '60');
INSERT INTO `traits` VALUES ('67', 'subtle blow', '2', '45', '3', '289', '15');
INSERT INTO `traits` VALUES ('23', 'martial arts', '2', '46', '4', '173', '140');
INSERT INTO `traits` VALUES ('66', 'kick attacks', '2', '51', '1', '292', '10');
INSERT INTO `traits` VALUES ('7', 'max hp boost', '2', '55', '3', '2', '120');
INSERT INTO `traits` VALUES ('23', 'martial arts', '2', '61', '5', '173', '160');
INSERT INTO `traits` VALUES ('67', 'subtle blow', '2', '65', '4', '289', '20');
INSERT INTO `traits` VALUES ('7', 'max hp boost', '2', '70', '4', '2', '180');
INSERT INTO `traits` VALUES ('66', 'kick attacks', '2', '71', '2', '292', '13');
INSERT INTO `traits` VALUES ('23', 'martial arts', '2', '75', '6', '173', '180');
INSERT INTO `traits` VALUES ('73', 'invigorate', '2', '75', '1', '0', '24'); -- Merit
INSERT INTO `traits` VALUES ('74', 'penance', '2', '75', '1', '0', '20'); -- Merit
INSERT INTO `traits` VALUES ('101', 'tactical guard', '2', '77', '1', '462', '3');
INSERT INTO `traits` VALUES ('17', 'counter', '2', '81', '2', '291', '2');
INSERT INTO `traits` VALUES ('23', 'martial arts', '2', '82', '7', '173', '200');
INSERT INTO `traits` VALUES ('106', 'skillchain bonus', '2', '85', '1', '174', '8');
INSERT INTO `traits` VALUES ('7', 'max hp boost', '2', '86', '5', '2', '240');
INSERT INTO `traits` VALUES ('101', 'tactical guard', '2', '2', '87', '462', '5');
INSERT INTO `traits` VALUES ('67', 'subtle blow', '2', '91', '5', '289', '25');
INSERT INTO `traits` VALUES ('106', 'skillchain bonus', '2', '95', '2', '174', '4');
INSERT INTO `traits` VALUES ('7', 'max hp boost', '2', '96', '6', '2', '280');
INSERT INTO `traits` VALUES ('66', 'kick attacks', '2', '96', '2', '292', '15');
INSERT INTO `traits` VALUES ('101', 'tactical guard', '2', '97', '3', '462', '6');

-- ------------------------------------------------------------
-- White Mage
-- ------------------------------------------------------------

INSERT INTO `traits` VALUES ('6', 'magic def. bonus', '3', '10', '1', '29', '10');
INSERT INTO `traits` VALUES ('24', 'clear mind', '3', '20', '1', '71', '3');
INSERT INTO `traits` VALUES ('114', 'tranquil heart', '3', '21', '1', '563', '1');
INSERT INTO `traits` VALUES ('9', 'auto regen', '3', '25', '1', '370', '1');
INSERT INTO `traits` VALUES ('6', 'magic def. bonus', '3', '30', '2', '29', '12');
INSERT INTO `traits` VALUES ('24', 'clear mind', '3', '35', '2', '71', '6');
INSERT INTO `traits` VALUES ('24', 'clear mind', '3', '50', '3', '71', '9');
INSERT INTO `traits` VALUES ('24', 'clear mind', '3', '50', '4', '295', '1');
INSERT INTO `traits` VALUES ('69', 'divine veil', '3', '50', '1', '632', '0');
INSERT INTO `traits` VALUES ('6', 'magic def. bonus', '3', '50', '3', '29', '14');
INSERT INTO `traits` VALUES ('111', 'divine benison', '3', '50', '1', '564', '10');
INSERT INTO `traits` VALUES ('111', 'divine benison', '3', '50', '1', '565', '-5');
INSERT INTO `traits` VALUES ('111', 'divine benison', '3', '60', '2', '564', '20');
INSERT INTO `traits` VALUES ('111', 'divine benison', '3', '60', '2', '565', '-10');
INSERT INTO `traits` VALUES ('24', 'clear mind', '3', '65', '4', '71', '12');
INSERT INTO `traits` VALUES ('24', 'clear mind', '3', '65', '4', '295', '1');
INSERT INTO `traits` VALUES ('6', 'magic def. bonus', '3', '70', '4', '29', '16');
INSERT INTO `traits` VALUES ('111', 'divine benison', '3', '70', '3', '564', '30');
INSERT INTO `traits` VALUES ('111', 'divine benison', '3', '70', '3', '565', '-15');
INSERT INTO `traits` VALUES ('9', 'auto regen', '3', '76', '2', '370', '2');
INSERT INTO `traits` VALUES ('111', 'divine benison', '3', '80', '4', '564', '40');
INSERT INTO `traits` VALUES ('111', 'divine benison', '3', '80', '4', '565', '-20');
INSERT INTO `traits` VALUES ('6', 'magic def. bonus', '3', '81', '5', '29', '18');
INSERT INTO `traits` VALUES ('102', 'shield def bonus', '3', '85', '1', '460', '6');
INSERT INTO `traits` VALUES ('111', 'divine benison', '3', '90', '5', '564', '50');
INSERT INTO `traits` VALUES ('111', 'divine benison', '3', '90', '5', '565', '-25');
INSERT INTO `traits` VALUES ('6', 'magic def. bonus', '3', '91', '6', '29', '20');

-- ------------------------------------------------------------
-- Black Mage
-- ------------------------------------------------------------

INSERT INTO `traits` VALUES ('5', 'magic atk. bonus', '4', '10', '1', '28', '20');
INSERT INTO `traits` VALUES ('24', 'clear mind', '4', '15', '1', '71', '3');
INSERT INTO `traits` VALUES ('13', 'conserve mp', '4', '20', '1', '296', '25');
INSERT INTO `traits` VALUES ('24', 'clear mind', '4', '30', '2', '71', '6');
INSERT INTO `traits` VALUES ('5', 'magic atk. bonus', '4', '30', '2', '28', '24');
INSERT INTO `traits` VALUES ('24', 'clear mind', '4', '45', '3', '71', '9');
INSERT INTO `traits` VALUES ('24', 'clear mind', '4', '45', '3', '295', '1');
INSERT INTO `traits` VALUES ('110', 'mag. burst bonus', '4', '45', '1', '414', '5');
INSERT INTO `traits` VALUES ('5', 'magic atk. bonus', '4', '50', '3', '28', '28');
INSERT INTO `traits` VALUES ('112', 'elemental celerity', '4', '50', '1', '403', '10');
INSERT INTO `traits` VALUES ('110', 'mag. burst bonus', '4', '58', '2', '414', '7');
INSERT INTO `traits` VALUES ('24', 'clear mind', '4', '60', '4', '71', '12');
INSERT INTO `traits` VALUES ('24', 'clear mind', '4', '60', '4', '295', '1');
INSERT INTO `traits` VALUES ('112', 'elemental celerity', '4', '60', '2', '403', '15');
INSERT INTO `traits` VALUES ('5', 'magic atk. bonus', '4', '70', '4', '28', '32');
INSERT INTO `traits` VALUES ('112', 'elemental celerity', '4', '70', '3', '403', '20');
INSERT INTO `traits` VALUES ('110', 'mag. burst bonus', '4', '71', '3', '414', '9');
INSERT INTO `traits` VALUES ('24', 'clear mind', '4', '75', '5', '71', '15');
INSERT INTO `traits` VALUES ('24', 'clear mind', '4', '75', '5', '295', '2');
INSERT INTO `traits` VALUES ('112', 'elemental celerity', '4', '80', '4', '403', '25');
INSERT INTO `traits` VALUES ('5', 'magic atk. bonus', '4', '81', '5', '28', '36');
INSERT INTO `traits` VALUES ('110', 'mag. burst bonus', '4', '84', '4', '414', '11');
INSERT INTO `traits` VALUES ('109', 'occult acumen', '4', '85', '1', '464', '25');
INSERT INTO `traits` VALUES ('112', 'elemental celerity', '4', '90', '5', '403', '30');
INSERT INTO `traits` VALUES ('5', 'magic atk. bonus', '4', '91', '6', '28', '40');
INSERT INTO `traits` VALUES ('109', 'occult acumen', '4', '95', '2', '464', '50');
INSERT INTO `traits` VALUES ('110', 'mag. burst bonus', '4', '97', '5', '414', '13');

-- ------------------------------------------------------------
-- Red Mage
-- ------------------------------------------------------------

INSERT INTO `traits` VALUES ('53', 'resist petrify', '5', '10', '1', '246', '5');
INSERT INTO `traits` VALUES ('12', 'fast cast', '5', '15', '1', '170', '10');
INSERT INTO `traits` VALUES ('5', 'magic atk. bonus', '5', '20', '1', '28', '20');
INSERT INTO `traits` VALUES ('6', 'magic def. bonus', '5', '25', '1', '29', '10');
INSERT INTO `traits` VALUES ('114', 'tranquil heart', '5', '26', '1', '563', '1');
INSERT INTO `traits` VALUES ('53', 'resist petrify', '5', '30', '2', '246', '10');
INSERT INTO `traits` VALUES ('24', 'clear mind', '5', '31', '1', '71', '3');
INSERT INTO `traits` VALUES ('12', 'fast cast', '5', '35', '2', '170', '10');
INSERT INTO `traits` VALUES ('5', 'magic atk. bonus', '5', '40', '2', '28', '24');
INSERT INTO `traits` VALUES ('6', 'magic def. bonus', '5', '45', '2', '29', '12');
INSERT INTO `traits` VALUES ('53', 'resist petrify', '5', '50', '3', '246', '15');
INSERT INTO `traits` VALUES ('24', 'clear mind', '5', '53', '2', '71', '6');
INSERT INTO `traits` VALUES ('12', 'fast cast', '5', '55', '3', '170', '15');
INSERT INTO `traits` VALUES ('53', 'resist petrify', '5', '70', '4', '246', '20');
INSERT INTO `traits` VALUES ('24', 'clear mind', '5', '75', '3', '71', '9');
INSERT INTO `traits` VALUES ('24', 'clear mind', '5', '75', '3', '295', '1');
INSERT INTO `traits` VALUES ('12', 'fast cast', '5', '76', '4', '170', '20');
INSERT INTO `traits` VALUES ('110', 'mag. burst bonus', '5', '85', '1', '414', '5');
INSERT INTO `traits` VALUES ('5', 'magic atk. bonus', '5', '86', '3', '28', '28');
INSERT INTO `traits` VALUES ('25', 'shield mastery', '5', '87', '10', '461', '1');
INSERT INTO `traits` VALUES ('12', 'fast cast', '5', '89', '5', '170', '30');
INSERT INTO `traits` VALUES ('110', 'mag. burst bonus', '5', '95', '2', '414', '7');
INSERT INTO `traits` VALUES ('6', 'magic def. bonus', '5', '96', '3', '29', '14');
INSERT INTO `traits` VALUES ('25', 'shield mastery', '5', '97', '20', '461', '2');

-- ------------------------------------------------------------
-- Thief
-- ------------------------------------------------------------

INSERT INTO `traits` VALUES ('20', 'gilfinder', '6', '5', '1', '468', '1');
INSERT INTO `traits` VALUES ('2', 'evasion bonus', '6', '10', '1', '69', '10');
INSERT INTO `traits` VALUES ('19', 'treasure hunter', '6', '15', '1', '303', '1');
INSERT INTO `traits` VALUES ('58', 'resist gravity', '6', '20', '1', '249', '5');
INSERT INTO `traits` VALUES ('2', 'evasion bonus', '6', '30', '2', '69', '22');
INSERT INTO `traits` VALUES ('58', 'resist gravity', '6', '40', '2', '249', '10');
INSERT INTO `traits` VALUES ('64', 'treasure hunter ii', '6', '45', '2', '303', '1');
INSERT INTO `traits` VALUES ('2', 'evasion bonus', '6', '50', '3', '69', '35');
INSERT INTO `traits` VALUES ('16', 'triple attack', '6', '55', '1', '302', '5');
INSERT INTO `traits` VALUES ('68', 'assassin', '6', '60', '1', '0', '0');
INSERT INTO `traits` VALUES ('58', 'resist gravity', '6', '60', '3', '249', '15');
INSERT INTO `traits` VALUES ('2', 'evasion bonus', '6', '70', '4', '69', '48');
INSERT INTO `traits` VALUES ('58', 'resist gravity', '6', '75', '4', '249', '20');
INSERT INTO `traits` VALUES ('76', 'ambush', '6', '75', '1', '0', '0'); -- Merit
INSERT INTO `traits` VALUES ('75', 'aura steal', '6', '75', '1', '0', '0'); -- Merit
INSERT INTO `traits` VALUES ('98', 'crit. atk. bonus', '6', '78', '1', '459', '5');
INSERT INTO `traits` VALUES ('18', 'dual wield', '6', '83', '1', '259', '10');
INSERT INTO `traits` VALUES ('98', 'crit. atk. bonus', '6', '84', '2', '459', '8');
INSERT INTO `traits` VALUES ('65', 'gilfinder ii', '6', '85', '1', '468', '1');
INSERT INTO `traits` VALUES ('2', 'evasion bonus', '6', '86', '5', '69', '60');
INSERT INTO `traits` VALUES ('64', 'treasure hunter ii', '6', '90', '3', '303', '2');
INSERT INTO `traits` VALUES ('18', 'dual wield', '6', '90', '2', '259', '15');
INSERT INTO `traits` VALUES ('98', 'crit. atk. bonus', '6', '84', '3', '459', '11');
INSERT INTO `traits` VALUES ('18', 'dual wield', '6', '98', '3', '259', '25');
INSERT INTO `traits` VALUES ('16', 'triple attack', '6', '55', '2', '302', '6');
INSERT INTO `traits` VALUES ('98', 'crit. atk. bonus', '6', '84', '4', '459', '14');

-- ------------------------------------------------------------
-- Paladin
-- ------------------------------------------------------------

INSERT INTO `traits` VALUES ('39', 'undead killer', '7', '5', '1', '231', '8');
INSERT INTO `traits` VALUES ('4', 'defense bonus', '7', '10', '1', '1', '10');
INSERT INTO `traits` VALUES ('48', 'resist sleep', '7', '20', '1', '240', '5');
INSERT INTO `traits` VALUES ('25', 'shield mastery', '7', '25', '10', '461', '1');
INSERT INTO `traits` VALUES ('4', 'defense bonus', '7', '30', '2', '1', '22');
INSERT INTO `traits` VALUES ('10', 'auto refresh', '7', '35', '1', '369', '1');
INSERT INTO `traits` VALUES ('48', 'resist sleep', '7', '40', '2', '240', '10');
INSERT INTO `traits` VALUES ('4', 'defense bonus', '7', '50', '3', '1', '35');
INSERT INTO `traits` VALUES ('25', 'shield mastery', '7', '50', '20', '461', '2');
INSERT INTO `traits` VALUES ('48', 'resist sleep', '7', '60', '3', '240', '15');
INSERT INTO `traits` VALUES ('4', 'defense bonus', '7', '70', '4', '1', '48');
INSERT INTO `traits` VALUES ('25', 'shield mastery', '7', '75', '30', '461', '3');
INSERT INTO `traits` VALUES ('77', 'iron will', '7', '75', '1', '0', '0'); -- Merit
INSERT INTO `traits` VALUES ('78', 'guardian', '7', '75', '1', '0', '0'); -- Merit
INSERT INTO `traits` VALUES ('4', 'defense bonus', '7', '76', '5', '1', '60');
INSERT INTO `traits` VALUES ('102', 'shield def. bonus', '7', '77', '1', '460', '6');
-- INSERT INTO `traits` VALUES ('99', 'crit. def. bonus', '7', '79', '1', '?', '4'); -- Crit DEF DMG -4%
INSERT INTO `traits` VALUES ('4', 'defense bonus', '7', '91', '6', '1', '72');
INSERT INTO `traits` VALUES ('25', 'shield mastery', '7', '96', '40', '461', '4');

-- ------------------------------------------------------------
-- Dark Knight
-- ------------------------------------------------------------

INSERT INTO `traits` VALUES ('3', 'attack bonus', '8', '10', '1', '23', '10');
INSERT INTO `traits` VALUES ('3', 'attack bonus', '8', '10', '1', '24', '10');
INSERT INTO `traits` VALUES ('50', 'resist paralyze', '8', '20', '1', '242', '5');
INSERT INTO `traits` VALUES ('41', 'arcana killer', '8', '25', '1', '232', '8');
INSERT INTO `traits` VALUES ('3', 'attack bonus', '8', '30', '2', '23', '22');
INSERT INTO `traits` VALUES ('3', 'attack bonus', '8', '30', '2', '24', '22');
INSERT INTO `traits` VALUES ('50', 'resist paralyze', '8', '40', '2', '242', '10');
INSERT INTO `traits` VALUES ('109', 'occult acumen', '8', '45', '1', '464', '25');
INSERT INTO `traits` VALUES ('115', 'stalwart soul', '8', '45', '1', '469', '15');
INSERT INTO `traits` VALUES ('3', 'attack bonus', '8', '50', '3', '23', '35');
INSERT INTO `traits` VALUES ('3', 'attack bonus', '8', '50', '3', '24', '35');
INSERT INTO `traits` VALUES ('109', 'occult acumen', '8', '58', '2', '464', '50');
INSERT INTO `traits` VALUES ('50', 'resist paralyze', '8', '60', '3', '242', '15');
INSERT INTO `traits` VALUES ('115', 'stalwart soul', '8', '60', '2', '469', '30');
INSERT INTO `traits` VALUES ('3', 'attack bonus', '8', '70', '4', '23', '48');
INSERT INTO `traits` VALUES ('3', 'attack bonus', '8', '70', '4', '24', '48');
INSERT INTO `traits` VALUES ('109', 'occult acumen', '8', '71', '3', '464', '75');
INSERT INTO `traits` VALUES ('50', 'resist paralyze', '8', '75', '4', '242', '20');
INSERT INTO `traits` VALUES ('115', 'stalwart soul', '8', '75', '3', '469', '40');
INSERT INTO `traits` VALUES ('79', 'muted soul', '8', '75', '1', '0', '0'); -- Merit
INSERT INTO `traits` VALUES ('80', 'desperate blows', '8', '75', '1', '0', '0'); -- Merit
INSERT INTO `traits` VALUES ('3', 'attack bonus', '8', '76', '5', '23', '60');
INSERT INTO `traits` VALUES ('3', 'attack bonus', '8', '76', '5', '24', '60');
INSERT INTO `traits` VALUES ('109', 'occult acumen', '8', '84', '4', '464', '100');
INSERT INTO `traits` VALUES ('100', 'tactical parry', '8', '88', '10', '463', '2');
INSERT INTO `traits` VALUES ('115', 'stalwart soul', '8', '90', '4', '469', '50');
INSERT INTO `traits` VALUES ('3', 'attack bonus', '8', '91', '6', '23', '72');
INSERT INTO `traits` VALUES ('3', 'attack bonus', '8', '91', '6', '24', '72');
INSERT INTO `traits` VALUES ('109', 'occult acumen', '8', '97', '5', '464', '125');
INSERT INTO `traits` VALUES ('100', 'tactical parry', '8', '88', '20', '463', '3');

-- ------------------------------------------------------------
-- Beastmaster
-- ------------------------------------------------------------

INSERT INTO `traits` VALUES ('34', 'vermin killer', '9', '10', '1', '224', '8');
INSERT INTO `traits` VALUES ('59', 'resist slow', '9', '15', '1', '250', '5');
INSERT INTO `traits` VALUES ('61', 'resist amnesia', '9', '15', '1', '255', '5');
INSERT INTO `traits` VALUES ('36', 'bird killer', '9', '20', '1', '225', '8');
INSERT INTO `traits` VALUES ('37', 'amorph killer', '9', '30', '1', '226', '8');
INSERT INTO `traits` VALUES ('59', 'resist slow', '9', '35', '2', '250', '10');
INSERT INTO `traits` VALUES ('35', 'lizard killer', '9', '40', '1', '227', '8');
INSERT INTO `traits` VALUES ('38', 'aquan killer', '9', '50', '1', '228', '8');
INSERT INTO `traits` VALUES ('59', 'resist slow', '9', '55', '3', '250', '15');
INSERT INTO `traits` VALUES ('33', 'plantoid killer', '9', '60', '1', '229', '8');
INSERT INTO `traits` VALUES ('32', 'beast killer', '9', '70', '1', '230', '8');
INSERT INTO `traits` VALUES ('59', 'resist slow', '9', '75', '4', '250', '20');
INSERT INTO `traits` VALUES ('81', 'beast affinity ', '9', '75', '1', '0', '0'); -- Merit
INSERT INTO `traits` VALUES ('82', 'beast healer', '9', '75', '1', '0', '0'); -- Merit
-- INSERT INTO `traits` VALUES ('103', 'stout servant', '9', '78', '1', '?', '?'); -- Pet Dmg reduction -5%
-- INSERT INTO `traits` VALUES ('107', 'fencer', '9', '80', '1', '165', '5'); -- Main Hand 1 Weapon Only
-- INSERT INTO `traits` VALUES ('107', 'fencer', '9', '80', '1', '?', '20'); -- WS TP Bonus
-- INSERT INTO `traits` VALUES ('107', 'fencer', '9', '87', '2', '165', '6'); -- Main Hand 1 Weapon Only
-- INSERT INTO `traits` VALUES ('107', 'fencer', '9', '87', '2', '?', '30'); -- WS TP Bonus
-- INSERT INTO `traits` VALUES ('103', 'stout servant', '9', '88', '2', '?', '?'); -- Pet Dmg reduction -7%
-- INSERT INTO `traits` VALUES ('107', 'fencer', '9', '94', '3', '165', '7'); -- Main Hand 1 Weapon Only
-- INSERT INTO `traits` VALUES ('107', 'fencer', '9', '94', '3', '?', '40'); -- WS TP Bonus
-- INSERT INTO `traits` VALUES ('103', 'stout servant', '9', '98', '3', '?', '?'); -- Pet Dmg reduction -9%

-- ------------------------------------------------------------
-- Bard
-- ------------------------------------------------------------

INSERT INTO `traits` VALUES ('52', 'resist silence', '10', '5', '1', '244', '5');
INSERT INTO `traits` VALUES ('52', 'resist silence', '10', '25', '2', '244', '10');
INSERT INTO `traits` VALUES ('52', 'resist silence', '10', '45', '3', '244', '15');
INSERT INTO `traits` VALUES ('52', 'resist silence', '10', '65', '4', '244', '20');
-- INSERT INTO `traits` VALUES ('99', 'crit. def. bonus', '10', '80', '1', '?', '4'); -- Crit DEF DMG -4%
-- INSERT INTO `traits` VALUES ('107', 'fencer', '10', '85', '1', '165', '5'); -- Main Hand 1 Weapon Only
-- INSERT INTO `traits` VALUES ('107', 'fencer', '10', '85', '1', '?', '20'); -- WS TP Bonus
-- INSERT INTO `traits` VALUES ('107', 'fencer', '10', '95', '2', '165', '6'); -- Main Hand 1 Weapon Only
-- INSERT INTO `traits` VALUES ('107', 'fencer', '10', '95', '2', '?', '30'); -- WS TP Bonus

-- ------------------------------------------------------------
-- Ranger
-- ------------------------------------------------------------

INSERT INTO `traits` VALUES ('21', 'alertness', '11', '5', '1', '358', '1');
INSERT INTO `traits` VALUES ('1', 'accuracy bonus', '11', '10', '1', '25', '10');
INSERT INTO `traits` VALUES ('1', 'accuracy bonus', '11', '10', '1', '26', '10');
INSERT INTO `traits` VALUES ('11', 'rapid shot', '11', '15', '1', '359', '10');
INSERT INTO `traits` VALUES ('49', 'resist poison', '11', '20', '1', '241', '5');
INSERT INTO `traits` VALUES ('1', 'accuracy bonus', '11', '30', '2', '25', '22');
INSERT INTO `traits` VALUES ('1', 'accuracy bonus', '11', '30', '2', '26', '22');
INSERT INTO `traits` VALUES ('49', 'resist poison', '11', '40', '2', '241', '10');
INSERT INTO `traits` VALUES ('1', 'accuracy bonus', '11', '50', '3', '25', '35');
INSERT INTO `traits` VALUES ('1', 'accuracy bonus', '11', '50', '3', '26', '35');
INSERT INTO `traits` VALUES ('113', 'dead aim', '11', '50', '1', '567', '10');
INSERT INTO `traits` VALUES ('49', 'resist poison', '11', '60', '3', '241', '15');
INSERT INTO `traits` VALUES ('113', 'dead aim', '11', '60', '2', '567', '20');
INSERT INTO `traits` VALUES ('1', 'accuracy bonus', '11', '70', '4', '25', '48');
INSERT INTO `traits` VALUES ('1', 'accuracy bonus', '11', '70', '4', '26', '48');
INSERT INTO `traits` VALUES ('113', 'dead aim', '11', '70', '3', '567', '30');
INSERT INTO `traits` VALUES ('83', 'snapshot', '11', '75', '1', '0', '0'); -- Merit
INSERT INTO `traits` VALUES ('84', 'recycle', '11', '20', '1', '305', '10'); -- Merit
-- INSERT INTO `traits` VALUES ('84', 'true shot', '11', '78', '1', '?', '?'); -- Bonus Dmg to Ranged Atks at distance +3%
INSERT INTO `traits` VALUES ('108', 'conserve tp', '11', '80', '1', '566', '25');
INSERT INTO `traits` VALUES ('113', 'dead aim', '11', '80', '4', '567', '35');
INSERT INTO `traits` VALUES ('1', 'accuracy bonus', '11', '86', '5', '25', '60');
INSERT INTO `traits` VALUES ('1', 'accuracy bonus', '11', '86', '5', '26', '60');
-- INSERT INTO `traits` VALUES ('84', 'true shot', '11', '88', '2', '?', '?'); -- Bonus Dmg to Ranged Atks at distance +5%
INSERT INTO `traits` VALUES ('113', 'dead aim', '11', '90', '5', '567', '40');
-- INSERT INTO `traits` VALUES ('84', 'true shot', '11', '98', '3', '?', '?'); -- Bonus Dmg to Ranged Atks at distance +7%
INSERT INTO `traits` VALUES ('113', 'dead aim', '11', '99', '6', '567', '45');

-- ------------------------------------------------------------
-- Samurai
-- ------------------------------------------------------------

INSERT INTO `traits` VALUES ('51', 'resist blind', '12', '5', '1', '243', '5');
INSERT INTO `traits` VALUES ('14', 'store tp', '12', '10', '1', '73', '10');
INSERT INTO `traits` VALUES ('70', 'zanshin', '12', '20', '1', '306', '15');
INSERT INTO `traits` VALUES ('51', 'resist blind', '12', '25', '2', '243', '10');
INSERT INTO `traits` VALUES ('14', 'store tp', '12', '30', '2', '73', '15');
INSERT INTO `traits` VALUES ('70', 'zanshin', '12', '35', '2', '306', '25');
INSERT INTO `traits` VALUES ('42', 'demon killer', '12', '40', '1', '234', '8');
INSERT INTO `traits` VALUES ('51', 'resist blind', '12', '45', '3', '243', '15');
INSERT INTO `traits` VALUES ('14', 'store tp', '12', '50', '3', '73', '20');
INSERT INTO `traits` VALUES ('70', 'zanshin', '12', '50', '3', '306', '35');
INSERT INTO `traits` VALUES ('51', 'resist blind', '12', '65', '4', '243', '20');
INSERT INTO `traits` VALUES ('14', 'store tp', '12', '70', '4', '73', '25');
INSERT INTO `traits` VALUES ('70', 'zanshin', '12', '75', '4', '306', '45');
INSERT INTO `traits` VALUES ('85', 'ikishoten', '12', '75', '1', '0', '0'); -- Merit
INSERT INTO `traits` VALUES ('86', 'overwhelm', '12', '75', '1', '0', '0'); -- Merit
INSERT INTO `traits` VALUES ('106', 'skillchain bonus', '12', '78', '1', '174', '8');
INSERT INTO `traits` VALUES ('106', 'skillchain bonus', '12', '88', '2', '174', '12');
INSERT INTO `traits` VALUES ('14', 'store tp', '12', '90', '5', '73', '30');
INSERT INTO `traits` VALUES ('70', 'zanshin', '12', '95', '5', '306', '50');
INSERT INTO `traits` VALUES ('106', 'skillchain bonus', '12', '98', '3', '174', '16');

-- ------------------------------------------------------------
-- Ninja
-- ------------------------------------------------------------

INSERT INTO `traits` VALUES ('22', 'stealth', '13', '5', '1', '358', '3');
INSERT INTO `traits` VALUES ('57', 'resist bind', '13', '10', '1', '247', '5');
INSERT INTO `traits` VALUES ('18', 'dual wield', '13', '10', '1', '259', '10');
INSERT INTO `traits` VALUES ('67', 'subtle blow', '13', '15', '1', '289', '5');
INSERT INTO `traits` VALUES ('18', 'dual wield', '13', '25', '2', '259', '15');
INSERT INTO `traits` VALUES ('57', 'resist bind', '13', '30', '2', '247', '10');
INSERT INTO `traits` VALUES ('67', 'subtle blow', '13', '30', '2', '289', '10');
INSERT INTO `traits` VALUES ('18', 'dual wield', '13', '45', '3', '259', '25');
INSERT INTO `traits` VALUES ('67', 'subtle blow', '13', '45', '3', '289', '15');
INSERT INTO `traits` VALUES ('57', 'resist bind', '13', '50', '3', '247', '15');
INSERT INTO `traits` VALUES ('67', 'subtle blow', '13', '60', '4', '289', '20');
INSERT INTO `traits` VALUES ('18', 'dual wield', '13', '65', '4', '259', '30');
INSERT INTO `traits` VALUES ('57', 'resist bind', '13', '70', '4', '247', '20');
INSERT INTO `traits` VALUES ('67', 'subtle blow', '13', '75', '5', '289', '25');
INSERT INTO `traits` VALUES ('87', 'ninja tool expert.', '13', '75', '1', '308', '0'); -- Merit
INSERT INTO `traits` VALUES ('100', 'tactical parry', '13', '77', '10', '463', '2');
INSERT INTO `traits` VALUES ('110', 'mag. burst bonus', '13', '80', '1', '414', '5');
INSERT INTO `traits` VALUES ('106', 'skillchain bonus', '13', '85', '1', '174', '8');
INSERT INTO `traits` VALUES ('18', 'dual wield', '13', '85', '5', '259', '35');
INSERT INTO `traits` VALUES ('100', 'tactical parry', '13', '87', '20', '463', '3');
INSERT INTO `traits` VALUES ('110', 'mag. burst bonus', '13', '90', '2', '414', '7');
INSERT INTO `traits` VALUES ('67', 'subtle blow', '13', '91', '6', '289', '27');
INSERT INTO `traits` VALUES ('106', 'skillchain bonus', '13', '95', '2', '174', '12');
INSERT INTO `traits` VALUES ('100', 'tactical parry', '13', '97', '30', '463', '4');

-- ------------------------------------------------------------
-- Dragoon
-- ------------------------------------------------------------

INSERT INTO `traits` VALUES ('3', 'attack bonus', '14', '10', '1', '23', '10');
INSERT INTO `traits` VALUES ('3', 'attack bonus', '14', '10', '1', '24', '10');
INSERT INTO `traits` VALUES ('43', 'dragon killer', '14', '25', '1', '233', '8');
INSERT INTO `traits` VALUES ('1', 'accuracy bonus', '14', '30', '1', '25', '10');
INSERT INTO `traits` VALUES ('1', 'accuracy bonus', '14', '30', '1', '26', '10');
INSERT INTO `traits` VALUES ('108', 'conserve tp', '14', '45', '1', '566', '25');
INSERT INTO `traits` VALUES ('1', 'accuracy bonus', '14', '50', '2', '25', '22');
INSERT INTO `traits` VALUES ('1', 'accuracy bonus', '14', '50', '2', '26', '22');
INSERT INTO `traits` VALUES ('89', 'strafe', '14', '75', '1', '0', '0'); -- Merit
INSERT INTO `traits` VALUES ('88', 'empathy', '14', '75', '1', '0', '0'); -- Merit
INSERT INTO `traits` VALUES ('1', 'accuracy bonus', '14', '76', '3', '25', '35');
INSERT INTO `traits` VALUES ('1', 'accuracy bonus', '14', '76', '3', '26', '35');
-- INSERT INTO `traits` VALUES ('99', 'crit. def. bonus', '14', '85', '1', '?', '4'); -- Crit DEF DMG -4%
INSERT INTO `traits` VALUES ('3', 'attack bonus', '14', '91', '2', '23', '22');
INSERT INTO `traits` VALUES ('3', 'attack bonus', '14', '91', '2', '24', '22');

-- ------------------------------------------------------------
-- Summoner
-- ------------------------------------------------------------

INSERT INTO `traits` VALUES ('8', 'max mp boost', '15', '10', '1', '5', '10');
INSERT INTO `traits` VALUES ('24', 'clear mind', '15', '15', '1', '71', '3');
INSERT INTO `traits` VALUES ('59', 'resist slow', '15', '20', '1', '250', '5');
INSERT INTO `traits` VALUES ('10', 'auto refresh', '15', '25', '1', '369', '1');
INSERT INTO `traits` VALUES ('8', 'max mp boost', '15', '30', '2', '5', '20');
INSERT INTO `traits` VALUES ('24', 'clear mind', '15', '30', '2', '71', '6');
INSERT INTO `traits` VALUES ('59', 'resist slow', '15', '40', '2', '250', '10');
INSERT INTO `traits` VALUES ('24', 'clear mind', '15', '45', '3', '71', '9');
INSERT INTO `traits` VALUES ('24', 'clear mind', '15', '45', '3', '295', '1');
INSERT INTO `traits` VALUES ('8', 'max mp boost', '15', '50', '3', '5', '40');
INSERT INTO `traits` VALUES ('24', 'clear mind', '15', '60', '4', '71', '12');
INSERT INTO `traits` VALUES ('24', 'clear mind', '15', '60', '4', '295', '1');
INSERT INTO `traits` VALUES ('59', 'resist slow', '15', '60', '3', '250', '15');
INSERT INTO `traits` VALUES ('105', 'blood boon', '15', '60', '1', '568', '25');
INSERT INTO `traits` VALUES ('8', 'max mp boost', '15', '70', '4', '5', '60');
INSERT INTO `traits` VALUES ('24', 'clear mind', '15', '70', '5', '71', '15');
INSERT INTO `traits` VALUES ('24', 'clear mind', '15', '70', '5', '295', '2');
INSERT INTO `traits` VALUES ('59', 'resist slow', '15', '75', '4', '250', '20');
-- INSERT INTO `traits` VALUES ('?', 'avatar physical accuracy', '15', '75', '1', '?', '?'); -- Merit
-- INSERT INTO `traits` VALUES ('?', 'avatar physical attack', '15', '75', '1', '?', '?'); -- Merit
-- INSERT INTO `traits` VALUES ('?', 'avatar magical accuracy', '15', '75', '1', '?', '?'); -- Merit
-- INSERT INTO `traits` VALUES ('?', 'avatar magical attack', '15', '75', '1', '?', '?'); -- Merit
INSERT INTO `traits` VALUES ('124', 'summoning magic cast time', '15', '75', '1', '0', '0'); -- Merit
INSERT INTO `traits` VALUES ('8', 'max mp boost', '15', '76', '5', '5', '80');
-- INSERT INTO `traits` VALUES ('103', 'stout servant', '15', '85', '1', '?', '?'); -- Pet Dmg reduction -5%
INSERT INTO `traits` VALUES ('10', 'auto refresh', '15', '90', '2', '369', '2');
-- INSERT INTO `traits` VALUES ('103', 'stout servant', '9', '95', '2', '?', '?'); -- Pet Dmg reduction -7%
INSERT INTO `traits` VALUES ('8', 'max mp boost', '15', '96', '6', '5', '100');

-- ------------------------------------------------------------
-- Blue Mage
-- ------------------------------------------------------------

INSERT INTO `traits` VALUES ('90', 'enchainment', '16', '75', '1', '0', '0'); -- Merit
INSERT INTO `traits` VALUES ('91', 'assimilation', '16', '75', '1', '0', '0'); -- Merit

-- ------------------------------------------------------------
-- Corsair
-- ------------------------------------------------------------

INSERT INTO `traits` VALUES ('50', 'resist paralyze', '17', '5', '1', '242', '5');
INSERT INTO `traits` VALUES ('11', 'rapid shot', '17', '15', '1', '359', '10');
INSERT INTO `traits` VALUES ('50', 'resist paralyze', '17', '25', '2', '242', '10');
INSERT INTO `traits` VALUES ('61', 'resist amnesia', '17', '30', '1', '255', '5');
INSERT INTO `traits` VALUES ('84', 'recycle', '17', '35', '1', '305', '10');
INSERT INTO `traits` VALUES ('50', 'resist paralyze', '17', '45', '3', '242', '15');
INSERT INTO `traits` VALUES ('11', 'rapid shot', '17', '45', '2', '359', '12');
INSERT INTO `traits` VALUES ('50', 'resist paralyze', '17', '65', '4', '242', '20');
INSERT INTO `traits` VALUES ('11', 'rapid shot', '17', '75', '3', '359', '14');
INSERT INTO `traits` VALUES ('92', 'winning streak', '17', '75', '1', '0', '0'); -- Merit
INSERT INTO `traits` VALUES ('93', 'loaded deck', '17', '75', '1', '0', '0'); -- Merit
-- INSERT INTO `traits` VALUES ('84', 'true shot', '17', '85', '1', '?', '?'); -- Bonus Dmg to Ranged Atks at distance +3%
-- INSERT INTO `traits` VALUES ('84', 'true shot', '17', '95', '2', '?', '?'); -- Bonus Dmg to Ranged Atks at distance +5%

-- ------------------------------------------------------------
-- Puppetmaster
-- ------------------------------------------------------------

INSERT INTO `traits` VALUES ('59', 'resist slow', '18', '10', '1', '250', '5');
INSERT INTO `traits` VALUES ('61', 'resist amnesia', '18', '15', '1', '255', '5');
INSERT INTO `traits` VALUES ('2', 'evasion bonus', '18', '20', '1', '69', '10');
INSERT INTO `traits` VALUES ('23', 'martial arts', '18', '25', '1', '173', '20');
INSERT INTO `traits` VALUES ('59', 'resist slow', '18', '50', '2', '250', '10');
INSERT INTO `traits` VALUES ('2', 'evasion bonus', '18', '50', '2', '69', '22');
INSERT INTO `traits` VALUES ('23', 'martial arts', '18', '50', '2', '173', '40');
INSERT INTO `traits` VALUES ('59', 'resist slow', '18', '75', '3', '250', '15');
INSERT INTO `traits` VALUES ('2', 'evasion bonus', '18', '75', '3', '69', '35');
INSERT INTO `traits` VALUES ('23', 'martial arts', '18', '75', '3', '173', '60');
INSERT INTO `traits` VALUES ('94', 'fine-tuning', '18', '75', '1', '0', '0'); -- Merit
INSERT INTO `traits` VALUES ('95', 'optimization', '18', '75', '1', '0', '0'); -- Merit
-- INSERT INTO `traits` VALUES ('103', 'stout servant', '18', '78', '1', '?', '?'); -- Pet Dmg reduction -5%
INSERT INTO `traits` VALUES ('101', 'tactical guard', '18', '80', '1', '462', '3');
-- INSERT INTO `traits` VALUES ('99', 'crit. def. bonus', '18', '85', '1', '?', '4'); -- Crit DEF DMG -4%
INSERT INTO `traits` VALUES ('23', 'martial arts', '18', '87', '4', '173', '80');
-- INSERT INTO `traits` VALUES ('103', 'stout servant', '18', '88', '2', '?', '?'); -- Pet Dmg reduction -7%
INSERT INTO `traits` VALUES ('101', 'tactical guard', '18', '90', '2', '462', '5');
INSERT INTO `traits` VALUES ('23', 'martial arts', '18', '97', '5', '173', '100');
-- INSERT INTO `traits` VALUES ('103', 'stout servant', '18', '98', '3', '?', '?'); -- Pet Dmg reduction -9%

-- ------------------------------------------------------------
-- Dancer
-- ------------------------------------------------------------

INSERT INTO `traits` VALUES ('2', 'evasion bonus', '19', '15', '1', '69', '10');
INSERT INTO `traits` VALUES ('59', 'resist slow', '19', '20', '1', '250', '5');
INSERT INTO `traits` VALUES ('18', 'dual wield', '19', '20', '1', '259', '10');
INSERT INTO `traits` VALUES ('67', 'subtle blow', '19', '25', '1', '289', '5');
INSERT INTO `traits` VALUES ('1', 'accuracy bonus', '19', '30', '1', '25', '10');
INSERT INTO `traits` VALUES ('1', 'accuracy bonus', '19', '30', '1', '26', '10');
INSERT INTO `traits` VALUES ('18', 'dual wield', '19', '40', '2', '259', '15');
INSERT INTO `traits` VALUES ('2', 'evasion bonus', '19', '45', '2', '69', '22');
INSERT INTO `traits` VALUES ('67', 'subtle blow', '19', '45', '2', '289', '10');
INSERT INTO `traits` VALUES ('106', 'skillchain bonus', '19', '45', '1', '174', '8');
INSERT INTO `traits` VALUES ('106', 'skillchain bonus', '19', '58', '2', '174', '12');
INSERT INTO `traits` VALUES ('1', 'accuracy bonus', '19', '60', '2', '25', '22');
INSERT INTO `traits` VALUES ('1', 'accuracy bonus', '19', '60', '2', '26', '22');
INSERT INTO `traits` VALUES ('18', 'dual wield', '19', '60', '3', '259', '25');
INSERT INTO `traits` VALUES ('67', 'subtle blow', '19', '65', '3', '289', '15');
INSERT INTO `traits` VALUES ('106', 'skillchain bonus', '19', '71', '3', '174', '16');
INSERT INTO `traits` VALUES ('2', 'evasion bonus', '19', '75', '3', '69', '35');
INSERT INTO `traits` VALUES ('96', 'closed position', '19', '75', '1', '0', '0'); -- Merit
INSERT INTO `traits` VALUES ('1', 'accuracy bonus', '19', '76', '3', '25', '35');
INSERT INTO `traits` VALUES ('1', 'accuracy bonus', '19', '76', '3', '26', '35');
INSERT INTO `traits` VALUES ('100', 'tactical parry', '19', '77', '10', '463', '2');
INSERT INTO `traits` VALUES ('108', 'conserve tp', '19', '77', '1', '566', '25');
INSERT INTO `traits` VALUES ('98', 'crit. atk. bonus', '19', '80', '1', '459', '5');
INSERT INTO `traits` VALUES ('18', 'dual wield', '19', '80', '4', '259', '30');
INSERT INTO `traits` VALUES ('100', 'tactical parry', '19', '84', '20', '463', '3');
INSERT INTO `traits` VALUES ('106', 'skillchain bonus', '19', '84', '4', '174', '20');
INSERT INTO `traits` VALUES ('2', 'evasion bonus', '19', '86', '4', '69', '48');
INSERT INTO `traits` VALUES ('98', 'crit. atk. bonus', '19', '88', '2', '459', '7');
INSERT INTO `traits` VALUES ('100', 'tactical parry', '19', '91', '30', '463', '4');
INSERT INTO `traits` VALUES ('106', 'skillchain bonus', '19', '97', '5', '174', '24');
INSERT INTO `traits` VALUES ('100', 'tactical parry', '19', '97', '40', '463', '5');
INSERT INTO `traits` VALUES ('98', 'crit. atk. bonus', '19', '99', '3', '459', '9');

-- ------------------------------------------------------------
-- Scolar
-- ------------------------------------------------------------

INSERT INTO `traits` VALUES ('52', 'resist silence', '20', '10', '1', '244', '5');
INSERT INTO `traits` VALUES ('24', 'clear mind', '20', '20', '1', '71', '3');
INSERT INTO `traits` VALUES ('13', 'conserve mp', '20', '25', '1', '296', '25');
INSERT INTO `traits` VALUES ('8', 'max mp boost', '20', '30', '1', '5', '10');
INSERT INTO `traits` VALUES ('114', 'tranquil heart', '20', '30', '1', '563', '1');
INSERT INTO `traits` VALUES ('24', 'clear mind', '20', '35', '2', '71', '6');
INSERT INTO `traits` VALUES ('52', 'resist silence', '20', '40', '2', '244', '10');
INSERT INTO `traits` VALUES ('24', 'clear mind', '20', '50', '3', '71', '9');
INSERT INTO `traits` VALUES ('24', 'clear mind', '20', '50', '3', '295', '1');
INSERT INTO `traits` VALUES ('24', 'clear mind', '20', '65', '4', '71', '12');
INSERT INTO `traits` VALUES ('24', 'clear mind', '20', '50', '4', '295', '1');
INSERT INTO `traits` VALUES ('52', 'resist silence', '20', '70', '3', '244', '15');
INSERT INTO `traits` VALUES ('97', 'stormsurge', '20', '75', '1', '0', '0'); -- Merit
INSERT INTO `traits` VALUES ('109', 'occult acumen', '20', '78', '1', '464', '25');
INSERT INTO `traits` VALUES ('110', 'mag. burst bonus', '20', '79', '1', '414', '5');
INSERT INTO `traits` VALUES ('8', 'max mp boost', '20', '88', '1', '5', '10');
INSERT INTO `traits` VALUES ('109', 'occult acumen', '20', '88', '2', '464', '50');
INSERT INTO `traits` VALUES ('110', 'mag. burst bonus', '20', '79', '2', '414', '7');
INSERT INTO `traits` VALUES ('109', 'occult acumen', '20', '98', '3', '464', '75');
INSERT INTO `traits` VALUES ('110', 'mag. burst bonus', '20', '79', '3', '414', '9');

-- ------------------------------------------------------------
-- Geomancer
-- ------------------------------------------------------------

INSERT INTO `traits` VALUES ('13', 'conserve mp', '21', '10', '1', '296', '25');
INSERT INTO `traits` VALUES ('24', 'clear mind', '21', '20', '1', '71', '3');
-- INSERT INTO `traits` VALUES ('117', 'cardinal chant', '21', '25', '1', '?', '?'); -- Enhances elemental magic spells
INSERT INTO `traits` VALUES ('13', 'conserve mp', '21', '25', '2', '296', '28');
INSERT INTO `traits` VALUES ('8', 'max mp boost', '21', '30', '1', '5', '10');
INSERT INTO `traits` VALUES ('24', 'clear mind', '21', '40', '2', '71', '6');
INSERT INTO `traits` VALUES ('13', 'conserve mp', '21', '40', '3', '296', '31');
INSERT INTO `traits` VALUES ('112', 'elemental celerity', '21', '55', '1', '403', '10');
INSERT INTO `traits` VALUES ('13', 'conserve mp', '21', '55', '4', '296', '34');
INSERT INTO `traits` VALUES ('24', 'clear mind', '21', '60', '3', '71', '9');
INSERT INTO `traits` VALUES ('24', 'clear mind', '21', '60', '3', '295', '1');
INSERT INTO `traits` VALUES ('8', 'max mp boost', '21', '60', '2', '5', '20');
INSERT INTO `traits` VALUES ('13', 'conserve mp', '21', '70', '5', '296', '37');
INSERT INTO `traits` VALUES ('119', 'c_recantation', '21', '75', '1', '0', '0'); -- Merit
INSERT INTO `traits` VALUES ('120', 'primordial_zeal', '21', '75', '1', '0', '0'); -- Merit

-- ------------------------------------------------------------
-- Rune Fencer
-- ------------------------------------------------------------

INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '5', '1', '240', '5');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '5', '1', '241', '5');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '5', '1', '242', '5');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '5', '1', '243', '5');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '5', '1', '244', '5');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '5', '1', '245', '5');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '5', '1', '246', '5');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '5', '1', '247', '5');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '5', '1', '248', '5');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '5', '1', '249', '5');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '5', '1', '250', '5');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '5', '1', '251', '5');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '5', '1', '252', '5');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '5', '1', '254', '5');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '5', '1', '255', '5');
INSERT INTO `traits` VALUES ('6', 'magic def. bonus', '22', '10', '1', '29', '10');
INSERT INTO `traits` VALUES ('119', 'inquartata', '22', '15', '1', '562', '7');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '25', '2', '240', '7');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '25', '2', '241', '7');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '25', '2', '242', '7');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '25', '2', '243', '7');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '25', '2', '244', '7');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '25', '2', '245', '7');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '25', '2', '246', '7');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '25', '2', '247', '7');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '25', '2', '248', '7');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '25', '2', '249', '7');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '25', '2', '250', '7');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '25', '2', '251', '7');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '25', '2', '252', '7');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '25', '2', '254', '7');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '25', '2', '255', '7');
INSERT INTO `traits` VALUES ('6', 'magic def. bonus', '22', '30', '2', '29', '12');
INSERT INTO `traits` VALUES ('9', 'auto regen', '22', '35', '1', '370', '1');
INSERT INTO `traits` VALUES ('100', 'tactical parry', '22', '40', '10', '463', '2');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '45', '3', '240', '9');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '45', '3', '241', '9');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '45', '3', '242', '9');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '45', '3', '243', '9');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '45', '3', '244', '9');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '45', '3', '245', '9');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '45', '3', '246', '9');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '45', '3', '247', '9');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '45', '3', '248', '9');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '45', '3', '249', '9');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '45', '3', '250', '9');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '45', '3', '251', '9');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '45', '3', '252', '9');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '45', '3', '254', '9');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '45', '3', '255', '9');
INSERT INTO `traits` VALUES ('6', 'magic def. bonus', '22', '50', '3', '29', '14');
INSERT INTO `traits` VALUES ('100', 'tactical parry', '22', '60', '20', '463', '3');
INSERT INTO `traits` VALUES ('9', 'auto regen', '22', '65', '2', '370', '2');
INSERT INTO `traits` VALUES ('6', 'magic def. bonus', '22', '70', '4', '29', '16');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '75', '3', '240', '11');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '75', '3', '241', '11');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '75', '3', '242', '11');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '75', '3', '243', '11');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '75', '3', '244', '11');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '75', '3', '245', '11');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '75', '3', '246', '11');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '75', '3', '247', '11');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '75', '3', '248', '11');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '75', '3', '249', '11');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '75', '3', '250', '11');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '75', '3', '251', '11');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '75', '3', '252', '11');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '75', '3', '254', '11');
INSERT INTO `traits` VALUES ('118', 'tenacity', '22', '75', '3', '255', '11');
INSERT INTO `traits` VALUES ('121', 'inspiration', '22', '75', '1', '0', '0'); -- Merit
INSERT INTO `traits` VALUES ('122', 'sleight_of_sword', '22', '75', '1', '0', '0'); -- Merit
INSERT INTO `traits` VALUES ('9', 'auto regen', '22', '95', '3', '370', '3');
INSERT INTO `traits` VALUES ('100', 'tactical parry', '22', '97', '30', '463', '4');