-----------------------------------
-- Area: La Theine Plateau
--  NM:  Bloodtear Baldurf
-- @zone 102
-- @pos 88.000 8.000 -239.000
-----------------------------------

require("/scripts/globals/fieldsofvalor");
require("scripts/globals/titles");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	killer:addTitle(THE_HORNSPLITTER);
end;