-----------------------------------
-- Area: Ru'Aun Gardens
--  NM:  Genbu
-- @zone 130
-- @pos 257.000 -70.000 517.000
-----------------------------------

require("scripts/zones/RuAun_Gardens/TextIDs");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------
function onMobDeath(mob, killer)
	killer:showText(mob,SKY_GOD_OFFSET + 6);
	GetNPCByID(17310097):hideNPC(120);
end;