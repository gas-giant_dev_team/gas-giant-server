-----------------------------------
-- Area: Ru'Aun Gardens
--  NM:  Byakko
-- @zone 130
-- @pos -410.488 -70.520 394.720
-----------------------------------

require("scripts/zones/RuAun_Gardens/TextIDs");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	killer:showText(mob,SKY_GOD_OFFSET + 12);
	GetNPCByID(17310050):hideNPC(120);
end;