-----------------------------------
-- Area: Ru'Aun Gardens
-- NPC: _3m0
-- @zone 130
-- @pos 0.000 -46.615 -109.159
-----------------------------------
package.loaded["scripts/zones/RuAun_Gardens/TextIDs"] = nil;
-----------------------------------

require("scripts/zones/RuAun_Gardens/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end; 

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	local DoorID = npc:getID();

	GetNPCByID(DoorID):openDoor(7);
	GetNPCByID(DoorID+1):openDoor(7);
	GetNPCByID(DoorID+2):openDoor(7);
end; 

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
end;