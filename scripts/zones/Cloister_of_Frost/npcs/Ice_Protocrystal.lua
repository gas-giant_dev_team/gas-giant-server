-----------------------------------
-- Area: Cloister of Frost
-- NPC: Ice Protocrystal
-- Quests: Trial by Ice, Trial Size
-- Trial by Ice
-- @pos 203
-- @pos 558 0 596
-----------------------------------
package.loaded["scripts/zones/Cloister_of_Frost/TextIDs"] = nil;
package.loaded["scripts/globals/bcnm"] = nil;
-------------------------------------

require("scripts/globals/keyitems");
require("scripts/globals/bcnm");
require("scripts/zones/Cloister_of_Frost/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
	
	if(TradeBCNM(player,player:getZone(),trade,npc))then
		return;
	end

end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	if(EventTriggerBCNM(player,npc))then
		return;
	else
		player:messageSpecial(PROTOCRYSTAL);
	end

end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("onUpdate CSID: %u",csid);
	-- printf("onUpdate RESULT: %u",option);

	if(EventUpdateBCNM(player,csid,option))then
		return;
	end

end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("onFinish CSID: %u",csid);
	-- printf("onFinish RESULT: %u",option);

	if (csid == 0x7D02) then
		player:setVar("ASA_shiva_done",1);
	elseif (csid == 0x0002) then
		player:setVar("ASA_shiva_done",3);
		player:delKeyItem(DOMINAS_AZURE_SEAL);
		player:addKeyItem(AZURE_COUNTERSEAL);
		player:messageSpecial(KEYITEM_OBTAINED,AZURE_COUNTERSEAL);
	elseif(EventFinishBCNM(player,csid,option))then
		return;
	end

end;