-- Area: Cloister of Frost
-- Last updated: client ver. 30140910_0

-- Variable TextID   Description text

-- General Texts
ITEM_CANNOT_BE_OBTAINED = 6378; -- You cannot obtain the item <item> come back again after sorting your inventory
          ITEM_OBTAINED = 6381; -- Obtained: <item>
           GIL_OBTAINED = 6382; -- Obtained <number> gil
       KEYITEM_OBTAINED = 6384; -- Obtained key item: <keyitem>

-- Quest dialog
YOU_CANNOT_ENTER_THE_BATTLEFIELD = 7185; -- You cannot enter the battlefield at present.
          IFRIT_UNLOCKED = 7537; -- You are now able to summon

-- ZM4 Dialog
    CANNOT_REMOVE_FRAG = 7627; -- It is an oddly shaped stone monument. A shining stone is embedded in it, but cannot be removed...
 ALREADY_OBTAINED_FRAG = 7628; -- You have already obtained this monument's . Try searching for another.
ALREADY_HAVE_ALL_FRAGS = 7629; -- You have obtained all of the fragments. You must hurry to the ruins of the ancient shrine!
       FOUND_ALL_FRAGS = 7630; -- You have obtained ! You now have all 8 fragments of light!
       ZILART_MONUMENT = 7631; -- It is an ancient Zilart monument.?Prompt?

-- Other
PROTOCRYSTAL = 7207; -- It is a giant crystal.

-- conquest Base
CONQUEST_BASE = 7024;
