-----------------------------------
-- Area: Sacrificial Chamber
-- NM: Vermilion-eared Noberry
-- BCNM: Jungle Boogymen
-- @zone 163
-- @pos -287.368, -32.000, 319.647
-----------------------------------

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobEngaged
-----------------------------------

function onMobEngaged(mob,target)
end;

-----------------------------------
-- onMobDeath Action
-----------------------------------

function onMobDeath(mob,killer)
	local elemental = mob:getID()+2;
	local kills = killer:getVar("EVERYONES_GRUDGE_KILLS");
	
	if(kills < 480) then
		killer:setVar("EVERYONES_GRUDGE_KILLS",kills + 1);
	end	

	if(GetMobAction(elemental) ~= 0) then
		DespawnMob(elemental);
	end
	
end;
