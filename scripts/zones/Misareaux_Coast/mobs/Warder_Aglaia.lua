-----------------------------------
-- Area: Misareaux Coast
-- NM: Warder Aglaia
-- @zone 25
-- @pos 262.868 13.636 -422.508
-----------------------------------

require("scripts/globals/missions");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	if(killer:getCurrentMission(COP) == A_PLACE_TO_RETURN and killer:getVar("PromathiaStatus") == 1)then 
		killer:setVar("Warder_Aglaia_KILL",1);
	end
end;