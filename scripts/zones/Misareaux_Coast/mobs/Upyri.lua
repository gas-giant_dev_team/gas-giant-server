-----------------------------------
-- Area: Misareaux Coast (25)
-- Mob: Upyri
-- @zone 25
-- @pos -136.075 -31.790 114.824
-- NOTES/TO DO: Tends to use weapon
-- skills twice in a row during
-- night time, based on retail
-- testing. There is about a 1-2
-- second delay, and will use the
-- same move twice. He will rarely
-- use a weaponskill 3 times in a
-- row. Let's say about a 10% chance. 
-- Will weapon skill normally during
-- the day. Also, may only use Soul
-- Accretion at night. Special
-- Attacks: Hits harder at night
-- than during the day.
-- Earring may or may not drop
-- only if the ToD was at night. 
-----------------------------------

-- require("scripts/zones/Misareaux_Coast/MobIDs");

-----------------------------------
-- onMobInitialize
-----------------------------------

function onMobInitialize(mob)
end;

-----------------------------------
-- onMobSpawn
-----------------------------------

function onMobSpawn(mob)
end;

-----------------------------------
-- onMobEngaged
-----------------------------------

function onMobEngaged(mob,target)
end;

-----------------------------------
-- onMobFight
-----------------------------------

function onMobFight(mob,target)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)
	mob:setRespawnTime(math.random((75600),(86400)));	-- 21 to 24 hr
end;
