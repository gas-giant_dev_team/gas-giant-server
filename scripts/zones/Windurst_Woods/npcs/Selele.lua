-----------------------------------
-- Area: Windurst Woods
-- NPC:  Selele
-- Type: Tutorial NPC (Windurst)
-- @zone 241
-- @pos 106.830, -5.000, -31.527
-----------------------------------
package.loaded["scripts/zones/Windurst_Woods/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/keyitems");
require("scripts/zones/Windurst_Woods/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	-- If Windurst National
	if (player:getVar("Windy_Tutorial_NPC") == 0 and player:getNation() == 2) then
		player:startEvent(0x032C); -- Initial CS
	-- Quest 1 -- Get Signet
	elseif (player:getVar("Windy_Tutorial_NPC") == 14) then
		player:startEvent(0x032D); -- quest parameters
	elseif (player:getVar("Windy_Tutorial_NPC") == 15) then
		if (player:hasStatusEffect(EFFECT_SIGNET) == true) then
			player:startEvent(0x032F); -- complete quest
		else
			player:startEvent(0x032E); -- repeat quest parameters
		end
	-- Quest 2 - Prerequisite - Food icon after eating
	elseif (player:getVar("Windy_Tutorial_NPC") == 16) then
		player:startEvent(0x0330);
	elseif (player:getVar("Windy_Tutorial_NPC") == 1) then
		if (player:hasStatusEffect(EFFECT_FOOD) == true) then
			player:startEvent(0x0331); -- advance to Quest 2
		else
			player:startEvent(0x0330); -- repeat quest prerequisite
		end
	-- Quest 2 - Learn a Weaponskill an use it on a mob
	elseif (player:getVar("Windy_Tutorial_NPC") == 2) then
		player:startEvent(0x0332); -- Quest 2 part 2
	elseif (player:getVar("Windy_Tutorial_NPC") == 3) then
		if (player:getVar("Windy_Tutorial_WS") == 1) then
			player:startEvent(0x0332); -- repeat quest parameters
		else
			player:startEvent(0x0333); -- advance to Quest 3
		end
	-- Quest 3 - Find the Auction House in Windy Woods
	elseif (player:getVar("Windy_Tutorial_NPC") == 4) then
		player:startEvent(0x0334); -- repeat quest parameters
	elseif (player:getVar("Windy_Tutorial_NPC") == 5) then
		player:startEvent(0x0335); -- complete Quest 3
	-- Quest 4 -- Make it to lvl 4
	elseif (player:getVar("Windy_Tutorial_NPC") == 6) then
		player:startEvent(0x0336); -- quest parameters
	elseif (player:getVar("Windy_Tutorial_NPC") == 7) then
		if (player:getMainLvl() >= 4) then
			player:startEvent(0x0337); -- complete quest
		else
			player:startEvent(0x0336); -- repeat quest parameters
		end
	-- Quest 5 -- Kill 1 mob in Tahrongi Canyon
	elseif (player:getVar("Windy_Tutorial_NPC") == 8) then
		player:startEvent(0x0338, 0, 0, 2); -- quest parameters
	elseif (player:getVar("Windy_Tutorial_NPC") == 9) then
		player:startEvent(0x0339, 800); -- quest parameters
	-- Quest 6 -- Make it to lvl 10
	elseif (player:getVar("Windy_Tutorial_NPC") == 10) then
		player:startEvent(0x033A, 0, 0, 2); -- quest parameters
	elseif (player:getVar("Windy_Tutorial_NPC") == 11) then
		if (player:getMainLvl() >= 10) then
			player:startEvent(0x033B, 0, 1000, 2); -- complete quest
		else
			player:startEvent(0x033A, 0, 0, 2); -- repeat quest parameters
		end
	-- Quest 7 -- Get Mea Gate Crystal
	elseif (player:getVar("Windy_Tutorial_NPC") == 12) then
		if (player:hasKeyItem(MEA_GATE_CRYSTAL) == true) then
			player:startEvent(0x033D, 354, 1000, 0, 1789, 3); -- complete quest
		else
			player:startEvent(0x033C, 0, 0, 2); -- quest parameters
		end
	elseif (player:getVar("Windy_Tutorial_NPC") == 13) then
		player:startEvent(0x033E);
	else
		player:startEvent(0x033F);
	end
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
	if (csid == 0x032C) then
		player:setVar("Windy_Tutorial_NPC",14);
	elseif (csid == 0x032D) then
		player:setVar("Windy_Tutorial_NPC",15);
	elseif (csid == 0x032F) then
		player:setVar("Windy_Tutorial_NPC",16);
		player:addItem(4376, 3); -- strip of meat jerky
		player:messageSpecial(ITEM_OBTAINED,4376);
	elseif (csid == 0x0330) then
		player:setVar("Windy_Tutorial_NPC",1);
	elseif (csid == 0x0331) then
		player:setVar("Windy_Tutorial_NPC",2);
	elseif (csid == 0x0332) then
		player:setVar("Windy_Tutorial_NPC",3);
		player:setVar("Windy_Tutorial_WS",1);
	elseif (csid == 0x0333) then
		player:setVar("Windy_Tutorial_NPC",4);
		player:addItem(4570); -- bird egg
		player:messageSpecial(ITEM_OBTAINED,4570);
		player:addItem(4370); -- honey
		player:messageSpecial(ITEM_OBTAINED,4370);
		player:addItem(4101); -- water crystal
		player:messageSpecial(ITEM_OBTAINED,4101);
	elseif (csid == 0x0335) then
		player:setVar("Windy_Tutorial_NPC",6);
		player:addKeyItem(CONQUEST_PROMOTION_VOUCHER);
		player:messageSpecial(KEYITEM_OBTAINED,CONQUEST_PROMOTION_VOUCHER);
	elseif (csid == 0x0336) then
		player:setVar("Windy_Tutorial_NPC",7);
	elseif (csid == 0x0337) then
		player:setVar("Windy_Tutorial_NPC",8);
		player:addItem(16003); -- raising earring
		player:messageSpecial(ITEM_OBTAINED,16003);
	elseif (csid == 0x0339) then
		player:setVar("Windy_Tutorial_NPC",10);
		target:addExp(EXP_RATE * 800);
	elseif (csid == 0x033A) then
		player:setVar("Windy_Tutorial_NPC",11);
	elseif (csid == 0x033B) then
		player:setVar("Windy_Tutorial_NPC",12);
		player:addGil(GIL_RATE*1000);
		player:messageSpecial(GIL_OBTAINED,GIL_RATE*1000);
	elseif (csid == 0x033D) then
		player:setVar("Windy_Tutorial_NPC",13);
		target:addExp(EXP_RATE * 1000);
		player:addItem(1789, 3); -- free chocopasses
		player:messageSpecial(ITEM_OBTAINED,1789);
	end
end;