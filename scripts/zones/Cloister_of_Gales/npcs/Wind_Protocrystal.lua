-----------------------------------
-- Area: Cloister of Gales
-- NPC: Wind Protocrystal
-- Quests: Trial by Wind, Trial
-- Size Trial By Wind
-- @zone 201
-- @pos -361 1 -381
-----------------------------------
package.loaded["scripts/zones/Cloister_of_Gales/TextIDs"] = nil;
package.loaded["scripts/globals/bcnm"] = nil;
-----------------------------------

require("scripts/globals/keyitems");
require("scripts/globals/bcnm");
require("scripts/zones/Cloister_of_Gales/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)

	if(TradeBCNM(player,player:getZone(),trade,npc))then
		return;
	end

end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	if(EventTriggerBCNM(player,npc))then
		return;
	else
		player:messageSpecial(PROTOCRYSTAL);
	end

end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("onUpdate CSID: %u",csid);
	-- printf("onUpdate RESULT: %u",option);

	if(EventUpdateBCNM(player,csid,option))then
		return;
	end

end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("onFinish CSID: %u",csid);
	-- printf("onFinish RESULT: %u",option);

	if (csid == 0x7D02) then
		player:setVar("ASA_garuda_done",1);
	elseif (csid == 0x0002) then
		player:setVar("ASA_garuda_done",3);
		player:delKeyItem(DOMINAS_EMERALD_SEAL);
		player:addKeyItem(EMERALD_COUNTERSEAL);
		player:messageSpecial(KEYITEM_OBTAINED,EMERALD_COUNTERSEAL);
	elseif(EventFinishBCNM(player,csid,option))then
		return;
	end
end;