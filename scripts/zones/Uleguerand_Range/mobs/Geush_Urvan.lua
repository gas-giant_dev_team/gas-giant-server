-----------------------------------
-- Area: Uleguerand_Range
--  NM:  Geush Urvan
-- @zone 5
-- @pos 563.638 -35.719 110.362
-- Notes: Spawned by trading a
-- Haunted Muleta to the ??? (qm1)
-- at (K-7)
-----------------------------------

-----------------------------------
-- OnMobSpawn Action
-----------------------------------

function onMobSpawn(mob)
end;

-----------------------------------
-- OnMobDeath Action
-----------------------------------

function onMobDeath(mob,killer)
	GetNPCByID(16798094):hideNPC(900);
end;