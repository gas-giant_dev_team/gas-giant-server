-----------------------------------
-- Area: Mordion Gaol
-- NPC: Read me
-- Type: GM Jailer
-- @zone 131
-- @pos -619.000 -12.000 668.000
-------------------------------------
package.loaded["scripts/zones/Mordion_Gaol/TextIDs"] = nil;
-------------------------------------

require("scripts/globals/settings");
require("scripts/zones/Mordion_Gaol/TextIDs");

----------------------------------- 
-- onTrade Action 
----------------------------------- 

function onTrade(player,npc,trade) 
end;

----------------------------------- 
-- onTrigger Action 
-----------------------------------
 
function onTrigger(player,npc) 
	player:startEvent(0x0067);
end; 

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;