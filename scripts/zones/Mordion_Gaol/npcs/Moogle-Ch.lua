-----------------------------------
-- Area: Mordion Gaol
-- NPC: Moogle-Ch
-- Type: GM Jailer
-- @zone 131
-- @pos -179.000 -12.000 -211.000
-- cutscenes 0x0064  0x0066  0x0065
-- for changing chocobo names ????
-------------------------------------
package.loaded["scripts/zones/Mordion_Gaol/TextIDs"] = nil;
-------------------------------------

require("scripts/globals/settings");
require("scripts/zones/Mordion_Gaol/TextIDs");

----------------------------------- 
-- onTrade Action 
----------------------------------- 

function onTrade(player,npc,trade) 
end;

----------------------------------- 
-- onTrigger Action 
-----------------------------------
 
function onTrigger(player,npc) 
	player:startEvent(0x0067);
end; 

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;