-----------------------------------
-- Area: King Ranperres Tomb
-- NM: Corrupted Ulbrig
-- Missions: San'dOria 6-2
-- @zone 190
-- @pos -37.429 7.500 18.147
-----------------------------------

require("/scripts/globals/missions");	

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	local currentMission = killer:getCurrentMission(SANDORIA);
	local MissionStatus = killer:getVar("MissionStatus");

	if(currentMission == RANPERRE_S_FINAL_REST and MissionStatus == 1) then
		killer:setVar("Mission6-2MobKilled",1);
	end
end;
