----------------------------------
-- Area: Kuftal Tunnel
-- NM: Arachne
-- @zone 174
-- @pos -22.423 20.75 -0.009
-----------------------------------

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)

	-- Set Arachne's Window Open Time
    local wait = math.random((7200),(28800)); -- 2-8 hours
	SetServerVariable("[POP]Arachne", os.time(t) + (wait /NM_TIMER_MOD));
	DeterMob(mob:getID(), true);

	-- Set PH back to normal, then set to respawn spawn
    local PH = GetServerVariable("[PH]Arachne");
	SetServerVariable("[PH]Arachne", 0);
	DeterMob(PH, false);
	GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

end;