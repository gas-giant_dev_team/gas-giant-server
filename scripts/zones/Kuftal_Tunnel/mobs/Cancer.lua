-----------------------------------
-- Area: Kuftal Tunnel
-- NM: Cancer
-- @zone 173
-- @pos -25.000 -10.000 -152.000
-----------------------------------

-----------------------------------
-- OnMobSpawn Action
-----------------------------------

function onMobSpawn(mob)
end; 

-----------------------------------
-- OnMobDeath Action
-----------------------------------

function onMobDeath(mob,killer)
	GetNPCByID(17490251):hideNPC(900); -- qm2 in npc_list
end;