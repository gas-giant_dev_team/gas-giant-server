-----------------------------------
-- Area: Bhaflau Thickets
-- NPC: Postern (door _1g0)
-- @zone 52
-- @pos 573.625 -12.500 140.000
-- Notes: Shortcut back into Aht
-- Urgan Whitegate, North Harbor
-----------------------------------

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end; 

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	player:startEvent(0x01F6);
	return 1;
end; 

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);

	if (csid == 0x01F6 and option == 1) then
		player:setPos(-37,1,56,0,50);
	end

end;