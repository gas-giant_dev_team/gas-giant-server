-----------------------------------	
-- Area: Bhaflau Thickets	
-- MOB:  Harvestman
-- @pos 398.130 -10.675 179.169 52
-----------------------------------	

require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)	

	-- Set Harvestman's spawnpoint and respawn time (21-24 hours)
	UpdateNMSpawnPoint(mob:getID());
	mob:setRespawnTime((math.random((75600),(86400))) /NM_TIMER_MOD);

end;
