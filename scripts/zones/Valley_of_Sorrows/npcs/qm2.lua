-----------------------------------
-- Area: Valley of Sorrows
-- NPC: qm2 (???)
-- @zone 128
-- @pos 15.332 -2.827 -28.387
-- Note: Used to spawn Aspidochelone
-----------------------------------
package.loaded["scripts/zones/Valley_of_Sorrows/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/zones/Valley_of_Sorrows/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
	local Adamantoise = GetMobAction(17301537);
	local Aspidochelone = GetMobAction(17301538);

	-- Trade Clump of Blue Pondweed
	if (LandKingSystem_HQ == 1 or LandKingSystem_HQ == 2) then
		if((Aspidochelone == 0 or Aspidochelone == 24) and trade:hasItemQty(3344,1) and trade:getItemCount() == 1) then -- Check trade, and if mob is ACTION_NONE (0) or waiting to spawn (24)
			player:tradeComplete();
			SpawnMob(17301538,180):updateEnmity(player);
			Aspid_Engaged = os.time(t);
		end
	end

		-- Trade Clump of Red Pondweed
	if (LandKingSystem_NQ == 1 or LandKingSystem_NQ == 2) then
		if((Adamantoise == 0 or Adamantoise == 24) and trade:hasItemQty(3343,1) and trade:getItemCount() == 1) then 
			player:tradeComplete();
			SpawnMob(17301537,180):updateEnmity(player);
			Adamantoise_Engaged = os.time(t);
		end
	end
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	player:messageSpecial(NOTHING_OUT_OF_ORDINARY);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;