-----------------------------------
-- Area: Mount Zhayolm
-- NPC: Waudeen
-- Type: Assault
-- Quests: Beginnings
-- Missions: ToAU 2
-- @zone 61
-- @pos 673.882 -23.995 367.604
-----------------------------------
package.loaded["scripts/zones/Mount_Zhayolm/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/quests");
require("scripts/globals/missions");
require("scripts/globals/titles");
require("scripts/globals/keyitems");
require("scripts/zones/Mount_Zhayolm/TextIDs");

-----------------------------------

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	if(player:hasKeyItem(SUPPLIES_PACKAGE))then
		player:startEvent(0x0004); -- ToAU Mission 1
	elseif(player:getVar("TOAUM2") ==1)then
		player:startEvent(0x0006); -- ToAU Mission 1 after you pass off supplies
	elseif(player:getCurrentMission(TOAU) > IMMORTAL_SENTRIES)then
		player:startEvent(0x0005); -- Reminder about the Runic Portal
	else
		player:startEvent(0x0003); -- Not the sort of place for common folk.
	end
end;

-- player:startEvent(0x000a); -- Become a Blue Mage
-- player:startEvent(0x000b); -- Standard Dialog after you become a Blue Mage
-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);

	if(csid == 0x0004 and option == 1)then
		player:delKeyItem(SUPPLIES_PACKAGE);
		player:setVar("TOAUM2",1);
	end
end;