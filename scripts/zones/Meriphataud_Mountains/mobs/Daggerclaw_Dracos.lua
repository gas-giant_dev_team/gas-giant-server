-----------------------------------	
-- Area: Meriphataud Mountains	
-- MOB: Daggerclaw Dracos
-- Quests: The Miraculous Dale
-- @zone 119
-- @pos 630.430, -16.372, -494.748
-----------------------------------	
	
require("/scripts/globals/fieldsofvalor");
require("scripts/globals/quests");
	
-----------------------------------	
-- onMobDeath	
-----------------------------------	
	
function onMobDeath(mob,killer)	
	checkRegime(killer,mob,39,1);
	local DALEQuest = player:getVar("DALEQuest");
	if(player:getQuestStatus(JEUNO,THE_MIRACULOUS_DALE) == QUEST_ACCEPTED) then
		DALEQuest = player:setMaskBit(DALEQuest,"DALEQuest",6,true);
	end
end;	
