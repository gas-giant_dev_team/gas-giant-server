-----------------------------------
-- Area: Meriphataud Mountains (119)
-- NM: Coo Keja the Unseen
-- @zone 119
-- @pos 684.000, -23.000, 6.000
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)	

	-- Set Coo_Keja_the_Unseen's spawnpoint and respawn time (21-24 hours)
	UpdateNMSpawnPoint(mob:getID());
	mob:setRespawnTime((math.random((75600),(86400))) /LongNM_TIMER_MOD);

end;
