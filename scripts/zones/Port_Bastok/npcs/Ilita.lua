-----------------------------------
-- Area: Port Bastok
-- NPC: Ilita
-- Type: Linkshell merchant
-- @zone 236
-- @pos -142.240 -1.667 -25.540
-----------------------------------
package.loaded["scripts/zones/Port_Bastok/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/shop");
require("scripts/zones/Port_Bastok/TextIDs");

-----------------------------------
-- onTrade
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger
-----------------------------------

function onTrigger(player,npc)
	player:showText(npc,ILITA_SHOP_DIALOG,513);

    local stock = {
		0x0200,  8000,	-- Linkshell
		0x3F9D,   375	-- Pendant Compass
	}
	showShop(player, STATIC, stock);

end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;