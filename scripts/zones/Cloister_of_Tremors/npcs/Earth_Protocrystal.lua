-----------------------------------
-- Area: Cloister of Tremors
-- NPC: Earth Protocrystal
-- Quests: Trial by Earth
-- @zone 209
-- @pos -539.946 0.455 -493.872
-----------------------------------
package.loaded["scripts/zones/Cloister_of_Tremors/TextIDs"] = nil;
package.loaded["scripts/globals/bcnm"] = nil;
-----------------------------------

require("scripts/globals/keyitems");
require("scripts/globals/bcnm");
require("scripts/zones/Cloister_of_Tremors/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)

	if(TradeBCNM(player,player:getZone(),trade,npc))then
		return;
	end

end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	if(EventTriggerBCNM(player,npc))then
		return;
	else
		player:messageSpecial(PROTOCRYSTAL);
	end

end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("onUpdate CSID: %u",csid);
	-- printf("onUpdate RESULT: %u",option);

	if(EventUpdateBCNM(player,csid,option))then
		return;
	end

end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("onFinish CSID: %u",csid);
	-- printf("onFinish RESULT: %u",option);

	if (csid == 0x7D02) then
		player:setVar("ASA_titan_done",1);
	elseif (csid == 0x0002) then
		player:setVar("ASA_titan_done",3);
		player:delKeyItem(DOMINAS_AMBER_SEAL);
		player:addKeyItem(AMBER_COUNTERSEAL);
		player:messageSpecial(KEYITEM_OBTAINED,AMBER_COUNTERSEAL);
	elseif(EventFinishBCNM(player,csid,option))then
		return;
	end

end;