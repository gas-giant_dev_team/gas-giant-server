-----------------------------------
-- Area: Cloister of Tremors
-- NM: Titan Prime
-- Quests: Trial by Earth
-- @zone 209
-- @pos
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/keyitems");

-----------------------------------
-- OnMobSpawn Action
-----------------------------------

function onMobSpawn(mob)
end;

-----------------------------------
-- OnMobDeath Action
-----------------------------------
function onMobDeath(mob, killer)
	local record = 300;
	local partyMembers = 6;
	local pZone = killer:getZone();

	killer:setVar("BCNM_Killed",1);
	killer:startEvent(0x7d01,0,record,0,(os.time() - killer:getVar("BCNM_Timer")),partyMembers,0,0);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("onUpdate CSID: %u",csid);
	-- printf("onUpdate RESULT: %u",option);
	if(csid == 0x7d01) then
		player:delStatusEffect(EFFECT_BATTLEFIELD);
	end
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("onFinish CSID: %u",csid);
	-- printf("onFinish RESULT: %u",option);
	if(csid == 0x7d01) then
		player:delKeyItem(TUNING_FORK_OF_EARTH);
		player:addKeyItem(WHISPER_OF_TREMORS);
		player:messageSpecial(KEYITEM_OBTAINED,WHISPER_OF_TREMORS);
	end
end;