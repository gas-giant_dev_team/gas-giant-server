-----------------------------------
-- Area: Woh Gates
-- NPC: Knotted Root
-- @zone 273
-- @pos many
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/missions");
require("scripts/globals/titles");
require("scripts/globals/status");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function onMobSpawn(mob)
end;


-----------------------------------
-- onMobEngaged
-----------------------------------
function onMobEngaged(mob,target)
end;

-----------------------------------
-- onMobDeath Action
-----------------------------------

function onMobDeath(mob,killer)

	local A = GetMobAction(17895665);
	local B = GetMobAction(17895666);
	local D = GetMobAction(17895673);
	local E = GetMobAction(17895674);
	local C = A+B;
	local F = D+E;

	if(C >= 42 and C <= 50)then
		GetNPCByID(17895748):openDoor(3600);
		GetNPCByID(17895749):openDoor(3600);
		DespawnMob(17895667);
		DespawnMob(17895668);
		DespawnMob(17895669);
		DespawnMob(17895670);
		DespawnMob(17895671);
		DespawnMob(17895672);
	elseif(F >= 42 and F <= 50)then
		-- GetNPCByID(17895750):openDoor(3600);
		-- GetNPCByID(17895751):openDoor(3600);
		DespawnMob(17895675);
		DespawnMob(17895676);
		DespawnMob(17895677);
		DespawnMob(17895678);
		DespawnMob(17895679);
		DespawnMob(17895680);
	end
end;