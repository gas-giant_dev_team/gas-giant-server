-----------------------------------
-- Area: Selbina
-- NPC: Ramona
-- Type: Quest NPC
-- Quests: It's Raining Mannequins!
-- @zone 248
-- @pos 12.511 -7.287 2.939
-----------------------------------
package.loaded["scripts/zones/Selbina/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/keyitems");
require("scripts/globals/quests");
require("scripts/zones/Selbina/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end; 

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	if(player:getQuestStatus(OTHER_AREAS,IT_S_RAINING_MANNEQUINS) == QUEST_ACCEPTED and
	player:getVar("Raining_M_Progress") == 2) then
		player:startEvent(0x044F);
	else
		player:startEvent(0x00AA);
	end
end; 

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
	if(csid == 0x044F) then
		player:setVar("Raining_M_Progress",3);
		player:addKeyItem(YE_OLDE_MANNEQUIN_CATALOGUE);
		player:messageSpecial(KEYITEM_OBTAINED,YE_OLDE_MANNEQUIN_CATALOGUE);
	end
end;