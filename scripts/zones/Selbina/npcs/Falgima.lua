-----------------------------------
-- Area: Selbina
-- NPC: Falgima
-- Type: Standard Merchant NPC
-- @zone 248
-- @pos 23.000 -6.000 11.000
-----------------------------------
package.loaded["scripts/zones/Selbina/TextIDs"] = nil;
-----------------------------------

require("scripts/zones/Selbina/TextIDs");
require("scripts/globals/shop");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
    player:showText(npc,HERMINIA_SHOP_DIALOG);
    local stock =
    {
        4744,   5351,   --Scroll of Invisible
        4745,   2325,   --Scroll of Sneak
        4746,   1204    --Scroll of Deodorize
    }
    showShop(player, STATIC, stock);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
    -- printf("CSID: %u",csid);
    -- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
    -- printf("CSID: %u",csid);
    -- printf("RESULT: %u",option);
end;