-----------------------------------
-- Area: Yorcia Weald
-- NM: Gnarled Rampart
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/missions");
require("scripts/globals/titles");
require("scripts/globals/status");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function onMobSpawn(mob)
end;


-----------------------------------
-- onMobEngaged
-----------------------------------
function onMobEngaged(mob,target)
end;

-----------------------------------
-- onMobDeath Action
-----------------------------------

function onMobDeath(mob,killer)

	local A = GetMobAction(17854824);
	local B = GetMobAction(17854825);
	local D = GetMobAction(17854832);
	local E = GetMobAction(17854833);
	local G = GetMobAction(17854840);
	local H = GetMobAction(17854841);
	local J = GetMobAction(17854848);
	local K = GetMobAction(17854849);

	local C = A+B;
	local F = D+E;
	local I = G+H;
	local L = J+K;

	if(C >= 42 and C <= 50)then
		GetNPCByID(17854945):openDoor(3600);
		DespawnMob(17854826);
		DespawnMob(17854827);
		DespawnMob(17854828);
		DespawnMob(17854829);
		DespawnMob(17854830);
		DespawnMob(17854831);
	elseif(F >= 42 and F <= 50)then
		GetNPCByID(17854948):openDoor(3600);
		DespawnMob(17854834);
		DespawnMob(17854835);
		DespawnMob(17854836);
		DespawnMob(17854837);
		DespawnMob(17854838);
		DespawnMob(17854839);
	elseif(I >= 42 and I <= 50)then
		GetNPCByID(17854946):openDoor(3600);
		DespawnMob(17854842);
		DespawnMob(17854843);
		DespawnMob(17854844);
		DespawnMob(17854845);
		DespawnMob(17854846);
		DespawnMob(17854847);
	elseif(L >= 42 and L <= 50)then
		GetNPCByID(17854947):openDoor(3600);
		DespawnMob(17854850);
		DespawnMob(17854851);
		DespawnMob(17854852);
		DespawnMob(17854853);
		DespawnMob(17854854);
		DespawnMob(17854855);
	end
end;
