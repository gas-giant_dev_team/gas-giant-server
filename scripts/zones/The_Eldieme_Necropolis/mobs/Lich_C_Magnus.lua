-----------------------------------
-- Area: The Eldieme Necropolis
-- NM: Lich C Magnus
-- @zone 195
-- @pos 299.000, -0.005, 19.000
-----------------------------------

require("scripts/globals/titles");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	killer:addTitle(LICH_BANISHER);
	if(killer:getQuestStatus(WINDURST,BLUE_RIBBON_BLUES) == QUEST_ACCEPTED) then
		killer:setVar("Lich_C_Magnus_Died",1);
	end
end;