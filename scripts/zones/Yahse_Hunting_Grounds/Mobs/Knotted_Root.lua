-----------------------------------
-- Area: Yahze Hunting Grounds
-- NM: Knotted Root
-- @zone 260
-- @pos many
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/missions");
require("scripts/globals/titles");
require("scripts/globals/status");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function onMobSpawn(mob)
end;


-----------------------------------
-- onMobEngaged
-----------------------------------
function onMobEngaged(mob,target)
end;

-----------------------------------
-- onMobDeath Action
-----------------------------------

function onMobDeath(mob,killer)

	local A = GetMobAction(17842559);
	local B = GetMobAction(17842560);
	local C = GetMobAction(17842561);
	local E = GetMobAction(17842566);
	local F = GetMobAction(17842567);
	local G = GetMobAction(17842568);
	local I = GetMobAction(17842573);
	local J = GetMobAction(17842574);
	local K = GetMobAction(17842575);
	local M = GetMobAction(17842582);
	local N = GetMobAction(17842583);
	local O = GetMobAction(17842584);
	local Q = GetMobAction(17842591);
	local R = GetMobAction(17842592);
	local S = GetMobAction(17842593);
	local D = A+B+C;
	local H = E+F+G;
	local L = I+J+K;
	local P = M+N+O;
	local T = Q+R+S;


	if(D >= 63 and D <= 75)then
		GetNPCByID(17842682):openDoor(3600);
		GetNPCByID(17842683):openDoor(3600);
		DespawnMob(17842562);
		DespawnMob(17842563);
		DespawnMob(17842564);
		DespawnMob(17842565);
	elseif(H >= 63 and H <= 75)then
		GetNPCByID(17842680):openDoor(3600);
		GetNPCByID(17842681):openDoor(3600);
		DespawnMob(17842569);
		DespawnMob(17842570);
		DespawnMob(17842571);
		DespawnMob(17842572);
	elseif(L >= 63 and L <= 75)then
		GetNPCByID(17842688):openDoor(3600);
		GetNPCByID(17842689):openDoor(3600);
		DespawnMob(17842576);
		DespawnMob(17842577);
		DespawnMob(17842578);
		DespawnMob(17842579);
		DespawnMob(17842580);
		DespawnMob(17842581);
	elseif(P >= 63 and P <= 75)then
		GetNPCByID(17842684):openDoor(3600);
		GetNPCByID(17842685):openDoor(3600);
		DespawnMob(17842585);
		DespawnMob(17842586);
		DespawnMob(17842587);
		DespawnMob(17842588);
		DespawnMob(17842589);
		DespawnMob(17842590);
	elseif(T >= 63 and T <= 75)then
		GetNPCByID(17842686):openDoor(3600);
		GetNPCByID(17842687):openDoor(3600);
		DespawnMob(17842594);
		DespawnMob(17842595);
		DespawnMob(17842596);
		DespawnMob(17842597);
		DespawnMob(17842598);
		DespawnMob(17842599);
	end
end;