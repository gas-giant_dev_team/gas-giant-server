----------------------------------
-- Area: Labyrinth of Onzozo
-- NM: Narasimha
-- @zone 213
-- @pos -123.000 -0.600 119.000
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)

	-- Set Narasimha's Window Open Time
    local wait = math.random((21600),(36000)); -- 6-10 hours
	SetServerVariable("[POP]Narasimha", os.time(t) + (wait /NM_TIMER_MOD));
	DeterMob(mob:getID(), true);

	-- Set PH back to normal, then set to respawn spawn
    local PH = GetServerVariable("[PH]Narasimha");
	SetServerVariable("[PH]Narasimha", 0);
	DeterMob(PH, false);
	GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

end;