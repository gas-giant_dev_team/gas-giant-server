-----------------------------------
-- Area: Fei'Yin
-- NM: Capricious_Cassie
-- @zone 204
-- @pos -69.000 0.075 189.000
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/titles");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)

    killer:addTitle(CASSIENOVA);

    -- Set Capricious_Cassie's spawnpoint and respawn time (21-24 hours)
    UpdateNMSpawnPoint(mob:getID());
	mob:setRespawnTime((math.random((75600),(86400))) /LongNM_TIMER_MOD);

end;