-----------------------------------
-- Area: Fei'Yin
-- NM: Goliath
-- @zone 204
-- @pos -168.000 -0.500 130.000
-----------------------------------

require("scripts/globals/titles");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	killer:addTitle(GOLIATH_KILLER);	

end;