-----------------------------------
-- Area: Castle Oztroja
-- NPC:  Brass Statue
-- Type: Passageway Machine
-- @zone 151
-- @pos -60.061, -4.348, -61.538 	(1)
-- @pos -18.599, -19.307, 20.024 	(2)
-- @pos -59.964, 20.649, -102.820	(3)
-- @pos -100.106, -74.486, -16.831	(4)
-----------------------------------
package.loaded["scripts/zones/Castle_Oztroja/TextIDs"] = nil;
-----------------------------------

require("scripts/zones/Castle_Oztroja/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	local Z = npc:getZPos();

	if(Z < -15 and Z > -19) then

		local DoorID = npc:getID() - 1;
		local DoorA = GetNPCByID(DoorID):getAnimation();

		if(DoorA == 9) then
			GetNPCByID(DoorID):openDoor(6);
		end 
	end
end;

	--player:startEvent(0x000d); -- Password event

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;