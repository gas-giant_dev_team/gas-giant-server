-----------------------------------
-- Area: Jugner Forest (104)
--  NM:  Fraelissa
-- @zone 104
-- @pos 9.320, -0.493, -371.654
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)	

    -- Set Fraelissa spawnpoint and respawn time (1-1.15 hours)
    UpdateNMSpawnPoint(mob:getID());
    mob:setRespawnTime((math.random((3600),(4500))) /NM_TIMER_MOD);

end;