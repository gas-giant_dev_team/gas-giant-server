-----------------------------------
-- Area: West Sarutabaruta
--  VNM: Orcus
-- @zone 115
-- @pos
-----------------------------------

require("scripts/globals/titles");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	killer:addTitle(ORCUS_TROPHY_HUNTER);
end;