-----------------------------------
--  Area: West Sarutabaruta (115)
--  NM:   Nunyenunc
-- @zone 115
-- @pos -25.000, -15.000, 385.000
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)

    -- Set Nunyenunc's Window Open Time
    local wait = math.random((7200),(10800))
    SetServerVariable("[POP]Nunyenunc", os.time(t) + (wait /NM_TIMER_MOD)); -- 2-3 hours
    DeterMob(mob:getID(), true);

    -- Set PH back to normal, then set to respawn spawn
    local PH = GetServerVariable("[PH]Nunyenunc");
    SetServerVariable("[PH]Nunyenunc", 0);
    DeterMob(PH, false);
    GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

end;

