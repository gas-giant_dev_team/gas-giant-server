-----------------------------------
-- Area: West Sarutabaruta
-- MOB:  Tiny Mandragora
-- @zone 115
-- @pos many
-----------------------------------

require("/scripts/globals/fieldsofvalor");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)
	checkRegime(killer,mob,26,1);
end;
