-----------------------------------
-- Area: West Sarutabaruta
-- NPC: Goblin Footprint
-- Missions: ASA 2 Burgeoning Dread
-- @zone 115
-- @pos 148.704 -1.992 -333.754
-----------------------------------
package.loaded["scripts/zones/West_Sarutabaruta/TextIDs"] = nil;
package.loaded["scripts/globals/missions"] = nil;
-----------------------------------

require("scripts/globals/keyitems");
require("scripts/globals/missions");
require("scripts/zones/West_Sarutabaruta/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	local CurrentMission = player:getCurrentMission(ASA);

	if (CurrentMission == BURGEONING_DREAD) then
		cs = math.random(62,63);
		
		if (cs == 62) then
			player:startEvent(0x003E);
		elseif (cs == 63) then
			player:startEvent(0x003F);
		end
	end
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("OPTION: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("OPTION: %u",option);
end;