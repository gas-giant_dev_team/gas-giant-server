-----------------------------------	
-- Area: East Sarutabaruta	
-- NM: Spiny Spipi
-- @zone 116
-- @pos 270.957, -12.815, 118.668
-----------------------------------	

require("scripts/globals/settings");

-----------------------------------	
-- onMobDeath	
-----------------------------------	
	
function onMobDeath(mob,killer)	

	-- Set Spiny_Spipi's Window Open Time
	wait = math.random((2700),(7200))
	SetServerVariable("[POP]Spiny_Spipi", os.time(t) + (wait /NM_TIMER_MOD)); -- 45 - 120 minutes
	DeterMob(mob:getID(), true);

	-- Set PH back to normal, then set to respawn spawn
	PH = GetServerVariable("[PH]Spiny_Spipi");
	SetServerVariable("[PH]Spiny_Spipi", 0);
	DeterMob(PH, false);
	GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

end;
