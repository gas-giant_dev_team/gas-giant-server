-----------------------------------
-- Area: North Gustaberg
-- NPC: Field Manual
-- @zone 106
-- @pos 625.019 -1.045 316.154
-- @pos -596.486 40.213 57.049
-- @pos 166.759 -0.438 693.964
-----------------------------------

require("/scripts/globals/settings");
require("/scripts/globals/fieldsofvalor");

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	startFov(FOV_EVENT_NORTH_GUSTABERG,player);
end;

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onEventSelection
-----------------------------------

function onEventUpdate(player,csid,menuchoice)
	updateFov(player,csid,menuchoice,16,17,18,19,59);
end;

-----------------------------------
-- onEventFinish Action
-----------------------------------

function onEventFinish(player,csid,option)
	finishFov(player,csid,option,16,17,18,19,59,FOV_MSG_NORTH_GUSTABERG);
end;
