-----------------------------------
-- Area: Sauromuge Champaign
-- NM: Old Sabertooth
-- Quests: The Fanged One
-- @zone 120
-- @pos 676.000 -10.000 -366.000
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/status");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function onMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	killer:setVar("TheFangedOne_Died",0);
end;
