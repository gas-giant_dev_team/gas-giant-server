-----------------------------------
-- Area: Ro'Maeve
-- NPC: _3e1 (Moongate)
-- Type: Door
-- @zone 122
-- @pos -184.516 -7.146 14.298
-----------------------------------

require("/scripts/globals/settings");
require("/scripts/globals/keyitems");

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	if(player:hasKeyItem(MOONGATE_PASS) == true) then
		npc:openDoor(30);
	end
end;

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onEventSelection
-----------------------------------

function onEventUpdate(player,csid,menuchoice)
end;

-----------------------------------
-- onEventFinish Action
-----------------------------------

function onEventFinish(player,csid,option)
end;
