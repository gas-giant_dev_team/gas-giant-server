-----------------------------------
-- Area: Bastok Markets
-- NPC: Olwyn
-- Type: Standard Merchant NPC
-- @zone 235
-- @pos -322.123 -10.319 -169.418
-----------------------------------
package.loaded["scripts/zones/Bastok_Markets/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/harvest_festivals");
require("scripts/globals/shop");
require("scripts/zones/Bastok_Markets/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
	onHalloweenTrade(player,trade,npc)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
    player:showText(npc,OLWYN_SHOP_DIALOG);

    local stock = {
        0x1020,   445,1,     --Ether

        0x1037,   736,2,     --Echo Drops
        0x1010,   837,2,     --Potion

        0x1036,  2387,3,     --Eye Drops
        0x1034,   290,3      --Antidote
    }
    showNationShop(player, BASTOK, stock);

end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;