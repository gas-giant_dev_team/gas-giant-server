-----------------------------------
-- Area: Davoi (149)
-- Mob: Blubbery Bulge
-- Quest: The Miraculous Dale
-- @zone 149
-- @pos 
-----------------------------------

-- require("scripts/zones/Davoi/MobIDs");
require("scripts/globals/quests");

-----------------------------------
-- onMobInitialize
-----------------------------------

function onMobInitialize(mob)	
end;

-----------------------------------
-- onMobSpawn
-----------------------------------

function onMobSpawn(mob)	
end;

-----------------------------------
-- onMobEngaged
-----------------------------------

function onMobEngaged(mob,target)	
end;

-----------------------------------
-- onMobFight
-----------------------------------

function onMobFight(mob,target)	
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)
	local DALEQuest = player:getVar("DALEQuest");
	if(player:getQuestStatus(JEUNO,THE_MIRACULOUS_DALE) == QUEST_ACCEPTED) then
		DALEQuest = player:setMaskBit(DALEQuest,"DALEQuest",1,true);
	end
end;