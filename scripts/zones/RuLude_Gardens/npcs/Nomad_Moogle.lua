-----------------------------------
-- Area: Ru'Lude Gardens
-- NPC: Nomad Moogle
-- Quests: Moogle Merit Management,
-- Limit Break 6 - 10
-- Type: Mog House Management Union
-- @zone 243
-- @pos 10.012 1.453 121.883
-- Custscenes
-- 0x004f - Maat Quest "In Defiant Challenge"
-- 0x0050 - Maat During Quest "In Defiant Challenge"
-- 0x0051 - Maat Finish Quest "In Defiant Challenge"
-- 0x0052 - Maat Quest "Atop the Highest Mountains"
-- 0x0053 - Maat During Quest "Atop the Highest Mountains"
-- 0x0054 - Maat Finish Quest "Atop the Highest Mountains"
-- 0x0055 - Maat Quest "Whence Blows the Wind"
-- 0x0056 - Maat During Quest "Whence Blows the Wind"
-- 0x0057 - Maat Finish Quest "Whence Blows the Wind"
-- 0x0058 - Maat Quest "Riding on the Clouds"
-- 0x0059 - Maat During Quest "Riding on the Clouds"
-- 0x005a - Maat Finish Quest "Riding on the Clouds"
-- 0x005c - Maat Start Quest "Shattering Stars"
-- 0x005b - Maat During Quest "Shattering Stars"
-- 0x0040 - Teleport to battlefield for "Shattering Stars"
-- 0x003e - Maat Testimony is a piece of junk ..
-- 0x003f - Maat Show me that testimony and I'll warp you ..
-- 0x005d - Maat Finish Quest "Shattering Stars"
-- 0x004e - Maat Now that you reached the stars ... fight Maat again
-- 0x005e - Maat Heeh heh heh. I see I see ...
-- 0x0075 - Maat Quest Beat Around The Bushin
-- 0x004a - Maat's Cap complete
-- 0x273d - I need a little time alone to think, come back later
-- 0x2797 - Moogle Merit Management cs
-- 0x2798 - New Worlds Await Quest finish ??
-- 0x2799 - CS Degenhard for Maat fight Beyond the Stars start??
-- 0x27b1 - Beyond the Stars (rock paper scissors cs) finish quest ??
-- 0x27cf - Quest Dormant Powers Dislodged
-- 0x27d0 - Dormant Powers Dislodged finish
-- 0x27d2 - Quest Prelude to Puissance
-- 0x27d3 - Trade complete 1 merit for Soul Gem Clasp
-- 0x279b - Beyond Infinity fight complete (final cs)
-- 0x27d4 - Quest Martial Mastery
-- 0x27d6 - Martial mastery returned with 15 merit points
-- 0x27b3 - Magician Moogle Quest A Trial in Tandem
-- 0x27b4 - Magician Moogle Quest A Trial in Tandem Redux
-- 0x27b5 - Magician Moogle Quest Another Trial in Tandem
-- 0x27b6 - Magician Moogle Quest A Quaternary Trial in Tandem
-- 0x27b7 - Magician Moogle Quest A Trial in Tandem Revisited
-- 0x27b8 - Magician Moogle Quest A Trial in Tandem Revisited
-- 0x27b9 - Magician Moogle Lost item, restart quest
-- 0x27ba - Magician Moogle Lost item, restart quest
-- 0x27bb - Magician Moogle Lost item, restart quest
-- 0x27bc - Magician Moogle Lost item, restart quest
-- 0x27bd - Magician Moogle Lost item, restart quest
-- 0x27be - Magician Moogle Lost item, restart quest
-----------------------------------
package.loaded["scripts/zones/RuLude_Gardens/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/keyitems");
require("scripts/globals/quests");
require("scripts/zones/RuLude_Gardens/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
	local tradeCount = trade:getItemCount();
	if((player:getQuestStatus(JEUNO,NEW_WORLDS_AWAIT) == QUEST_AVAILABLE or player:getQuestStatus(JEUNO,NEW_WORLDS_AWAIT) == QUEST_ACCEPTED) and player:getMainLvl() >= 75) then -- will need reworked once quest cs is found
		-- Trade 5x Kindred Seals / 3 Merit Points
		if(trade:hasItemQty(1127,5) and  tradeCount == 5) then
			if(player:getMeritPoints() >= 3) then
				player:addQuest(JEUNO,NEW_WORLDS_AWAIT);
				player:setVar("newWorldsAwait",1);
				player:startEvent(0x2798);
			end
		end
	elseif(player:getQuestStatus(JEUNO,EXPANDING_HORIZONS) == QUEST_AVAILABLE and player:getMainLvl() >= 76) then -- will need reworked once quest cs is found
		-- Trade 5x Kindred Crests / 4 Merit Points
		if(trade:hasItemQty(2955,5) and  tradeCount == 5) then
			if(player:getMeritPoints() >= 4) then
				player:addQuest(JEUNO,EXPANDING_HORIZONS);
				player:setVar("expandingHorizons",1);
				player:startEvent(0x2798);
			end
		end
	elseif(player:getQuestStatus(JEUNO,BEYOND_THE_STARS) == QUEST_AVAILABLE and player:getMainLvl() >= 81) then -- will need reworked once quest cs is found
		-- Trade 10x Kindred Crests / 5 Merit Points
		if(trade:hasItemQty(2955,10) and  tradeCount == 10) then
			if(player:getMeritPoints() >= 5) then
				player:addQuest(JEUNO,BEYOND_THE_STARS);
				player:startEvent(0x2799);
			end
		end
	--elseif(player:getQuestStatus(JEUNO,DORMANT_POWERS_DISLODGED) == QUEST_ACCEPTED) then
		-- Trade 1x Kindred rest, 1x Randomly Chosen Item, and 10 Merit Points
		--player:startEvent(0x27CF ; -- 
	end
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	if(player:hasKeyItem(LIMIT_BREAKER) == false and player:getMainLvl() >= 75) then
		-- player:startEvent(0x2797);
		player:startEvent(0x273D,0x4B,0x02,0x0A,0x07,0x1E,0x049F2F,0x0FFF);
	-- elseif(player:getQuestStatus(JEUNO,NEW_WORLDS_AWAIT) == QUEST_AVAILABLE and player:getMainLvl() == 75) then
		-- player:startEvent(0x0000); -- New Worlds Awaits cs unknown ??
	-- elseif(player:getQuestStatus(JEUNO,EXPANDING_HORIZONS) == QUEST_AVAILABLE and player:getMainLvl() >= 76) then
		-- player:startEvent(0x0000); -- Expanding Horizons cs unknown ??
	-- elseif(player:getQuestStatus(JEUNO,BEYOND_THE_STARS) == QUEST_AVAILABLE and player:getMainLvl() >= 81) then
		-- Quest does not start witha  cs, starst with dialog.
	elseif(player:getQuestStatus(JEUNO,BEYOND_THE_STARS) == QUEST_ACCEPTED) then
		player:startEvent(0x27B1); -- Rock Paper Scissors
	--elseif(player:getQuestStatus(JEUNO,DORMANT_POWERS_DISLODGED) == QUEST_AVAILABLE and player:getMainLvl() >= 86) then
		--player:startEvent(? ; -- 
	-- elseif(player:getQuestStatus(JEUNO,Beyond Infinity) == QUEST_AVAILABLE and player:getMainLvl() >= 91) then
		-- player:startEvent();
	else
		player:startEvent(0x005E);
	end

end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);

	-- if(csid == 0x2797) then
	if(csid == 0x273D and option ~= -1) then
		player:addKeyItem(LIMIT_BREAKER);
		player:messageSpecial(KEYITEM_OBTAINED,LIMIT_BREAKER);
	elseif(csid == 0x2798) then
		player:tradeComplete();
		if(player:getVar("newWorldsAwait") == 1) then
			player:setMerits(player:getMeritPoints() - 3);
			player:levelCap(80);
			player:messageSpecial(YOUR_LEVEL_LIMIT_IS_NOW_80);
			player:completeQuest(JEUNO,NEW_WORLDS_AWAIT);
			player:addFame(JEUNO, JEUNO_FAME*30);
			player:setVar("newWorldsAwait",0);
		elseif(player:getVar("expandingHorizons") == 1) then
			player:setMerits(player:getMeritPoints() - 4);
			player:levelCap(85);
			player:messageSpecial(YOUR_LEVEL_LIMIT_IS_NOW_85);
			player:completeQuest(JEUNO,EXPANDING_HORIZONS);
			player:addFame(JEUNO, JEUNO_FAME*40);
			player:setVar("expandingHorizons",0);
		end
	elseif(csid == 0x2799) then
		player:tradeComplete();
		player:setMerits(player:getMeritPoints() - 5);
	elseif(csid == 0x27B1) then
		player:levelCap(90); -- auto win ??
		player:messageSpecial(YOUR_LEVEL_LIMIT_IS_NOW_90);
		player:addFame(JEUNO, JEUNO_FAME*50);
		player:completeQuest(JEUNO,BEYOND_THE_STARS);
	-- elseif(csid == 0x279B) then
		-- player:messageSpecial(YOUR_LEVEL_LIMIT_IS_NOW_95);
		-- player:completeQuest(JEUNO,BEYOND_INFINITY);
	end

end;