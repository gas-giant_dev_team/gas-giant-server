-----------------------------------
-- Area: Batallia Downs (105)
-- NM: Lumber Jack
-- @zone 105
-- @pos -670.000 -23.000 352.000
-----------------------------------

-- require("scripts/zones/Batallia_Downs/MobIDs");

-----------------------------------
-- onMobInitialize
-----------------------------------

function onMobInitialize(mob)
end;

-----------------------------------
-- onMobSpawn
-----------------------------------

function onMobSpawn(mob)
end;

-----------------------------------
-- onMobEngaged
-----------------------------------

function onMobEngaged(mob,target)
end;

-----------------------------------
-- onMobFight
-----------------------------------

function onMobFight(mob,target)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)

	-- Set Weeping Willow's respawn time (21-24 hours)
	--UpdateNMSpawnPoint(mob:getID(17207302));
	GetMobByID(17207302):setRespawnTime((math.random((75600),(86400))) /LongNM_TIMER_MOD);

end;
