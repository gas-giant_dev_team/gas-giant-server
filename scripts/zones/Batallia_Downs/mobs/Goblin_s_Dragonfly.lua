-----------------------------------
-- Area: Batallia Downs (105)
-- Mob: Goblin's Dragonfly
-- @zone 105
-- @pos many
-----------------------------------
package.loaded["scripts/zones/Batallia_Downs/TextIDs"] = nil;
-----------------------------------

-- require("scripts/zones/Batallia_Downs/MobIDs");
require("scripts/globals/settings");
require("scripts/globals/keyitems");
require("scripts/zones/Batallia_Downs/TextIDs");

-----------------------------------
-- onMobInitialize
-----------------------------------

function onMobInitialize(mob)
end;

-----------------------------------
-- onMobSpawn
-----------------------------------

function onMobSpawn(mob)
end;

-----------------------------------
-- onMobEngaged
-----------------------------------

function onMobEngaged(mob,target)
end;

-----------------------------------
-- onMobFight
-----------------------------------

function onMobFight(mob,target)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)

	if(ENABLE_ACP == 1 and (killer:hasKeyItem(BOWL_OF_BLAND_GOBLIN_SALAD) == false) and killer:getCurrentMission(ACP) >= THE_ECHO_AWAKENS) then
		-- Guesstimating 15% chance
		if (math.random((1),(100)) >= 85) then
			killer:addKeyItem(BOWL_OF_BLAND_GOBLIN_SALAD);
			killer:messageSpecial(KEYITEM_OBTAINED,BOWL_OF_BLAND_GOBLIN_SALAD);
		end
	end

end;