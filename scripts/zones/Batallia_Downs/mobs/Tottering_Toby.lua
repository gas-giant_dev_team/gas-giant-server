-----------------------------------
-- Area: Batallia Downs (105)
-- Mob: Tottering Toby
-- Quests: The Miraculous Dale
-- @zone 105
-- @pos -255.542, -7.872, 185.826
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/quests");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)

	-- Set Tottering_Toby's Window Open Time
	wait = math.random((3600),(14400));
	SetServerVariable("[POP]Tottering_Toby", os.time(t) + (wait /NM_TIMER_MOD)); -- 1-4 hours
	DeterMob(mob:getID(), true);

	-- Set PH back to normal, then set to respawn spawn
	PH = GetServerVariable("[PH]Tottering_Toby");
	SetServerVariable("[PH]Tottering_Toby", 0);
	DeterMob(PH, false);
	GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

	local DALEQuest = killer:getVar("DALEQuest");
	if(killer:getQuestStatus(JEUNO,THE_MIRACULOUS_DALE) == QUEST_ACCEPTED) then
		DALEQuest = killer:setMaskBit(DALEQuest,"DALEQuest",14,true);
	end

end;

