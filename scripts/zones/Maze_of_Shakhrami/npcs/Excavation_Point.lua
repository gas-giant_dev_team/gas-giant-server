-----------------------------------
-- Area: Maze of Shakhrami
-- NPC: Excavation Point
-- Quests: The Holy Crest
-- @zone 198
-- @pos 233.929 0.307 31.796
-- @pos 170.729 0.117 -91.409
-- @pos 235.017 -0.918 -107.491
-- @pos 255.750 -0.179 -144.886
-- @pos 255.110 0.364 -169.030
-- @pos 123.376 0.949 28.916
-----------------------------------
package.loaded["scripts/zones/Maze_of_Shakhrami/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/excavation");
require("scripts/globals/teleports");
require("scripts/zones/Maze_of_Shakhrami/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)

	if(player:getVar("TheHolyCrest_Event") == 3 and player:hasItem(1159) == false) then
		if(trade:hasItemQty(605,1) and trade:getItemCount() == 1) then
			if(player:getFreeSlotsCount(0) >= 1) then
				player:tradeComplete();
				player:addItem(1159);
				player:messageSpecial(ITEM_OBTAINED, 1159); -- Wyvern Egg
				debugTeleport(player,17723419); -- Morjean, zone 231, 98 x
			else
				player:messageSpecial(ITEM_CANNOT_BE_OBTAINED, 1159); -- Wyvern Egg
			end
		end
	else
		startExcavation(player,player:getZone(),npc,trade,0x003C);
	end

end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	player:messageSpecial(MINING_IS_POSSIBLE_HERE,605);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
end;