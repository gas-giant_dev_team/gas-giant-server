-----------------------------------
-- Area: Maze of Shakhrami (198)
-- Mob: Lost Soul
-- @zone 198
-- @pos 245.000 19.000 -142.000
-----------------------------------

require("scripts/globals/settings");
require("scripts/zones/Maze_of_Shakhrami/TextIDs");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)

	if(killer:getVar("EquipedforAllOccasions") == 1) then
		killer:setVar("EquipedforAllOccasions",2);
	end
end;

