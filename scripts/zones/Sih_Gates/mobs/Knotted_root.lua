-----------------------------------
-- Area: Knotted root
-- NPC: Sih Gates
-- @zone 268
-- @pos many
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/missions");
require("scripts/globals/titles");
require("scripts/globals/status");
require("scripts/globals/magic");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function onMobSpawn(mob)
end;


-----------------------------------
-- onMobEngaged
-----------------------------------
function onMobEngaged(mob,target)
end;

-----------------------------------
-- onMobDeath Action
-----------------------------------

function onMobDeath(mob,killer)

	local A = GetMobAction(17875096);
	local B = GetMobAction(17875097);
	local C = GetMobAction(17875098);
	local E = GetMobAction(17875105);
	local F = GetMobAction(17875106);
	local G = GetMobAction(17875107);

	local D = A+B+C;
	local H = E+F+G;

	if(D >= 63 and D <= 75)then
		GetNPCByID(17875188):openDoor(3600);
		GetNPCByID(17875189):openDoor(3600);
		DespawnMob(17875099);
		DespawnMob(17875100);
		DespawnMob(17875101);
		DespawnMob(17875102);
		DespawnMob(17875103);
		DespawnMob(17875104);
	elseif(H >= 63 and H <= 75)then
		-- GetNPCByID(17875190):openDoor(3600);
		-- GetNPCByID(17863491):openDoor(3600);
		DespawnMob(17875108);
		DespawnMob(17875109);
		DespawnMob(17875110);
		DespawnMob(17875111);
		DespawnMob(17875112);
		DespawnMob(17875113);
	end
end;
