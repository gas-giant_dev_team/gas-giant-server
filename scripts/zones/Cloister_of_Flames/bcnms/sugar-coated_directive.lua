-----------------------------------
-- Area: Cloister of Flames
-- BCNM: Sugar-Coated Directive
-- @zone 207
-- @pos -721 0 -598
-----------------------------------
package.loaded["scripts/zones/Cloister_of_Flames/TextIDs"] = nil;
-------------------------------------

require("scripts/globals/keyitems");
require("scripts/globals/shop");
require("scripts/globals/quests");
require("scripts/globals/missions");
require("scripts/zones/Cloister_of_Flames/TextIDs");

-----------------------------------

-- After registering the BCNM via bcnmRegister(bcnmid)
function OnBcnmRegister(player,instance)
end;

-- Physically entering the BCNM via bcnmEnter(bcnmid)
function OnBcnmEnter(player,instance)
end;

-- Leaving the BCNM by every mean possible, given by the LeaveCode
-- 1=Select Exit on circle
-- 2=Winning the BC
-- 3=Disconnected or warped out
-- 4=Losing the BC
-- via bcnmLeave(1) or bcnmLeave(2). LeaveCodes 3 and 4 are called
-- from the core when a player disconnects or the time limit is up, etc

function OnBcnmLeave(player,instance,leavecode)
	-- print("leave code "..leavecode);

	CurrentMission = player:getCurrentMission(ASA);

	if(leavecode == 2) then -- play end CS. Need time and battle id for record keeping + storage
		player:setVar("ASA_primals_count",player:getVar("ASA_primals_count")+1);
		player:setVar("ASA_ifrit_done",2);
		player:startEvent(0x7d01,1,1,1,instance:getTimeInside(),1,0,0);
	elseif(leavecode == 4) then
		player:setVar("TrialSizeFire_date",tonumber(os.date("%j"))); -- If you loose, you need to wait 1 real day
		player:startEvent(0x7d02);
	end

end;

function onEventUpdate(player,csid,option)
	-- print("bc update csid "..csid.." and option "..option);
end;

function onEventFinish(player,csid,option)
	-- print("bc finish csid "..csid.." and option "..option);
end;