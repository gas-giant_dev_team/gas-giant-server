-----------------------------------
-- Area: Outer Ra'Kaznar
-- NPC: Vertical Transit Device
-- Zones to Ra'Kaznar Inner Court (zone 276)
-- @zone 274
-- @pos -414 -142 -20
-----------------------------------
package.loaded["scripts/zones/Outer_RaKaznar/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/zones/Outer_RaKaznar/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
    local X = player:getXPos();
    local Y = player:getYPos();
    local Z = player:getZPos();

    if (X < -404 and X > -424 and Y < -132 and Y > -152 and Z < -10 and Z > -30) then
        player:startEvent(0x002b);
    end

end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
    -- printf("CSID: %u",csid);
    -- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
    -- printf("CSID: %u",csid);
    -- printf("RESULT: %u",option);
    if (csid == 0x002b and option == 1) then
        player:setPos(-476,-520.5,20,0,276);
    end
end;