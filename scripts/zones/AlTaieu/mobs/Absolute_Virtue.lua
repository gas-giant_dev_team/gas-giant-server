-----------------------------------
-- Area: Al'Taieu
--  NM:  Absolute Virtue
-- @zone 33
-- @pos 461.266 -1.643 -580.192
-----------------------------------
package.loaded["scripts/zones/AlTaieu/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/titles");
require("scripts/zones/AlTaieu/TextIDs" );

-----------------------------------
-- onMobInitialize Action
-----------------------------------

function onMobInitialize(mob)
	-- mob:addMod(MOD_REGEN, 60); -- ?? too much / little
end;

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobFight Action
-----------------------------------
function onMobFight(mob, target)
	local spawnNumber = mob:getExtraVar(1);

	if(spawnNumber == 0 or spawnNumber == nil) then
		spawnNumber = 1;
		mob:setExtraVar(spawnNumber);
	end

	local AV = mob:getID();

	for i = AV+1, AV+3, 1 do
		if (GetMobAction(i) == 16) then
			GetMobByID(i):updateEnmity(target);
		end
	end

	if (mob:getHP() <= 50000 and spawnNumber == 3) then
		spawnNumber = spawnNumber + 1;
		mob:setExtraVar(spawnNumber);
		for i = AV+1, AV+3, 1 do
			if (GetMobAction(i) == 0) then
				SpawnMob(i):updateEnmity(target);
				GetMobByID(i):setPos(mob:getXPos()+1, mob:getYPos(), mob:getZPos()+1);
			end
		end
	elseif (spawnNumber == 2 and mob:getHP() <= 100000) then
		spawnNumber = spawnNumber + 1;
		mob:setExtraVar(spawnNumber);
		for i = AV+1, AV+3, 1 do
			if (GetMobAction(i) == 0) then
				SpawnMob(i):updateEnmity(target);
				GetMobByID(i):setPos(mob:getXPos()+1, mob:getYPos(), mob:getZPos()+1);
			end
		end
	elseif (spawnNumber == 1 and mob:getHP() <= 150000) then
		spawnNumber = spawnNumber + 1;
		mob:setExtraVar(spawnNumber);
		for i = AV+1, AV+3, 1 do
			SpawnMob(i):updateEnmity(target);
			GetMobByID(i):setPos(mob:getXPos()+1, mob:getYPos(), mob:getZPos()+1);
		end
	end
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	killer:addTitle(VIRTUOUS_SAINT);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
	if (option==1)then
		player:setVar("MemoryReceptacle",0);
	end
end;