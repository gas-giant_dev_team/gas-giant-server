-----------------------------------
-- Area: Al'Taieu
-- NPC: Auroral Updraft
-- Type: Standard NPC
-- @zone 33
-- @pos -31.799 -1.000 -618.700
-- @pos -521.000 -1.000 4.500
-- @pos 601.783 -1.000 36.745
-- @pos 197.899 -2.951 441.000
-- @pos -640.900 -2.799 284.700
-- @pos 399.999 -3.117 232.500
-----------------------------------
package.loaded["scripts/zones/AlTaieu/TextIDs"] = nil;
-----------------------------------

require("scripts/zones/AlTaieu/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	local Auroral_Offset = 16912900;
	local npcID = npc:getID();

	if(npcID == Auroral_Offset) then
		player:startEvent(0x0096);
	elseif(npcID == Auroral_Offset+1) then
		player:startEvent(0x009B);
	elseif(npcID == Auroral_Offset+2) then
		player:startEvent(0x009B);
	elseif(npcID == Auroral_Offset+3) then
		player:startEvent(0x009B);
	elseif(npcID == Auroral_Offset+4) then
		player:startEvent(0x009B);
	elseif(npcID == Auroral_Offset+5) then
		player:startEvent(0x009B);
	end
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
	if(csid == 0x009B) and option == 1 then
		player:setPos(-25,-1 ,-620 ,208 ,33);
	elseif(csid == 0x0096) and option == 1 then
		player:setPos(611.931, 132.787, 773.427, 192, 32); -- To Sealion's Den {R}
	end

end;
