-----------------------------------
-- Area: Bastok Mines
-- NPC: Aulavia
-- Type: Regional Marchant NPC
-- @zone 234
-- @pos -49.456 0.000 -38.320
-- Notes: Only sells when Bastok
-- controls Vollbow.
-----------------------------------
package.loaded["scripts/zones/Bastok_Mines/TextIDs"] = nil;
-----------------------------------
require("scripts/globals/harvest_festivals");
require("scripts/globals/shop");
require("scripts/globals/conquest");
require("scripts/zones/Bastok_Mines/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
	onHalloweenTrade(player,trade,npc)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	local RegionOwner = GetRegionOwner(VOLLBOW);

	if (RegionOwner ~= BASTOK) then
		player:showText(npc,AULAVIA_CLOSED_DIALOG);
	else
		player:showText(npc,AULAVIA_OPEN_DIALOG);
		local stock = 
		{
			0x27c,   119,    -- Chamomile
			0x360,    88,    -- Fish Scales
			0x3a8,    14,    -- Rock Salt
			0x582,  1656     -- Sweet William
		}
		showShop(player,BASTOK,stock);

	end

end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;
