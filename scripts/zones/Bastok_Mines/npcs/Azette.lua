-----------------------------------
-- Area: Bastok Mines
-- NPC: Azette
-- Type: Chocobo Renter
-- @zone 234
-- @pos 43.882 0.750 -108.629
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/keyitems");


-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
	local hasLicense = player:hasKeyItem(CHOCOBO_LICENSE);
	local level = player:getMainLvl();
	local count = trade:getItemCount();
	local ChocoboTicket = trade:hasItemQty(1514,1);
	local Chocopass = trade:hasItemQty(1789,1);
	local durationMod = 0;
	durationMod = player:getMod(MOD_CHOCOBO_TIME) * 60;

	if(ChocoboTicket == true and count == 1) then
		if (hasLicense and level >= 20) then
			player:tradeComplete();
			player:addStatusEffectEx(EFFECT_CHOCOBO,EFFECT_CHOCOBO,1,0,(1800 + durationMod),true);
			player:setPos(580,0,-305,0x40,0x6b);
		end
	elseif(Chocobopass == true and count == 1) then
		player:tradeComplete();
		player:addStatusEffectEx(EFFECT_CHOCOBO,EFFECT_CHOCOBO,1,0,(120 + durationMod),true);
		player:setPos(580,0,-305,0x40,0x6b);
	end
end;


-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	local price = 100;
	local gil = player:getGil();
	local hasLicense = player:hasKeyItem(CHOCOBO_LICENSE);
	local level = player:getMainLvl();

	if (hasLicense and level >= 15) then
		player:startEvent(0x003d,price,gil);
	else
		player:startEvent(0x0040,price,gil);
	end

end;

-----------------------------------
-- onEventFinish Action
-----------------------------------
function onEventFinish(player,csid,option)
	-- print("CSID:",csid);
	-- print("OPTION:",option);
	local durationMod = 0;
	durationMod = player:getMod(MOD_CHOCOBO_TIME) * 60;
	local price = 100;

	if (csid == 0x003d and option == 0) then
		if (player:delGil(price)) then
			if (player:getMainLvl() >= 20) then
				player:addStatusEffectEx(EFFECT_CHOCOBO,EFFECT_CHOCOBO,1,0,(1800 + durationMod),true);
			else
				player:addStatusEffectEx(EFFECT_CHOCOBO,EFFECT_CHOCOBO,1,0,(900 + durationMod),true);
			end
			player:setPos(580,0,-305,0x40,0x6b);
		end
	end

end;