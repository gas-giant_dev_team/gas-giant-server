-----------------------------------
-- Area: Crawler's Nest
-- NM: Matron Crawler
-- @zone 197
-- @pos -141.158 -33.689 20.944
-- Note: Spawned by trading a
-- Rolanberry 874 to the basket at
-- F-7 on the first map. Does not
-- spawn 100% of the time, can
-- display the message "Nothing
-- Happened", which still uses up
-- a Rolanberry 874. 
-----------------------------------


-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	GetNPCByID(17584458):hideNPC(900); -- qm6
end;