-----------------------------------
-- Area: Crawler's Nest
-- NM: Guardian Crawler
-- @zone 197
-- @pos 124.335 -34.609 -75.373
-- (Spawn Area 1)
-- @pos 44.436 -2.602 195.381
-- (Spawn Area 2)
-- Note: Spawned by trading a
-- Rolanberry to the basket at J-9
-- on the first map, or H-8 on
-- second map. Does not spawn 100%
-- of the time, can display the
-- message "Nothing Happened",
-- which still uses up a Rolanberry. 
-----------------------------------


-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)

	local mob = mob:getID();
	 
	if (mob == 17584129) then
		GetNPCByID(17584453):hideNPC(900); -- qm1

	elseif (mob == 17584130) then
		GetNPCByID(17584454):hideNPC(900); -- qm2

   end

end;
