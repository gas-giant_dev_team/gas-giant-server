-----------------------------------
-- Area: Rolanberry Fields (110)
-- Mob: Goblin's Bee
-- @zone 110
-- @pos many
-----------------------------------
package.loaded["scripts/zones/Rolanberry_Fields/TextIDs"] = nil;
-----------------------------------

-- require("scripts/zones/Rolanberry_Fields/MobIDs");
require("/scripts/globals/fieldsofvalor");
require("scripts/globals/settings");
require("scripts/globals/keyitems");
require("scripts/zones/Rolanberry_Fields/TextIDs");

-----------------------------------
-- onMobInitialize
-----------------------------------

function onMobInitialize(mob)
end;

-----------------------------------
-- onMobSpawn
-----------------------------------

function onMobSpawn(mob)
end;

-----------------------------------
-- onMobEngaged
-----------------------------------

function onMobEngaged(mob,target)
end;

-----------------------------------
-- onMobFight
-----------------------------------

function onMobFight(mob,target)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)

	if(ENABLE_ACP == 1 and (killer:hasKeyItem(JUG_OF_GREASY_GOBLIN_JUICE) == false) and killer:getCurrentMission(ACP) >= THE_ECHO_AWAKENS) then
		-- Guesstimating 15% chance
		if (math.random((1),(100)) >= 85) then
			killer:addKeyItem(JUG_OF_GREASY_GOBLIN_JUICE);
			killer:messageSpecial(KEYITEM_OBTAINED,JUG_OF_GREASY_GOBLIN_JUICE);
		end
	end

end;