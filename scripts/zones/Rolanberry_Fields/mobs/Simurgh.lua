-----------------------------------
-- Area: Rolanberry Fields (110)
-- HMN:  Simurgh
-- @zone 110
-- @pos -681.000 -31.000 -447.000
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/titles");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobFight Action
-----------------------------------

function onMobFight(mob,target)
end;


-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)

    killer:addTitle(SIMURGH_POACHER);

    -- Set Simurgh's spawnpoint and respawn time (21-24 hours)
    UpdateNMSpawnPoint(mob:getID());
    mob:setRespawnTime((math.random((75600),(86400))) /LongNM_TIMER_MOD);

end;