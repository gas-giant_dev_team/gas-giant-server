-----------------------------------
-- Area: Konschtat Highlands
--  NM: Steelfleece_Baldarich
-- @zone 108
-- @pos -10.000 7.000 45.000
-----------------------------------

require("scripts/globals/titles");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	killer:addTitle(THE_HORNSPLITTER);
end;