-----------------------------------	
-- Area: Konschtat Highlands
-- NPC: Field Manual
-- Type: Item NPC
-- @zone 108
-- @pos -285.839 -48.539 -421.089
-- @pos 203.023 23.999 283.554
-----------------------------------
require("/scripts/globals/fieldsofvalor");

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	startFov(FOV_EVENT_KONSCHTAT,player);
end;

-----------------------------------	
-- onTrade Action
-----------------------------------	

function onTrade(player,npc,trade)	
end;

-----------------------------------	
-- onEventSelection
-----------------------------------

function onEventUpdate(player,csid,menuchoice)
	updateFov(player,csid,menuchoice,20,81,82,83,84);
end;

-----------------------------------
-- onEventFinish Action
-----------------------------------

function onEventFinish(player,csid,option)
	finishFov(player,csid,option,20,81,82,83,84,FOV_MSG_KONSCHTAT);
end;
