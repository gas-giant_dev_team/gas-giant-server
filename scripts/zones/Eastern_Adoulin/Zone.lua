-----------------------------------
-- 
-- Zone: Eastern Adoulin
-- 
-----------------------------------
package.loaded["scripts/zones/Eastern_Adoulin/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/zones/Eastern_Adoulin/TextIDs");

-----------------------------------
-- onInitialize
-----------------------------------

function onInitialize(zone)
end;

-----------------------------------
-- onZoneIn
-----------------------------------

function onZoneIn(player,prevZone)
	local cs = -1;

	if ((player:getXPos() == 0) and (player:getYPos() == 0) and (player:getZPos() == 0)) then
		player:setPos(-59,0.001,-132,196);
		if (GM_SKIP_MOGHOUSE_SAVE == 1 and (player:getGMLevel() >= 1)) then -- No save after Mog House cs yet?
		end
	end

	return cs;
end;

-----------------------------------
-- onRegionEnter          
-----------------------------------

function onRegionEnter(player,region)
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;