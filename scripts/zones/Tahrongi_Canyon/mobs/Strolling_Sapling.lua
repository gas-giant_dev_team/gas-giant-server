-----------------------------------
-- Area: Tahrongi Canyon
--  MOB: Strolling Sapling
-- @zone 117
-----------------------------------

require("/scripts/globals/fieldsofvalor");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)
	checkRegime(killer,mob,30,1);
	if (killer:getVar("Windy_Tutorial_NPC") == 8) then
		killer:setVar("Windy_Tutorial_NPC",9);
	end
end;