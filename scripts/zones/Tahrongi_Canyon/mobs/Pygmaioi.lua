-----------------------------------
-- Area: Tahrongi Canyon
--  MOB: Pygmaioi
-- @zone 117
-----------------------------------

require("/scripts/globals/fieldsofvalor");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)
	checkRegime(killer,mob,94,2);
	checkRegime(killer,mob,95,1);
	if (killer:getVar("Windy_Tutorial_NPC") == 8) then
		killer:setVar("Windy_Tutorial_NPC",9);
	end
end;