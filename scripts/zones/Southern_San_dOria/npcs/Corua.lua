-----------------------------------
-- Area: Southern San d'Oria
-- NPC: Corua
-- Type: Regional Merchant NPC
-- Quests: Flyers for Regine
-- @zone 230
-- @pos -64.239 1.999 -8.665
-- Note: Only sells when San d'Oria
-- controlls Ronfaure Region.
-----------------------------------
package.loaded["scripts/zones/Southern_San_dOria/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/harvest_festivals");
require("scripts/globals/settings");
require("scripts/globals/shop");
require("scripts/globals/quests");
require("scripts/globals/conquest");
require("scripts/zones/Southern_San_dOria/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
	-- "Flyers for Regine" conditional script
	if (player:getQuestStatus(SANDORIA,FLYERS_FOR_REGINE) == 1) then
		if (trade:hasItemQty(532,1) == true and trade:getItemCount() == 1) then
			player:messageSpecial(FLYER_REFUSED);
		end
	else
		onHalloweenTrade(player,trade,npc);
	end
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

		local RegionOwner = GetRegionOwner(RONFAURE);
		-- player:startEvent(0x0351) - Are you the chicks owner
		if (RegionOwner ~= SANDORIA) then
				player:showText(npc,CORUA_CLOSED_DIALOG);
		else
				player:showText(npc,CORUA_OPEN_DIALOG);

			local stock = 
			{
				0x1125,  29, -- San d'Orian Carrot
				0x114f,  69, -- San d'Orian Grape
				0x027f, 110, -- Chestnut
				0x0262,  55  -- San d'Orian Flour
			}
				showShop(player,SANDORIA,stock);
		end

end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
end;