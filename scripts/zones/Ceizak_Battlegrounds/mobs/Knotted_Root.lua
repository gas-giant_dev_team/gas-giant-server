-----------------------------------
-- Area: Ceizak Battlegrounds
-- NPC: Knotted Root
-- @zone 261
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/missions");
require("scripts/globals/titles");
require("scripts/globals/status");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function onMobSpawn(mob)
end;


-----------------------------------
-- onMobEngaged
-----------------------------------
function onMobEngaged(mob,target)
end;

-----------------------------------
-- onMobDeath Action
-----------------------------------

function onMobDeath(mob,killer)

	local A = GetMobAction(17846624);
	local B = GetMobAction(17846625);
	local C = GetMobAction(17846626);
	local E = GetMobAction(17846631);
	local F = GetMobAction(17846632);
	local G = GetMobAction(17846633);
	local I = GetMobAction(17846640);
	local J = GetMobAction(17846641);
	local K = GetMobAction(17846642);
	local M = GetMobAction(17846649);
	local N = GetMobAction(17846650);
	local O = GetMobAction(17846651);
	local P = M+N+O;
	local L = I+J+K;
	local H = E+F+G;
	local D = A+B+C;

	if(D >= 63 and D <= 75)then
		GetNPCByID(17846758):openDoor(3600);
		GetNPCByID(17846759):openDoor(3600);
		DespawnMob(17846627);
		DespawnMob(17846628);
		DespawnMob(17846629);
		DespawnMob(17846630);
	elseif(H >= 63 and H <= 75)then
		GetNPCByID(17846762):openDoor(3600);
		GetNPCByID(17846763):openDoor(3600);
		DespawnMob(17846634);
		DespawnMob(17846635);
		DespawnMob(17846636);
		DespawnMob(17846637);
		DespawnMob(17846638);
		DespawnMob(17846639);
	elseif(L >= 63 and L <= 75)then
		GetNPCByID(17846760):openDoor(3600);
		GetNPCByID(17846761):openDoor(3600);
		DespawnMob(17846643);
		DespawnMob(17846644);
		DespawnMob(17846645);
		DespawnMob(17846646);
		DespawnMob(17846647);
		DespawnMob(17846648);
	elseif(P >= 63 and P <= 75)then
		GetNPCByID(17846763):openDoor(3600);
		GetNPCByID(17846764):openDoor(3600);
		DespawnMob(17846652);
		DespawnMob(17846653);
		DespawnMob(17846654);
		DespawnMob(17846655);
		DespawnMob(17846656);
		DespawnMob(17846657);
	end
end;