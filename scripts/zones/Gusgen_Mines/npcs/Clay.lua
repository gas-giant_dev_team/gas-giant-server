-----------------------------------
-- Area: Gusgen Mines
-- NPC: Clay
-- Quests: A Potter's Preference
-- @zone 196
-- @pos 118.438 -23.477 434.517
-----------------------------------
package.loaded["scripts/zones/Gusgen_Mines/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/titles");
require("scripts/globals/quests");
require("scripts/globals/settings");
require("scripts/zones/Gusgen_Mines/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)

end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	if(player:getFreeSlotsCount(0) >= 1) then
		player:addItem(569,1); --569 - dish_of_gusgen_clay
		player:messageSpecial(ITEM_OBTAINED,569); -- dish_of_gusgen_clay
	else
		player:messageSpecial(ITEM_CANNOT_BE_OBTAINED,569);
	end
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
--printf("CSID2: %u",csid);
--printf("RESULT2: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
end;