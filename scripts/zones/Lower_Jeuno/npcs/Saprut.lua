-----------------------------------
-- Area: Lower Jeuno
-- NPC: Saprut
-- Type: Quest NPC
-- Quests: Lure of the Wildcat
-- (Jeuno)
-- @zone 245
-- @pos 2.257 -5.999 -16.434
-----------------------------------
package.loaded["scripts/zones/Lower_Jeuno/TextIDs"] = nil;
-----------------------------------

require("scripts/zones/Lower_Jeuno/TextIDs");
require("scripts/globals/quests");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end; 

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	local WildcatJeuno = player:getVar("WildcatJeuno");

	if (player:getQuestStatus(JEUNO,LURE_OF_THE_WILDCAT_JEUNO) == QUEST_ACCEPTED and player:getMaskBit(WildcatJeuno,11) == false) then
		player:startEvent(10054);
	else
		player:startEvent(0x00E0);
	end
end; 

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
	if (csid == 10054) then
		player:setMaskBit(player:getVar("WildcatJeuno"),"WildcatJeuno",11,true);
	end
end;
