-----------------------------------
-- Area: Lower Jeuno
-- NPC: Parike-Poranke
-- Type: Adventurer's Assistant
-- @zone 245
-- @pos -33.161 -1 -61.303
-----------------------------------
package.loaded["scripts/zones/Lower_Jeuno/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/titles");
require("scripts/zones/Lower_Jeuno/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
	local count = trade:getItemCount();
	local GysahlGreens = trade:hasItemQty(4545,1);
	local digestiveMagicSkill = GetServerVariable("DigestiveMagic");
	local scienceSkill = GetServerVariable("ScienceSkill");
	if(player:hasStatusEffect(EFFECT_FOOD) == true) then
		if(GysahlGreens == true and count == 1) then
			player:tradeComplete();
			player:showText(npc,PARIKE_PORANKE_3);
			player:showText(npc,PARIKE_PORANKE_4);
			player:showText(npc,PARIKE_PORANKE_5);
			player:delStatusEffect(EFFECT_FOOD);
			SetServerVariable("DigestiveMagic",(digestiveMagicSkill + 1));
			if(digestiveMagicSkill == 1000 or digestiveMagicSkill == 2000 or digestiveMagicSkill == 3000
			or digestiveMagicSkill == 4000 or digestiveMagicSkill == 5000 or digestiveMagicSkill == 6000
			or digestiveMagicSkill == 7000 or digestiveMagicSkill == 8000 or digestiveMagicSkill == 9000) then
				player:showText(npc,PARIKE_PORANKE_6);
				player:showText(npc,PARIKE_PORANKE_8);
			elseif(digestiveMagicSkill == 10000) then
				player:showText(npc,PARIKE_PORANKE_6);
				player:showText(npc,PARIKE_PORANKE_7);
				player:showText(npc,PARIKE_PORANKE_9);
				player:addTitle(BROWN_MAGE_GUINEA_PIG);
			end
		else
			player:showText(npc,PARIKE_PORANKE_2);
		end
	elseif(player:hasStatusEffect(EFFECT_FIELD_SUPPORT_FOOD) == true) then
		if(GysahlGreens == true and count == 1) then
			player:tradeComplete();
			player:showText(npc,PARIKE_PORANKE_3);
			player:showText(npc,PARIKE_PORANKE_4);
			player:showText(npc,PARIKE_PORANKE_5);
			player:delStatusEffect(EFFECT_FIELD_SUPPORT_FOOD);
			SetServerVariable("DigestiveMagic",(digestiveMagicSkill + 1));
			if(digestiveMagicSkill == 1000 or digestiveMagicSkill == 2000 or digestiveMagicSkill == 3000
			or digestiveMagicSkill == 4000 or digestiveMagicSkill == 5000 or digestiveMagicSkill == 6000
			or digestiveMagicSkill == 7000 or digestiveMagicSkill == 8000 or digestiveMagicSkill == 9000) then
				player:showText(npc,PARIKE_PORANKE_6);
				player:showText(npc,PARIKE_PORANKE_8);
			elseif(digestiveMagicSkill == 10000) then
				player:showText(npc,PARIKE_PORANKE_6);
				player:showText(npc,PARIKE_PORANKE_7);
				player:showText(npc,PARIKE_PORANKE_9);
				player:addTitle(BROWN_MAGE_GUINEA_PIG);
			end
		else
			player:showText(npc,PARIKE_PORANKE_2);
		end
	else
		if(GysahlGreens == true and count == 1) then
			player:tradeComplete();
			SetServerVariable("ScienceSkill",(scienceSkill + 1));
			player:showText(npc,PARIKE_PORANKE_10);
			player:showText(npc,PARIKE_PORANKE_12);
			if(scienceSkill == 1000 or scienceSkill == 2000 or scienceSkill == 3000 or scienceSkill == 4000
			or scienceSkill == 5000 or scienceSkill == 6000 or scienceSkill == 7000 or scienceSkill == 8000
			or scienceSkill == 9000) then
				player:showText(npc,PARIKE_PORANKE_13);
				player:showText(npc,PARIKE_PORANKE_15);
			elseif(scienceSkill == 10000) then
				player:showText(npc,PARIKE_PORANKE_13);
				player:showText(npc,PARIKE_PORANKE_14);
				player:showText(npc,PARIKE_PORANKE_15);
				player:showText(npc,PARIKE_PORANKE_16);
				player:addTitle(BROWN_MAGIC_BYPRODUCT);
			end
		end
	end
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	player:messageSpecial(PARIKE_PORANKE_DIALOG);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;