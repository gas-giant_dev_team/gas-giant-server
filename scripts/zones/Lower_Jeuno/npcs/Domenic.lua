-----------------------------------
-- Area: Lower Jeuno
-- NPC:  Domenic
-- Type: Teleport NPC
-- @zone 245
-- @pos 32.776, -0.100, -15.281
-----------------------------------
package.loaded["scripts/zones/Lower_Jeuno/TextIDs"] = nil;
-----------------------------------

require("scripts/zones/Lower_Jeuno/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end; 

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	-- if(player:getQuestStatus(JEUNO,BEYOND_INFINITY) == QUEST_COMPLETED) then
		player:startEvent(0x2783,player:getGil());
	-- else
	--	player:startEvent(0x2784);
	-- end
end; 

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
	if(csid == 0x2783) then
		if(option == 1) then
			player:delGil(750);
			player:setPos(125,-19,180,128,140);
		elseif(option == 2) then
			player:delGil(750);
			player:setPos(-536,160,-210,17,139);
		elseif(option == 3) then
			player:delGil(750);
			player:setPos(-361,104,-259,0,144);
		elseif(option == 4) then
			player:delGil(750);
			player:setPos(317,-124,380,127,146);
		elseif(option == 5) then
			player:delGil(1000);
			player:setPos(-241,-24,19,0,206);
		end
	end
end;