-----------------------------------
-- Area: Beaucedine Glacier (111)
-- NM: Nue
-- @zone 111
-- @pos -313.911, -100.308, 147.196
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)	

	-- Set Nue's Window Open Time
	wait = math.random((3600),(7200));
	SetServerVariable("[POP]Nue", os.time(t) + (wait /NM_TIMER_MOD)); -- 1-2 hours
	DeterMob(mob:getID(), true);

	-- Set PH back to normal, then set to respawn spawn
	PH = GetServerVariable("[PH]Nue");
	SetServerVariable("[PH]Nue", 0);
	DeterMob(PH, false);
	GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

end;
