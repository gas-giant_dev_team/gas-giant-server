-----------------------------------
-- Area: Port Jeuno
-- NPC: Treasure Coffer
-- Type: Treasure NPC
-- @zone 246
-- @pos -52.000 0.000 -11.000
-----------------------------------
package.loaded["scripts/zones/Port_Jeuno/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/zones/Port_Jeuno/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	if (ENABLE_ABYSSEA == 1) then
		player:startEvent(0x015E, 65532);
	else
		player:messageSpecial(CHEST_IS_EMPTY);
	end
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
	if (csid == 0x015E and option == 2) then
		if (player:hasItem(277) or player:getFreeSlotsCount() == 0) then
			player:messageSpecial(ITEM_CANNOT_BE_OBTAINED,277);
		else
			player:addItem(277);
			player:messageSpecial(ITEM_OBTAINED,277);
		end
	end
end;