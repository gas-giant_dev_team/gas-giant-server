-----------------------------------
-- Area: Buburimu Peninsula (118)
-- Mob: Helldiver
-- @zone 118
-- @pos 475.000 -0.500 -240.000
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)

	-- Set Helldiver's Window Open Time
	local  wait = math.random((3600),(28800));
	SetServerVariable("[POP]Helldiver", os.time(t) + (wait /NM_TIMER_MOD)); -- 1-8 hours
	DeterMob(mob:getID(), true);

	-- Set PH back to normal, then set to respawn spawn
	local PH = GetServerVariable("[PH]Helldiver");
	SetServerVariable("[PH]Helldiver", 0);
	DeterMob(PH, false);
	GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

end;
