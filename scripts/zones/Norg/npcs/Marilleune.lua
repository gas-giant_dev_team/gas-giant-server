-----------------------------------
-- Area: Norg
-- NPC: Marilleune
-- Type: Chocobo Renter
-- @zone 252
-- @pos -11.309, -0.749, -51.162
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/keyitems");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
	local hasLicense = player:hasKeyItem(CHOCOBO_LICENSE);
	local level = player:getMainLvl();
	local count = trade:getItemCount();
	local ChocoboTicket = trade:hasItemQty(1514,1);
	local durationMod = 0;
	durationMod = player:getMod(MOD_CHOCOBO_TIME) * 60;

	if(ChocoboTicket == true and count == 1) then
		if (hasLicense and level >= 20) then
			player:tradeComplete();
			player:addStatusEffectEx(EFFECT_CHOCOBO,EFFECT_CHOCOBO,1,0,(1800 + durationMod),true);
			player:setPos(-456,17,-348,0,0x7b);
		end
	end
end;


-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	local price = 100;
	local gil = player:getGil();
	local hasLicense = player:hasKeyItem(CHOCOBO_LICENSE);
	local level = player:getMainLvl();

	if (hasLicense and level >= 15) then
		player:startEvent(0x0083,price,gil);
	else
		player:startEvent(0x0084,price,gil);
	end

end;

  
-----------------------------------
-- onEventFinish Action
-----------------------------------
function onEventFinish(player,csid,option)
--print("CSID:",csid);
--print("OPTION:",option);
	local durationMod = 0;
	durationMod = player:getMod(MOD_CHOCOBO_TIME) * 60;
	local price = 100;

	if (csid == 0x0083 and option == 0) then
		if (player:delGil(price)) then
			if (player:getMainLvl() >= 20) then
				player:addStatusEffectEx(EFFECT_CHOCOBO,EFFECT_CHOCOBO,1,0,(1800 + durationMod),true);
			else
				player:addStatusEffectEx(EFFECT_CHOCOBO,EFFECT_CHOCOBO,1,0,(900 + durationMod),true);
			end
			player:setPos(-456,17,-348,0,0x7b);
		end
	end
end;