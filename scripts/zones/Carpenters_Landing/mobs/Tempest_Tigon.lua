-----------------------------------
-- Area: Carpenter's Landing (2)
-- NM: Tempest Tigon
-- @zone 2
-- @pos -237.025 -4.946 -294.933
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)	

	-- Set Tempest Tigon's spawnpoint and respawn time (1-2 hours)
	UpdateNMSpawnPoint(mob:getID());
	mob:setRespawnTime((math.random((3600),(7200))) /NM_TIMER_MOD);

end;