-----------------------------------
--  Area: Windurst Walls
--  NPC:  Moan-Maon
--  Type: Standard NPC
-- Quests: Lure of the Wildcat
-- (Windurst)
-- @zone 239
-- @pos 88.244 -6.320 148.912
-----------------------------------
package.loaded["scripts/zones/Windurst_Walls/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/quests");
require("scripts/zones/Windurst_Walls/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	local WildcatWindurst = player:getVar("WildcatWindurst");

	if (player:getQuestStatus(WINDURST,LURE_OF_THE_WILDCAT_WINDURST) == QUEST_ACCEPTED and player:getMaskBit(WildcatWindurst,7) == false) then
		player:startEvent(0x01f1);
	else
		player:startEvent(0x0133);
	end

end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);

	if (csid == 0x01f1) then
		player:setMaskBit(player:getVar("WildcatWindurst"),"WildcatWindurst",7,true);
	end

end;

