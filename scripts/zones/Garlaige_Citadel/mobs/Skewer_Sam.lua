---------------------------------
-- Area: Garlaige Citadel (200)
-- NM: Skewer Sam
-- @zone 200
-- @pos -108.000 -0.500 177.000
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)

	-- Set Skewer_Sam's spawnpoint and respawn time (21-24 hours)
	UpdateNMSpawnPoint(mob:getID());
	mob:setRespawnTime((math.random((75600),(86400))) /LongNM_TIMER_MOD);

end;