-----------------------------------
-- Area: Riverne - Site #B01
--  NM:  Imdugud
-- @zone 29
-- @pos 655.263 20.664 651.320
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)

    -- Set Imduguds ToD
    SetServerVariable("[POP]Imdugud", os.time(t) + (75600 /LongNM_TIMER_MOD)); -- 21 hour
    DeterMob(mob:getID(), true);

    -- Set PH back to normal, then set to respawn spawn
    local PH = GetServerVariable("[PH]Imdugud");
    SetServerVariable("[PH]Imdugud", 0);
    DeterMob(PH, false);
    GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));
end;