-----------------------------------
-- Area: Mhaura
-- NPC: Fyi Chalmwoh
-- Type: Quest NPC
-- Quests: It's Raining Mannequins!,
-- Behind the Smile, Knocking on
-- Forbidden Doors 
-- @zone 249
-- @pos 24.000, -15.000, 58.000
-----------------------------------
package.loaded["scripts/zones/Mhaura/TextIDs"] = nil;
-----------------------------------

require("scripts/zones/Mhaura/TextIDs");
require("scripts/globals/quests");
require("scripts/globals/keyitems");
require("scripts/globals/settings");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
	local count = trade:getItemCount();
	local Head = trade:hasItemQty(1601,1);
	local Body = trade:hasItemQty(1602,1);
	local Hands = trade:hasItemQty(1603,1);
	local Legs = trade:hasItemQty(1604,1);
	local Feet = trade:hasItemQty(1605,1);
	local HumeM = trade:hasItemQty(256,1);
	local HumeF = trade:hasItemQty(257,1);
	local ElvaanM = trade:hasItemQty(258,1);
	local ElvaanF = trade:hasItemQty(259,1);
	local TaruM = trade:hasItemQty(260,1);
	local TaruF = trade:hasItemQty(261,1);
	local Mithra = trade:hasItemQty(262,1);
	local Galka = trade:hasItemQty(263,1);

	if(player:getQuestStatus(OTHER_AREAS,IT_S_RAINING_MANNEQUINS) == QUEST_ACCEPTED and
	player:getVar("Raining_M_Progress") == 4) then
		if(Head == true and Body == true and Hands == true and Legs == true and Feet == true and count == 5) then
			player:tradeComplete();
			player:startEvent(0x0135); -- found all parts, might take awhile
		end
	elseif(player:getQuestStatus(OTHER_AREAS,IT_S_RAINING_MANNEQUINS) == QUEST_COMPLETED) then
		if((HumeM == true or HumeF == true or ElvaanM == true or ElvaanF == true or TaruM == true
		or TaruF == true or Mithra == true or Galka == true) and count == 1) then
			player:tradeComplete();
			local stock = {
				256,2000,		-- Male Hume Mannequin
				257,2000,		-- Female Hume Mannequin
				258,2000,		-- Elvaan Male Mannequin
				259,2000,		-- Elvaan Female Mannequin
				260,2000,		-- Tarutaru Male Mannequin
				261,2000,		-- Tarutaru Female Mannequin
				262,2000,		-- Mithra Mannequin
				263,2000}		-- Galka Mannequin
			showShop(player, STATIC, stock);
		end
	end
			
end; 

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	if(player:getQuestStatus(OTHER_AREAS,IT_S_RAINING_MANNEQUINS) == QUEST_COMPLETED) then
		player:startEvent(0x0138); -- how's the mannequin
	elseif(player:getVar("Raining_M_Progress") == 1) then
		player:startEvent(0x0132); -- goto selbina
	elseif(player:getVar("Raining_M_Progress") == 3) then
		player:startEvent(0x0133); -- goto carpenters guild
	elseif(player:getVar("Raining_M_Progress") == 4) then
		player:startEvent(0x0134); -- diagrams
	elseif(player:getVar("Raining_M_Progress") == 5 and player:getVar("Mannequin_Timer")<= os.time()) then
		player:startEvent(0x0137); -- I've done it
	elseif(player:getVar("Raining_M_Progress") == 5 and player:getVar("Mannequin_Timer")> os.time()) then
		player:startEvent(0x0136); -- might take awhile
	else
		player:startEvent(0x0131); -- give quest
	end
end; 

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
	if(csid == 0x0131) then
		player:addQuest(OTHER_AREAS,IT_S_RAINING_MANNEQUINS);
		player:setVar("Raining_M_Progress",1);
	elseif(csid == 0x0132) then
		player:setVar("Raining_M_Progress",2);
	elseif(csid == 0x0135) then
		player:setVar("Raining_M_Progress",5);
		player:setVar("Mannequin_Timer",os.time() + 3456);
	elseif(csid == 0x0137) then
		player:setVar("Raining_M_Progress",0);
		player:setVar("Mannequin_Timer",0);
		local race = player:getRace();
		local body = 0;
		switch(race) : caseof
		{
			-- HUME MALE
		 	[1]	= function (x)
			 	body = 256;
		  	end,

			-- HUME FEMALE
		  	[2]	= function (x)
				body = 257;
		  	end,

			-- ELVAAN MALE
		  	[3]	= function (x)
			  	body = 258;
		  	end,

			-- ELVAAN FEMALE
		  	[4]	= function (x)
				body = 259;
		  	end,

			-- TARU MALE
		  	[5]	= function (x)
				body = 260;
		  	end,

			-- TARU FEMALE
		  	[6]	= function (x)
				body = 261;
		  	end,

			-- MITHRA
		  	[7]	= function (x)
				body = 262;
		  	end,

			-- GALKA
		  	[8]	= function (x)
				body = 263;
		  	end,

		  	default = function (x) end,
		}
		player:addItem(body); -- mannequin relating to your race
		player:messageSpecial(ITEM_OBTAINED,body);
		player:completeQuest(OTHER_AREAS,IT_S_RAINING_MANNEQUINS);
		player:addFame(OTHER_AREAS,50);
	elseif(csid == 0x0138) then
		local stock = {
			256,100000,		-- Male Hume Mannequin
			257,100000,		-- Female Hume Mannequin
			258,100000,		-- Elvaan Male Mannequin
			259,100000,		-- Elvaan Female Mannequin
			260,100000,		-- Tarutaru Male Mannequin
			261,100000,		-- Tarutaru Female Mannequin
			262,100000,		-- Mithra Mannequin
			263,100000}		-- Galka Mannequin
		showShop(player, STATIC, stock);
	end
end;