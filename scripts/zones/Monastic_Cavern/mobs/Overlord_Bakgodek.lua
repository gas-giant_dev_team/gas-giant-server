-----------------------------------
-- Area: Monastic Cavern
-- NM:  Overlord Bakgodek
-- @zone 150
-- @pos 220.000, -2.000, -108.000
-----------------------------------

require("scripts/globals/titles");
require("scripts/zones/Monastic_Cavern/TextIDs");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobEngaged
-----------------------------------

function OnMobEngaged(mob,target)
	-- mob:messagePublic(mob,ORC_KING_ENGAGE);
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)
	killer:addTitle(OVERLORD_OVERTHROWER);
	-- mob:messagePublic(mob,ORC_KING_DEATH);
end;