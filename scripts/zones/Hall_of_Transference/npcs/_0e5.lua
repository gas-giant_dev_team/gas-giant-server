-----------------------------------
-- Area: Hall of Tansference - Dem
-- Large Apparatus (Left) - Dem
-- @zone 14
-- @pos -236.300 -44.900 -270.000
-- PH PM PD 
-- 1=available 2=working
-- 3=complete
-----------------------------------
package.loaded["scripts/zones/Hall_of_Transference/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/missions");
require("scripts/globals/settings");
require("scripts/globals/titles");
require("scripts/globals/keyitems");
require("/scripts/globals/teleports");
require("scripts/zones/Hall_of_Transference/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	
	if(player:getVar("PromathiaStatus") == 2)then -- Initial Entrance cutscene
		player:startEvent(0x00A0);
	elseif(player:getVar("PD") == 2)then -- Erase Memory exit Hall to Crag
		player:startEvent(0x007D);
	elseif(player:getVar("TuLiaRegistration") == 1)then
		player:messageSpecial(REGISTERED_DATA);
	else
		player:messageSpecial(NODATA);
		player:messageSpecial(REGISTER_DATA);
	end
	return 1;
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);

	if(csid == 0x00A0)then
		if(player:getCurrentMission(COP) > BELOW_THE_ARKS) then
			player:setPos(152,0 ,-70 ,81 ,18 );
		else
			player:setVar("PD",2);
			player:setPos(152,0 ,-70 ,81 ,18 );
		end
	elseif(csid == 0x007D and option == 0)then
		player:setVar("PD",1);
		player:setPos(136,19,220,130,108);
	end
end;