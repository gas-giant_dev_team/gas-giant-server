-----------------------------------
-- Area: Port San d'Oria
-- NPC: Nazar
-- Type: Standard Info NPC
-- @zone 232
-- @pos -31.922 -3.000 44.240
-----------------------------------
package.loaded["scripts/zones/Port_San_dOria/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
	if (TRAVEL_SKIP >= 1) then
		if (trade:getGil() >= TRAVEL_SKIP and trade:getItemCount() == 1) then
			player:delGil(TRAVEL_SKIP);
			player:setPos(-90,12,120,64,246);
		end
	end
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	player:startEvent(0x02bf);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;
