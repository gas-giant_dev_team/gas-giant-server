-----------------------------------
-- Area: Beadeaux
-- NPC: Copper Quadav
-- Missions: 3-1 (Bastok)
-- @zone 147
-- @pos many
-----------------------------------
package.loaded["scripts/zones/Beadeaux/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/missions");
require("scripts/zones/Beadeaux/TextIDs");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath Action
-----------------------------------

function onMobDeath(mob, killer)

	if(killer:getCurrentMission(BASTOK) == THE_FOUR_MUSKETEERS) then
		local missionStatus = killer:getVar("MissionStatus");

		if(missionStatus < 22) then
			killer:setVar("MissionStatus", missionStatus + 1)
		end
	end

end;