-----------------------------------
-- Area: Aht Urhgan Whitegate
-- NPC: Sajaaya
-- Type: Weather Checker
-- @zone 50
-- @pos -4.727 -6.999 -25.312
-----------------------------------
package.loaded["scripts/zones/Aht_Urhgan_Whitegate/TextIDs"] = nil;
-----------------------------------

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	local birth = 1009810800;
	local timer = os.time();
	local counter = (timer - birth);
	player:startEvent(0x01f6,0,0,0,0,0,0,0,counter);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;