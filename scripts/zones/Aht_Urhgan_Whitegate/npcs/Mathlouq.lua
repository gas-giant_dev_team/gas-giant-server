-----------------------------------
-- Area: Aht Urhgan Whitegate
-- NPC: Mathlouq
-- Type: Quest NPC
-- Quests: Arts and Crafts
-- @zone 50
-- @pos -92.892 -7.00 129.277
-----------------------------------
package.loaded["scripts/zones/Aht_Urhgan_Whitegate/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/quests");
require("scripts/zones/Aht_Urhgan_Whitegate/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	local artsAndCrafts = player:getQuestStatus(AHT_URHGAN,ARTS_AND_CRAFTS);
	local artsAndCrafts_Mathloug = player:getVar("QUEST_ARTSANDCRAFTS_MATHLOUQ");

	if (artsAndCrafts == 1 and artsAndCrafts_Mathloug ~= 1) then
		player:startEvent(0x01FF);
	else
		player:startEvent(0x021F);
	end
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
	if (csid == 0x01FF) then
		player:setVar("QUEST_ARTSANDCRAFTS_MATHLOUQ",1);
	end
end;