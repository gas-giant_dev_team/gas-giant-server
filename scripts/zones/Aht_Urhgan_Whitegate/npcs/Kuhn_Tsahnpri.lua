-----------------------------------
-- Area: Aht Urhgan Whitegate
-- NPC: Kuhn Tsahnpri
-- Type: Standard Info NPC
-- @zone 50
-- @pos 12.082 1.999 143.392
-----------------------------------
package.loaded["scripts/zones/Aht_Urhgan_Whitegate/TextIDs"] = nil;
-----------------------------------

require("scripts/zones/Aht_Urhgan_Whitegate/TextIDs");
require("scripts/globals/settings");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
	if (TRAVEL_SKIP >= 1) then
		if (trade:getGil() >= TRAVEL_SKIP and trade:getItemCount() == 1) then
			player:delGil(TRAVEL_SKIP);
			player:setPos(12,2,-100,192,53);
		end
	end
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	local timer = 1152 - ((os.time() - 1009810800)%1152);
	local direction = 0; -- Arrive, 1 for depart
	local waiting = 431; -- Offset for Nashmau

	if (timer <= waiting) then
		direction = 1; -- Ship arrived, switch dialog from "arrive" to "depart"
	else
		timer = timer - waiting; -- Ship hasn't arrived, subtract waiting time to get time to arrival
	end

	player:startEvent(236,timer,direction);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	--printf("CSID: %u",csid);
	--printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	--printf("CSID: %u",csid);
	--printf("RESULT: %u",option);
end;