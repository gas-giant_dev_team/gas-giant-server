-----------------------------------
-- Area: Windurst Waters
-- Door: Acolyte Hostel 8
-- Type: Object NPC
-- @zone 238
-- @pos 174.987, -2.601, 238.860
-----------------------------------
package.loaded["scripts/zones/Windurst_Waters/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/quests");
require("scripts/globals/settings");
require("scripts/zones/Windurst_Waters/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	if(player:getQuestStatus(WINDURST,NOTHING_MATTERS) == QUEST_ACCEPTED and player:getVar("NothingMatters_Door_8") == 0
	and player:getVar("NothingMatters_Progress") == 2) then
		player:startEvent(0x032B);
	else
		player:showText(player,NOTHING_OUT_OF_ORDINARY);
	end
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
	if (csid == 0x032B and option == 100) then
		player:setVar("NothingMatters_Qs",(player:getVar("NothingMatters_Qs") + 1));
		if (player:getVar("NothingMatters_Qs") == 6 and player:getVar("NothingMatters_Q_Fail") == 0) then
			player:addItem(4174);
			player:messageSpecial(ITEM_OBTAINED,4174);
		end
		player:setVar("NothingMatters_Door_8",1);
	elseif (csid == 0x032B and option == 0) then
		player:setVar("NothingMatters_Q_Fail",1);
	end
end;