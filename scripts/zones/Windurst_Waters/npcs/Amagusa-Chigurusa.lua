-----------------------------------
-- Area: Windurst Waters
-- NPC: Amagusa-Chigurusa
-- Type: Quest NPC
-- Quests: Lure of the Wildcat
-- (Windurst)
-- @zone 238
-- @pos -28.746 -4.500 61.954
-----------------------------------
package.loaded["scripts/zones/Windurst_Waters/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/quests");
require("scripts/zones/Windurst_Waters/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	local WildcatWindurst = player:getVar("WildcatWindurst");

	if (player:getQuestStatus(WINDURST,LURE_OF_THE_WILDCAT_WINDURST) == QUEST_ACCEPTED and player:getMaskBit(WildcatWindurst,12) == false) then
		player:startEvent(0x03a9);
	else
		player:startEvent(0x0232);
	end
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);

	if (csid == 0x03a9) then
		player:setMaskBit(player:getVar("WildcatWindurst"),"WildcatWindurst",12,true);
	end	

end;

