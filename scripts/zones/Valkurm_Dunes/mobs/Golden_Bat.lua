-----------------------------------
-- Area: Valkurm Dunes (103)
--  NM:  Golden_Bat
-- @zone 103
-- @pos -810.440, -8.270, 33.978
-----------------------------------

require("scripts/globals/settings");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)	

    -- Set Golden_Bat's Window Open Time
    wait = math.random((3600),(18000));
    SetServerVariable("[POP]Golden_Bat", os.time(t) + (wait /NM_TIMER_MOD)); -- 1-5 hours
    DeterMob(mob:getID(), true);

    -- Set PH back to normal, then set to respawn spawn
    PH = GetServerVariable("[PH]Golden_Bat");
    SetServerVariable("[PH]Golden_Bat", 0);
    DeterMob(PH, false);
    GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

end;

