-----------------------------------
-- Area: Western Adoulin
-- NPC: Andrival
-- Type: Patrol NPC
-- @zone 256
-- @pos 26.000 0.001 127.700
-----------------------------------
package.loaded["scripts/zones/Western_Adoulin/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/zones/Western_Adoulin/TextIDs");
require("scripts/globals/pathfind");

-----------------------------------

local path = {
     25.999987,0.000000,122.318810,
     25.999990,0.000000,123.418785,
     25.999992,0.000000,153.419571,
     25.999994,0.000000,159.363541,
     25.999987,0.000000,129.343399
};

-----------------------------------
-- onSpawn Action
-----------------------------------

function onSpawn(npc)
	npc:initNpcAi();
	npc:setPos(pathfind.first(path));
	onPath(npc);
end;

-----------------------------------
-- onPath Action
-----------------------------------

function onPath(npc)
	pathfind.patrol(npc, path);
end;


-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	player:startEvent(0x0228);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;