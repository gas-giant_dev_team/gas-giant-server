-----------------------------------
-- Area: Dho Gates
-- NPC: Knotted Root
-- @zone 272
-- @pos many
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/missions");
require("scripts/globals/titles");
require("scripts/globals/status");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function onMobSpawn(mob)
end;


-----------------------------------
-- onMobEngaged
-----------------------------------
function onMobEngaged(mob,target)
end;

-----------------------------------
-- onMobDeath Action
-----------------------------------

function onMobDeath(mob,killer)

	local A = GetMobAction(17891484);
	local B = GetMobAction(17891485);
	local D = GetMobAction(17891492);
	local E = GetMobAction(17891493);
	local C = A+B;
	local F = D+E;

	if(C >= 42 and C <= 50)then
		-- GetNPCByID(17891567):openDoor(3600);
		-- GetNPCByID(17891568):openDoor(3600);
		DespawnMob(17891486);
		DespawnMob(17891487);
		DespawnMob(17891488);
		DespawnMob(17891489);
		DespawnMob(17891490);
		DespawnMob(17891491);
	elseif(F >= 42 and F <= 50)then
		GetNPCByID(17891569):openDoor(3600);
		GetNPCByID(17891570):openDoor(3600);
		DespawnMob(17891494);
		DespawnMob(17891495);
		DespawnMob(17891496);
		DespawnMob(17891497);
		DespawnMob(17891498);
		DespawnMob(17891499);
	end
end;