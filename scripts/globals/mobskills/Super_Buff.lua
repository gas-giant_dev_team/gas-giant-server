---------------------------------------------------
-- Mobskill: Super Buff
--
-- Increases the mobs ATTK%, DEF%, MATT and MEVA
---------------------------------------------------

require("/scripts/globals/settings");
require("/scripts/globals/status");
require("/scripts/globals/monstertpmoves");

---------------------------------------------------

function OnMobSkillCheck(target,mob,skill)
	return 1;
end;

function OnMobWeaponSkill(target, mob, skill)
	target:addStatusEffectEx(EFFECT_SUPER_BUFF, 0, 50, 0, 30);
	skill:setMsg(0);
	return 0;
end;