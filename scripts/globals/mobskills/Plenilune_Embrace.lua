---------------------------------------------
--  Mobskill: Plenilune Embrace
--
--  Restores target party member's HP and
--  enhances attack and magic attack.
---------------------------------------------

require("/scripts/globals/settings");
require("/scripts/globals/status");
require("/scripts/globals/monstertpmoves");

---------------------------------------------
function OnMobSkillCheck(target,mob,skill)
	return 0;
end;

function OnMobWeaponSkill(target, mob, skill)

	local potency = skill:getParam();

	if(potency == 0) then
		potency = 9;
	end

	potency = potency - math.random(0, potency/4);

	skill:setMsg(MSG_SELF_HEAL);
	
	MobPhysicalStatusEffectMove(mob, target, skill, EFFECT_ATTACK_BOOST, 25, 0, 300);
	MobPhysicalStatusEffectMove(mob, target, skill, EFFECT_MAGIC_ATK_BOOST, 25, 0, 300);
	
	return MobHealMove(mob, mob:getMaxHP() * potency / 100);
end;