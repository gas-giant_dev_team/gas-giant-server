-----------------------------------
--	EFFECT_DOUBLE_SHOT
-- Occasionally uses two units of
-- ammunition to deal double damage. 
-----------------------------------

-----------------------------------
-- onEffectGain Action
-----------------------------------

function onEffectGain(target,effect)
	target:addMod(MOD_DOUBLE_SHOT_RATE, effect:getPower());
end;

-----------------------------------
-- onEffectTick Action
-----------------------------------

function onEffectTick(target,effect)
end;

-----------------------------------
-- onEffectLose Action
-----------------------------------

function onEffectLose(target,effect)
	target:delMod(MOD_DOUBLE_SHOT_RATE, effect:getPower());
end;