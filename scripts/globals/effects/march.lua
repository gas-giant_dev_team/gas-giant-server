-----------------------------------
--	EFFECT_MARCH
-- It gives an effect similar to
-- Haste to party members within
-- range, reducing their attack
-- delay and magic recast timers.
-----------------------------------

require("scripts/globals/status");

-----------------------------------
-- onEffectGain Action
-----------------------------------

function onEffectGain(target,effect)
	target:addMod(MOD_HASTE_MAGIC, effect:getPower());
end;

-----------------------------------
-- onEffectTick Action
-----------------------------------

function onEffectTick(target,effect)
end;

-----------------------------------
-- onEffectLose Action
-----------------------------------

function onEffectLose(target,effect)
	target:delMod(MOD_HASTE_MAGIC, effect:getPower());
end;