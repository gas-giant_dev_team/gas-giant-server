-----------------------------------
--
--	EFFECT_MAGIC_SHIELD
-- Magic Shield BLOCKS all magic
-- attacks.
-----------------------------------

require("scripts/globals/status");

-----------------------------------
-- onEffectGain Action
-----------------------------------

function onEffectGain(target,effect)
	target:addMod(MOD_UDMGMAGIC, -256);
	target:addMod(MOD_SPELLINTERRUPT, effect:getSubPower());
end;

-----------------------------------
-- onEffectTick Action
-----------------------------------

function onEffectTick(target,effect)
end;

-----------------------------------
-- onEffectLose Action
-----------------------------------

function onEffectLose(target,effect)
	target:delMod(MOD_UDMGMAGIC, -256);
	target:delMod(MOD_SPELLINTERRUPT, effect:getSubPower());
end;