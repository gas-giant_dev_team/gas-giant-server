-----------------------------------
-- EFFECT_SIRVENTE
-- Reduces target party member's
-- enmity loss. 
-----------------------------------

-----------------------------------
-- onEffectGain Action
-----------------------------------

function onEffectGain(target,effect)
	target:addMod(MOD_ENMITY,-effect:getPower());
end;

-----------------------------------
-- onEffectTick Action
-----------------------------------

function onEffectTick(target,effect)
	-- the effect gains enmity of 1 every 3 ticks depending on the source of the enmity boost
	if(effect:getPower() > 0) then
		effect:setPower(effect:getPower()-1)
		target:delMod(MOD_ENMITY,-1);
	end
end;

-----------------------------------
-- onEffectLose Action
-----------------------------------

function onEffectLose(target,effect)
	if(effect:getPower() > 0) then
		target:delMod(MOD_ENMITY,-effect:getPower());
	end
end;