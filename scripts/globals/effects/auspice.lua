-----------------------------------
--	EFFECT_AUSPICE
-- Reduces TP dealt when striking
-- an enemy and bestows an accuracy
-- bonus when the target is missed
-- for party members within area of
-- effect.
-----------------------------------

-----------------------------------
-- onEffectGain Action
-----------------------------------

function onEffectGain(target,effect)
	target:addMod(MOD_SUBTLE_BLOW,effect:getPower());
end;

-----------------------------------
-- onEffectTick Action
-----------------------------------

function onEffectTick(target,effect)
end;

-----------------------------------
-- onEffectLose Action
-----------------------------------

function onEffectLose(target,effect)
	target:delMod(MOD_SUBTLE_BLOW,effect:getPower());
end;