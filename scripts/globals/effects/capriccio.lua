-----------------------------------
-- EFFECT_CAPRICCIO
-- Increases resistance to petrification
-- for party members within the area of
-- effect.
-----------------------------------

-----------------------------------
-- onEffectGain Action
-----------------------------------

function onEffectGain(target,effect)
	target:addMod(MOD_PETRIFYRES,effect:getPower());
end;

-----------------------------------
-- onEffectTick Action
-----------------------------------

function onEffectTick(target,effect)
end;

-----------------------------------
-- onEffectLose Action
-----------------------------------

function onEffectLose(target,effect)
	target:delMod(MOD_PETRIFYRES,effect:getPower());
end;