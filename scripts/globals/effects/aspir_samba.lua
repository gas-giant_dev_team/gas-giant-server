-----------------------------------
--	EFFECT_ASPIR_SAMBA
-- Inflicts the next target you
-- strike with Aspir daze, allowing
-- all those engaged in battle with
-- it to drain its MP. 
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/status");
require("scripts/globals/magic");

-----------------------------------
-- onEffectGain Action
-----------------------------------

function onEffectGain(target,effect)
end;

-----------------------------------
-- onEffectTick Action
-----------------------------------

function onEffectTick(target,effect)
end;

-----------------------------------
-- onEffectLose Action
-----------------------------------

function onEffectLose(target,effect)
end;