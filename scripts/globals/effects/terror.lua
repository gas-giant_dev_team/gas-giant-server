-----------------------------------
--	EFFECT_TERROR
-- Terror is a harmful status
-- effect that makes the player
-- immobile with fear until it
-- wears off. Afflicted characters
-- cannot move, attack, cast
-- spells, or use abilities. It
-- also freezes the player's
-- character model.
-- Nothing can remove this effect
-- except waiting for it to wear
-- off (generally a short period of
-- time). 
-----------------------------------

-----------------------------------
-- onEffectGain Action
-----------------------------------

function onEffectGain(target,effect)
end;

-----------------------------------
-- onEffectTick Action
-----------------------------------

function onEffectTick(target,effect)
end;

-----------------------------------
-- onEffectLose Action
-----------------------------------

function onEffectLose(target,effect)
end;