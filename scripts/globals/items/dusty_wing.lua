-----------------------------------------
-- ID: 5440
-- Item: Dusty Wing
-- Effect: Increases TP of the user by 300
-----------------------------------------

require("scripts/globals/status");

-----------------------------------------
-- OnItemCheck
-----------------------------------------

function onItemCheck(target)
    return 0;
end;

-----------------------------------------
-- OnItemUse
-----------------------------------------

function onItemUse(target)
    target:addTP(300);
end;