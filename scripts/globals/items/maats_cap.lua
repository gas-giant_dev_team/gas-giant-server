-----------------------------------------
-- ID: 15194
-- Item: Maat's Cap
-- Enchantment: "Teleport" (Ru'Lude Gardens)
-----------------------------------------

require("scripts/globals/teleports");
require("scripts/globals/settings");
require("scripts/globals/status");

-----------------------------------------
-- OnItemCheck
-----------------------------------------

function onItemCheck(target)
	return true;
end;

-----------------------------------------
-- OnItemUse
-----------------------------------------

function onItemUse(target)
	target:addStatusEffectEx(EFFECT_TELEPORT,0,TELEPORT_MAAT,0,1);
end;
