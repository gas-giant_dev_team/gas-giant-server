-- -------------------------------------------------------------------
--																	--
--			== Utils.lua ==											--
--																	--
--  Contains functions for damage, resistance, etc...				--
--																	--
-- -------------------------------------------------------------------

utils = {};

-- -------------------------------------------------------------------
--																	--
--		== Clamp ==													--
--	Caps minimum and maximum values.								--
--																	--
-- -------------------------------------------------------------------

function utils.clamp(input, min_val, max_val)
    if input < min_val then
        input = min_val;
    elseif input > max_val then
        input = max_val;
    end
    return input;
end;

-- -------------------------------------------------------------------
--																	--
--		== Manawall ==												--
--	Calculates reduction from the Manawall ability.					--
--																	--
-- -------------------------------------------------------------------

function utils.manawall(target, dmg)
	local wall = target:getMP();
    if(wall > 0) then
        if(wall > ((dmg / 2) - (dmg * (target:getMod(MOD_MANA_WALL_DMG) / 100)))) then -- Absorb full half damage
            target:delMP(dmg / 2);
            return 0;
        else -- Absorbs some damage based on MP
            target:delMP(wall);
			target:delStatusEffect(EFFECT_MANA_WALL);
            return (((dmg / 2) - (dmg * (target:getMod(MOD_MANA_WALL_DMG) / 100))) - wall);
        end
		updateEnmity(target,-180,0);
    end

    return dmg;
end;

-- -------------------------------------------------------------------
--																	--
--		== Sacrosanctity ==											--
--	Reduces magic damage taken by a further 75% in a separate step	--
--	after other forms of reduction are applied. 					--
--																	--
-- -------------------------------------------------------------------

function utils.sacrosanctityDmg(target, dmg)

    dmg = dmg * .25;

    return dmg;
end;

-- -------------------------------------------------------------------
--																	--
--		== Scarlet Delirium ==										--
--	Channels damage taken into enhanced attack and magic attack. 	--
--																	--
-- -------------------------------------------------------------------

function utils.scarletDelirium(target, dmg)
	if (dmg > 0) then
		scarletDeliriumBonus = ((dmg / target:getMaxHP()) / 2) * 100;
		target:delStatusEffect(EFFECT_SCARLET_DELIRIUM_I);
		target:addStatusEffect(EFFECT_SCARLET_DELIRIUM_II,scarletDeliriumBonus,0,60);
	end

    return;
end;

-- -------------------------------------------------------------------
--																	--
--		== Stoneskin ==												--
--	Absorbs a certain amount of damage from physical and magical	--
--	attacks. 														--
--																	--
-- -------------------------------------------------------------------

function utils.stoneskin(target, dmg)

    if (dmg > 0) then
        skin = target:getMod(MOD_STONESKIN);
        if(skin > 0) then
            if(skin > dmg) then -- Absorb all damage
                target:delMod(MOD_STONESKIN,dmg);
                return 0;
            else -- Absorbs some damage then wear
                target:delStatusEffect(EFFECT_STONESKIN);
                target:setMod(MOD_STONESKIN, 0);
                return dmg - skin;
            end
        end
    end

    return dmg;
end;

-- -------------------------------------------------------------------
--																	--
--		== Take Shadows ==											--
--	Determine how many shadows are taken from an attack. 			--
--																	--
-- -------------------------------------------------------------------

function utils.takeShadows(target, dmg, shadowbehav)
    if(shadowbehav == nil) then
        shadowbehav = 1;
    end

    local targShadows = target:getMod(MOD_UTSUSEMI);
    local shadowType = MOD_UTSUSEMI;

    if(targShadows == 0) then --try blink, as utsusemi always overwrites blink this is okay
        targShadows = target:getMod(MOD_BLINK);
        shadowType = MOD_BLINK;
    end

    if(targShadows > 0) then
    -- Blink has a VERY high chance of blocking tp moves, so im assuming its 100% because its easier!

        if(targShadows >= shadowbehav) then --no damage, just suck the shadows

            local shadowsLeft = targShadows - shadowbehav;

            target:setMod(shadowType, shadowsLeft);

            if(shadowsLeft > 0 and shadowType == MOD_UTSUSEMI) then --update icon
                effect = target:getStatusEffect(EFFECT_COPY_IMAGE);
                if(effect ~= nil) then
                    if(shadowsLeft == 1) then
                        effect:setIcon(EFFECT_COPY_IMAGE);
                    elseif(shadowsLeft == 2) then
                        effect:setIcon(EFFECT_COPY_IMAGE_2);
                    elseif(shadowsLeft == 3) then
                        effect:setIcon(EFFECT_COPY_IMAGE_3);
                    end
                end
            end
            -- Remove icon
            if(shadowsLeft <= 0) then
                target:delStatusEffect(EFFECT_COPY_IMAGE);
                target:delStatusEffect(EFFECT_BLINK);
            end

            return 0;
        else -- Less shadows than this move will take, remove all and factor damage down
            target:delStatusEffect(EFFECT_COPY_IMAGE);
            target:delStatusEffect(EFFECT_BLINK);
            return dmg * ((shadowbehav-targShadows)/shadowbehav);
        end
    end

    return dmg;
end;

-- -------------------------------------------------------------------
--																	--
--		== Third Eye ==												--
--	Returns true if taken by third eye, third eye doesnt care how	--
--	many shadows, so attempt to anticipate, but reduce chance of	--
--	anticipate based on previous successful anticipates. 			--
--																	--
-- -------------------------------------------------------------------

function utils.thirdeye(target)

    local teye = target:getStatusEffect(EFFECT_THIRD_EYE);

    if(teye == nil) then
        return false;
    end

    local prevAnt = teye:getPower();

    if( prevAnt == 0 or (math.random()*100) < (80-(prevAnt*10)) ) then
        -- Anticipated!
        target:delStatusEffect(EFFECT_THIRD_EYE);
        return true;
    end

    return false;
end

-- -------------------------------------------------------------------
--																	--
--		== Damage Taken ==											--
--	Determine how much damage is taken from an attack. 				--
--																	--
-- -------------------------------------------------------------------

function utils.dmgTaken(target, dmg)
    local resist = 1 + (target:getMod(MOD_DMG) / 100);

    if(resist < 0.5) then
        resist = 0.5;
    end

	local new_dmg = dmg * resist;
	return utils.clamp(new_dmg, 0, 999999);
end;

-- -------------------------------------------------------------------
--																	--
--		== Absorb Magical Damage ==									--
--	Absorb magical damage from an attack to HP. 					--
--																	--
-- -------------------------------------------------------------------

function utils.absorbMagicDmg(target, dmg) -- Elemental Sachets
	target:addHP(dmg);
    if(dmg > 0) then
		target:messageBasic(24,0,dmg);
	end
	dmg = 0;
	return dmg;
end;

-- -------------------------------------------------------------------
--																	--
--		== Damage to MP ==											--
--	Convert a percentage of damage to MP. 							--
--																	--
-- -------------------------------------------------------------------

function utils.dmgToMP(target, dmg)

    if(target:getObjType() == TYPE_PC) then
		local etherealMP = 0;
		local percentMP = target:getMod(MOD_ABSORB_DMG_TO_MP) * .01;
		etherealMP = math.floor(dmg * percentMP);
		target:addMP(etherealMP);
    	if(etherealMP > 0) then
			target:messageBasic(25,0,etherealMP);
		end
		-- printf("MP Gained %u",etherealMP);
    end
	
end;

-- -------------------------------------------------------------------
--																	--
--		== Damage to TP ==											--
--	Convert a percentage of damage to TP. 							--
--																	--
-- -------------------------------------------------------------------

function utils.dmgToTP(target, dmg)

    if(target:getObjType() == TYPE_PC) then
		local dmgTP = 0;
		local percentTP = target:getMod(MOD_ABSORB_DMG_TO_TP) * .001;
		dmgTP = math.floor(dmg * percentTP);
		target:addTP(dmgTP);
		-- printf("TP Gained %u",dmgTP);
    end
	
end;

-- -------------------------------------------------------------------
--																	--
--		== Breath Damage Taken ==									--
--	Determine how much damage is taken from a breath attack. 		--
--																	--
-- -------------------------------------------------------------------

function utils.breathDmgTaken(target, breathDmg)

    local resist = 1 + (target:getMod(MOD_UDMGBREATH) / 100);

    breathDmg = breathDmg * resist;

    resist = 1 + (target:getMod(MOD_DMGBREATH) / 100);

    if(resist < 0.5) then
        resist = 0.5;
    end

    return breathDmg * resist;
end;

-- -------------------------------------------------------------------
--																	--
--		== Magic Damage Taken ==									--
--	Determine how much damage is taken from a magical attack. 		--
--																	--
-- -------------------------------------------------------------------

function utils.magicDmgTaken(target, magicDmg)

    -- MDT is stored in amount/256
    local resist = ((256 + target:getMod(MOD_UDMGMAGIC))/256);

    magicDmg = magicDmg * resist;

    resist = ((256 + target:getMod(MOD_DMGMAGIC))/256);

    if(resist < 0.5) then
        resist = 0.5;
    end
	
	if(target:getObjType() == TYPE_PC) then
		if(math.random(100) <= target:getMod(MOD_MAGIC_NULL)) then
			magicDmg = 0;
		end
	
		if((((magicDmg * resist) / target:getMaxHP()) * 100) > 50) then
			if(((ring1 == 11647) or (ring2 == 11647)) -- Archon Ring
			and (math.random(100) <= 5)) then
				magicDmg = 0;
			end
		end

		-- Sentinel's Scherzo
		if((((magicDmg * resist) / target:getMaxHP()) * 100) > 75) then
			magicDmg = magicDmg - (magicDmg * (target:getMod(MOD_SCHERZO_DMG) / 100));
		end
	end

    return magicDmg * resist;
end;

-- -------------------------------------------------------------------
--																	--
--		== Physical Damage Taken ==									--
--	Determine how much damage is taken from a physical attack. 		--
--																	--
-- -------------------------------------------------------------------

function utils.physicalDmgTaken(target, dmg)

    local resist = 1 + (target:getMod(MOD_UDMGPHYS) / 100);

    dmg = dmg * resist;

    resist = 1 + (target:getMod(MOD_DMGPHYS) / 100);

    if(resist < 0.5) then
        resist = 0.5;
    end
	
	if(target:getObjType() == TYPE_PC) then
		-- Occasionally Annul Physical Damage
		if(math.random(100) <= target:getMod(MOD_NULL_PHYSICAL_DAMAGE)) then
			dmg = 0;
			return dmg;
		end
		-- Occasionally Annul Severe Physical Damage
		if((((dmg * resist) / target:getMaxHP()) * 100) > 50) then
			if(target:getEquipID(SLOT_BACK) == 14646 and (math.random(100) <= 5)) then -- Archon Cape
				dmg = 0;
				return dmg;
			end
		end
		-- Occasionally Absorb Physical Damage
		if(math.random(100) <= target:getMod(MOD_ABSORB_DMG_CHANCE)) then
			target:addHP(dmg);
			if(dmg > 0) then
				target:messageBasic(24,0,dmg);
			end
			dmg = 0;
			return dmg;
		end
		-- Sentinel's Scherzo
		if((((dmg * resist) / target:getMaxHP()) * 100) > 75) then
			dmg = dmg - (dmg  * (target:getMod(MOD_SCHERZO_DMG) / 100));
		end
	end

    return dmg * resist;
end;

-- -------------------------------------------------------------------
--																	--
--		== Ranged Damage Taken ==									--
--	Determine how much damage is taken from a ranged attack. 		--
--																	--
-- -------------------------------------------------------------------

function utils.rangedDmgTaken(target, dmg)

    local resist = 1 + (target:getMod(MOD_UDMGRANGE) / 100);

    dmg = dmg * resist;

    resist = 1 + (target:getMod(MOD_DMGRANGE) / 100);

    if(resist < 0.5) then
        resist = 0.5;
    end

    return dmg * resist;
end;