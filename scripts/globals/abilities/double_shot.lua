-----------------------------------
-- Ability: Double Shot
-- Occasionally uses two units of
-- ammunition to deal double damage. 
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/status");

-----------------------------------
-- OnUseAbility
-----------------------------------

function OnAbilityCheck(player,target,ability)
	return 0,0;
end;

function OnUseAbility(player, target, ability)

	local chance = 40;
	
	player:addStatusEffect(EFFECT_DOUBLE_SHOT,chance,0,90);
end;