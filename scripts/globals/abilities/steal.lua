-----------------------------------
-- Ability: Steal
-- Steals items from enemy.
-- Aura Steal adds Dispel to Steal
-- with a chance to absorb it.
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/status");

-----------------------------------

-----------------------------------
-- OnUseAbility
-----------------------------------

function OnAbilityCheck(player,target,ability)

	if (player:getFreeSlotsCount() == 0) then
		return MSGBASIC_FULL_INVENTORY,0;
	else
		return 0,0;
	end
end;

function OnUseAbility(player, target, ability)
	local thfLevel;
	local stolen = 0;
	local merits = player:getMerit(MERIT_AURA_STEAL);

	if(player:getMainJob() == JOB_THF) then
		thfLevel = player:getMainLvl();
	else
		thfLevel = player:getSubLvl();
	end

	local stealMod = player:getMod(MOD_STEAL); -- Rouge Ring accounted for in latents

	local stealChance = 50 + stealMod * 2 + thfLevel - target:getMainLvl();

	stolen = target:getStealItem();
	if(target:isMob() and math.random(100) < stealChance and stolen ~= 0) then
		if (merits >= 20) then -- Aura Steal
			if (math.random(100) <= merits) then -- Absorb an effect
				local effect1 = target:copyStatusEffect();
				local effect = target:dispelStatusEffect(EFFECTFLAG_DISPELABLE);
				player:addStatusEffect(effect1:getType(), effect1:getPower(), effect1:getTickCount(), effect1:getDuration());
			else
				target:dispelStatusEffect(EFFECTFLAG_DISPELABLE); -- Otherwise we just Dispel it
			end
		end
	
		player:addItem(stolen);
		target:itemStolen();
		ability:setMsg(125); -- Item stolen successfully
	else
		ability:setMsg(153); -- Failed to steal
	end

	return stolen;
end;
