-----------------------------------
-- Ability: Martyr
-- Sacrifices HP to heal a party
-- member double the amount.
-- Merits: Increases HP restored by
-- 5% for each rank beyond the
-- first.
-----------------------------------

require("scripts/globals/status");

-----------------------------------
-- OnUseAbility
-----------------------------------

function OnAbilityCheck(player,target,ability)
	return 0,0;
end;

function OnUseAbility(player, target, ability)
	local merit = target:getMerit(MERIT_MARTYR); -- 5% increase to HP past 1st Merit
	if (player:getHP() > 3) then
		player:delHP(effectPower);
		effectPower = effectPower + (effectPower * ((merit * .01) - .05));
		player:addHP(effectPower*2);
	end
end;