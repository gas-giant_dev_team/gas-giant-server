-----------------------------------
-- Ability: Spirit Link
-- Sacrifices own HP to heal
-- Wyvern's HP.
-- The correct equation is
-- ( Expended HP + MND + α ) × 2
-- α = FLOOR(Wyvern Level * 0.7)
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/status");

-----------------------------------
-- OnUseAbility
-----------------------------------

function OnAbilityCheck(player,target,ability)
	local pet = player:getPet();
	if (pet == nil) then
		return MSGBASIC_REQUIRES_A_PET,0;
	else
		if (player:getPet():getHP() == player:getPet():getMaxHP() and player:getMerit(MERIT_EMPATHY) == 0) then
			return MSGBASIC_UNABLE_TO_USE_JA,0;
		else
			return 0,0;
		end
	end
end;

function OnUseAbility(player, target, ability)

	local playerHP = player:getHP();
	local drainamount = (math.random(25,35) / 100) * playerHP;

	if (player:hasStatusEffect(EFFECT_STONESKIN)) then
		local skin = player:getMod(MOD_STONESKIN);

		if(skin >= drainamount) then
			if(skin == drainamount) then
				player:delStatusEffect(EFFECT_STONESKIN);
			else
				local effect = player:getStatusEffect(EFFECT_STONESKIN);
				effect:setPower(effect:getPower() - drainamount); -- Fixes the status effect so when it ends it uses the new power instead of old
				player:delMod(MOD_STONESKIN,drainamount); -- Removes the amount from the mod

			end
		else
			player:delStatusEffect(EFFECT_STONESKIN);
			player:delHP((drainamount-skin));
		end

	else
		player:delHP(drainamount);
	end

	local pet = player:getPet();
	local alpha = pet:getMainLvl() * 0.7;
	local healPet = (drainamount + player:getStat(MOD_MND) + alpha) * 2;
	-- Add in the Gauntlet Modifiers (Enhances "Spirit Link" effect)
	healPet = healPet + player:getMod(MOD_SPIRIT_LINK_HP); -- Straight addition
	healPet = healPet + (healPet * (player:getMod(MOD_SPIRIT_LINK_HP) * .01)); -- Percent multiplier
	local petTP = pet:getTP();
	local numEffectsToRemove = 1; -- Default try to remove 1 status effect.
	if (player:getMod(MOD_SPIRIT_LINK_HP) == 5) then
		numEffectsToRemove = numEffectsToRemove + math.random(1,2);
	elseif (player:getMod(MOD_SPIRIT_LINK_HP) == 10) then
		numEffectsToRemove = numEffectsToRemove + math.random(2,3);
	end
	-- print("Heal Amount:",healPet);
	pet:addHP(healPet); -- Add the HP to pet
	player:addTP(petTP/2); -- Add half pet TP to you
	pet:delTP(petTP/2); -- Remove half TP from pet

	-- Remove status effects
	local i = 1;
	removeSleepEffects(pet); -- ALWAYS remove sleep statuses
	while i <= (numEffectsToRemove) do
		if (pet:delStatusEffect(EFFECT_POISON)) then
			i = i + 1;
		elseif (pet:delStatusEffect(EFFECT_PARALYSIS)) then
			i = i + 1;
		elseif (pet:delStatusEffect(EFFECT_BLINDNESS)) then
			i = i + 1;
		elseif (pet:delStatusEffect(EFFECT_PETRIFICATION)) then
			i = i + 1;
		elseif (pet:delStatusEffect(EFFECT_GRADUAL_PETRIFICATION)) then
			i = i + 1;
		elseif (pet:delStatusEffect(EFFECT_SLOW)) then
			i = i + 1;
		elseif (pet:delStatusEffect(EFFECT_DISEASE)) then
			i = i + 1;
		elseif (pet:delStatusEffect(EFFECT_CURSE_I)) then
			i = i + 1;
		elseif (pet:delStatusEffect(EFFECT_PLAGUE)) then
			i = i + 1;
		elseif (pet:delStatusEffect(EFFECT_BANE)) then
			i = i + 1;
		elseif (pet:hasStatusEffect(EFFECT_DOOM)) then -- Even chance to cure Doom
			if (math.random(1,2) == 1) then
				pet:delStatusEffect(EFFECT_DOOM);
			end
			i = i + 1;
		else -- No effects to remove
			break;
		end
	end

	-- Copy the players status effects from Empathy (Max 5 from Merits)
	local merits = player:getMerit(MERIT_EMPATHY);
	local effect1 = player:copyStatusEffect();
	local effect2 = player:copyStatusEffect();
	local effect3 = player:copyStatusEffect();
	local effect4 = player:copyStatusEffect();
	local effect5 = player:copyStatusEffect();

	if (player:getMerit(MERIT_EMPATHY) >= 1) then
		i = 1;
		while i <= (merits) do -- Add to Wyvern
			if(effect1 ~= nil) then
				pet:addStatusEffect(effect1:getType(), effect1:getPower(), effect1:getTickCount(), effect1:getDuration());
				i = i + 1;
			elseif (effect2 ~= nil) then
				pet:addStatusEffect(effect2:getType(), effect2:getPower(), effect2:getTickCount(), effect2:getDuration());
				i = i + 1;
			elseif (effect3 ~= nil) then
				pet:addStatusEffect(effect3:getType(), effect3:getPower(), effect3:getTickCount(), effect3:getDuration());
				i = i + 1;
			elseif (effect4 ~= nil) then
				pet:addStatusEffect(effect4:getType(), effect4:getPower(), effect4:getTickCount(), effect4:getDuration());
				i = i + 1;
			elseif (effect5 ~= nil) then
				pet:addStatusEffect(effect5:getType(), effect5:getPower(), effect5:getTickCount(), effect5:getDuration());
				i = i + 1;
			else -- No effects to remove
				break;
			end
		end
	end

	-- HP recovered by Regen: Player’s level divided by 3, every 3 seconds
	pet:addStatusEffect(EFFECT_REGEN, (player:getMainLvl() / 3), 3, 90);

end;