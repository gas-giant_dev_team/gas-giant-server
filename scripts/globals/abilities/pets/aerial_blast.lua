---------------------------------------------------
-- Pet Ability: Aerial Blast
-- Blood Pact: Rage
-- Required MP: Caster's level x2.
-- Uses all MP and deals wind elemental damage to
-- enemies within area of effect. 
---------------------------------------------------

require("/scripts/globals/settings");
require("/scripts/globals/status");
require("/scripts/globals/magic");

---------------------------------------------------

function OnAbilityCheck(player, target, ability)
	return 0,0;
end;

function OnPetAbility(target, pet, skill)
	return 0;
end