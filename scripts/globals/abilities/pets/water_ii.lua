---------------------------------------------------
-- Pet Ability: Water II
-- Leviathan deals water elemental damage to target.
---------------------------------------------------

require("/scripts/globals/settings");
require("/scripts/globals/status");
require("/scripts/globals/monstertpmoves");
require("/scripts/globals/magic");

---------------------------------------------------

function OnAbilityCheck(player, target, ability)
    return 0,0;
end;

function OnPetAbility(target, pet, skill)
	local spell = getSpell(170);
	-- Calculate raw damage
	local dmg = calculateMagicDamage(95,2,pet,spell,target,ELEMENTAL_MAGIC_SKILL,MOD_INT,false);
	-- Get resist multiplier (1x if no resist)
	local resist = applyPlayerResistance(pet,-1,target,pet:getStat(MOD_INT)-target:getStat(MOD_INT),ELEMENTAL_MAGIC_SKILL,ELE_WATER);
	-- Get the resisted damage
	dmg = dmg*resist;
	-- Add on bonuses (staff/day/weather/jas/mab/etc all go in this function)
	dmg = mobAddBonuses(pet,spell,target,dmg, 3);
	-- Add on TP bonuses
	local tp = pet:getTP();
	if tp < 100 then
		tp = 100;
	end
	dmg = dmg * tp / 100;
	-- Add in final adjustments
	dmg = finalMagicAdjustments(pet,target,spell,dmg);
	return dmg;
end