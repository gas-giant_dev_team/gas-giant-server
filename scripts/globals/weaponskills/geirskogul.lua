-----------------------------------	
-- Weaponskill: Geirskogul	
-- Polearm weapon skill	
-- Skill Level: N/A	
-- Gae Assail/Gungnir: Shock
-- Spikes.	
-- This weapon skill is only
-- available with the stage 5 relic
-- Polearm Gungnir, within Dynamis
-- with the stage 4 Gae Assail, or
-- by activating the latent effect
-- on the Skogul Lance.	
-- Aligned with the Light Gorget,
-- Aqua Gorget & Snow Gorget.	
-- Aligned with the Light Belt,
-- Aqua Belt & Snow Belt.	
-- Element: None	
-- Modifiers: AGI:60%	
-- 100%TP    200%TP    300%TP	
-- 3.00      3.00      3.00	
-----------------------------------	

require("scripts/globals/status");
require("scripts/globals/settings");
require("scripts/globals/weaponskills");

-----------------------------------	

function OnUseWeaponSkill(player, target, wsID)	

	local params = {};
	params.numHits = 1;
	params.ftp100 = 3; params.ftp200 = 3; params.ftp300 = 3;
	params.str_wsc = 0.0; params.dex_wsc = 0.0; params.vit_wsc = 0.0; params.agi_wsc = 0.6; params.int_wsc = 0.0; params.mnd_wsc = 0.0; params.chr_wsc = 0.0;
	params.crit100 = 0.0; params.crit200 = 0.0; params.crit300 = 0.0;
	params.canCrit = false;
	params.acc100 = 0.0; params.acc200= 0.0; params.acc300= 0.0;
	params.atkmulti = 1;
	local damage, criticalHit, tpHits, extraHits = doPhysicalWeaponskill(player, target, params);

	local main = player:getEquipID(SLOT_MAIN);
	local aftermath = 0;
	local tp = player:getTP();
	local duration = 0;
	local zone = player:getZone();

	if (main == 18300) then
		aftermath = 1;
	elseif (main == 18301) then
		aftermath = 1;
	elseif (main == 18643) then
		aftermath = 1;
	elseif (main == 18657) then
		damage = damage * 1.25;
		aftermath = 1;
	elseif (main == 18671) then
		damage = damage * 1.25;
		aftermath = 1;
	elseif (main == 19752) then
		damage = damage * 1.4;
		aftermath = 1;
	elseif (main == 19845) then
		damage = damage * 1.4;
		aftermath = 1;
	elseif (main == 20925) then
		damage = damage * 1.4;
		aftermath = 1;
	elseif (main == 20926) then
		damage = damage * 1.4;
		aftermath = 1;
	elseif ((main == 18299) and (zone == 39 or zone == 40 or zone == 41 or zone == 42 or zone == 134 or 
		zone ==135 or zone == 185 or zone == 186 or zone == 187 or zone == 188)) then
		aftermath = 1;
	end

	if (aftermath == 1) then
		if (tp == 300) then
			duration = 60;
			player:delStatusEffect(EFFECT_AFTERMATH_LV1);
			player:delStatusEffect(EFFECT_AFTERMATH_LV2);
			player:delStatusEffect(EFFECT_AFTERMATH_LV3);
			player:addStatusEffect(EFFECT_AFTERMATH_LV3,8,0,duration);
		elseif (tp >= 200) then
			duration = 40;
			if (player:hasStatusEffect(EFFECT_AFTERMATH_LV3) == false) then
				player:delStatusEffect(EFFECT_AFTERMATH_LV1);
				player:delStatusEffect(EFFECT_AFTERMATH_LV2);
				player:addStatusEffect(EFFECT_AFTERMATH_LV2,8,0,duration);
			end
		else
			duration = 20;
			if (player:hasStatusEffect(EFFECT_AFTERMATH_LV3) == false) then
				if (player:hasStatusEffect(EFFECT_AFTERMATH_LV2) == false) then
					player:delStatusEffect(EFFECT_AFTERMATH_LV1);
					player:addStatusEffect(EFFECT_AFTERMATH_LV1,8,0,duration);
				end
			end
		end
	end

	return tpHits, extraHits, criticalHit, damage;

end
