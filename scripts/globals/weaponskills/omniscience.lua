-----------------------------------	
-- Weaponskill: Omniscience
-- Staff weapon skill
-- Skill Level: N/A
-- Lowers target's magic attack.
-- Duration of effect varies with
-- TP. Tupsimati: Aftermath effect
-- varies with TP.	
-- Reduces enemy's magic attack by
-- -10.	
-- Available only after completing
-- the Unlocking a Myth (Scholar)
-- quest.	
-- Aligned with the Shadow Gorget,
-- Soil Gorget & Light Gorget.	
-- Aligned with the Shadow Belt,
-- Soil Belt & Light Belt.	
-- Element: Dark	
-- Modifiers: MND:30%	
-- 100%TP    200%TP    300%TP	
-- 2.00      2.00      2.00	
-----------------------------------	

require("scripts/globals/status");
require("scripts/globals/settings");
require("scripts/globals/weaponskills");

-----------------------------------

function OnUseWeaponSkill(player, target, wsID)

	local params = {};
	params.numHits = 1;
	params.ftp100 = 2; params.ftp200 = 2; params.ftp300 = 2;
	params.str_wsc = 0.0; params.dex_wsc = 0.0; params.vit_wsc = 0.0; params.agi_wsc = 0.0; params.int_wsc = 0.0; params.mnd_wsc = 0.3; params.chr_wsc = 0.0;
	params.crit100 = 0.0; params.crit200 = 0.0; params.crit300 = 0.0;
	params.canCrit = false;
	params.acc100 = 0.0; params.acc200= 0.0; params.acc300= 0.0;
	params.atkmulti = 1;
	local damage, criticalHit, tpHits, extraHits = doPhysicalWeaponskill(player, target, params);

	if damage > 0 then
		local tp = player:getTP();
		local duration = (tp/100);
		if(target:hasStatusEffect(EFFECT_MAGIC_ATK_DOWN) == false) then
			target:addStatusEffect(EFFECT_MAGIC_ATK_DOWN, 10, 0, duration);
		end
	end

	local main = player:getEquipID(SLOT_MAIN);
	local aftermath = 0;
	local tp = player:getTP();
	local duration = 0;
	local subpower = 0;
			

	if((player:getEquipID(SLOT_MAIN) == 18990) and (player:getMainJob() == JOB_SCH)) then
		if(damage > 0) then	

--		AFTERMATH LV1		

		if ((player:getTP() >= 100) and (player:getTP() <= 110)) then 
			player:addStatusEffect(EFFECT_AFTERMATH_LV1, 10, 0, 180, 0, 2); 
	elseif ((player:getTP() >= 111) and (player:getTP() <= 120)) then 
			player:addStatusEffect(EFFECT_AFTERMATH_LV1, 11, 0, 180, 0, 2); 
	elseif ((player:getTP() >= 121) and (player:getTP() <= 130)) then 
			player:addStatusEffect(EFFECT_AFTERMATH_LV1, 12, 0, 180, 0, 2); 
	elseif ((player:getTP() >= 131) and (player:getTP() <= 140)) then 
			player:addStatusEffect(EFFECT_AFTERMATH_LV1, 13, 0, 180, 0, 2); 
	elseif ((player:getTP() >= 141) and (player:getTP() <= 150)) then 
			player:addStatusEffect(EFFECT_AFTERMATH_LV1, 14, 0, 180, 0, 2); 
	elseif ((player:getTP() >= 151) and (player:getTP() <= 160)) then 
			player:addStatusEffect(EFFECT_AFTERMATH_LV1, 15, 0, 180, 0, 2); 
	elseif ((player:getTP() >= 161) and (player:getTP() <= 170)) then 
			player:addStatusEffect(EFFECT_AFTERMATH_LV1, 16, 0, 180, 0, 2); 
	elseif ((player:getTP() >= 171) and (player:getTP() <= 180)) then 
			player:addStatusEffect(EFFECT_AFTERMATH_LV1, 17, 0, 180, 0, 2); 
	elseif ((player:getTP() >= 181) and (player:getTP() <= 190)) then 
			player:addStatusEffect(EFFECT_AFTERMATH_LV1, 18, 0, 180, 0, 2); 
	elseif ((player:getTP() >= 191) and (player:getTP() <= 199)) then 
			player:addStatusEffect(EFFECT_AFTERMATH_LV1, 19, 0, 180, 0, 2); 


--		AFTERMATH LV2

	elseif ((player:getTP() >= 200) and (player:getTP() <= 210)) then
			player:addStatusEffect(EFFECT_AFTERMATH_LV2, 24, 0, 180, 0, 3);
	elseif ((player:getTP() >= 211) and (player:getTP() <= 219)) then
			player:addStatusEffect(EFFECT_AFTERMATH_LV2, 28, 0, 180, 0, 3);
	elseif ((player:getTP() >= 221) and (player:getTP() <= 229)) then
			player:addStatusEffect(EFFECT_AFTERMATH_LV2, 32, 0, 180, 0, 3);
	elseif ((player:getTP() >= 231) and (player:getTP() <= 239)) then
			player:addStatusEffect(EFFECT_AFTERMATH_LV2, 36, 0, 180, 0, 3);
	elseif ((player:getTP() >= 241) and (player:getTP() <= 249)) then
			player:addStatusEffect(EFFECT_AFTERMATH_LV2, 40, 0, 180, 0, 3);
	elseif ((player:getTP() >= 251) and (player:getTP() <= 259)) then
			player:addStatusEffect(EFFECT_AFTERMATH_LV2, 44, 0, 180, 0, 3);
	elseif ((player:getTP() >= 261) and (player:getTP() <= 269)) then
			player:addStatusEffect(EFFECT_AFTERMATH_LV2, 48, 0, 180, 0, 3);
	elseif ((player:getTP() >= 271) and (player:getTP() <= 279)) then
			player:addStatusEffect(EFFECT_AFTERMATH_LV2, 52, 0, 180, 0, 3);
	elseif ((player:getTP() >= 281) and (player:getTP() <= 289)) then
			player:addStatusEffect(EFFECT_AFTERMATH_LV2, 56, 0, 180, 0, 3);
	elseif ((player:getTP() >= 291) and (player:getTP() <= 299)) then
			player:addStatusEffect(EFFECT_AFTERMATH_LV2, 59, 0, 180, 0, 3);

--		AFTERMATH LV3

		elseif ((player:getTP() == 300)) then
			player:addStatusEffect(EFFECT_AFTERMATH_LV3, 45, 0, 120, 0, 1);

			end
		end
	end
	if (main == 18990) then
		aftermath = 1;
	elseif (main == 19079) then
		aftermath = 1;
	elseif (main == 19099) then
		aftermath = 1;
	elseif (main == 19631) then
		aftermath = 1;
		damage = damage * 1.15;
	elseif (main == 19729) then
		aftermath = 1;
		damage = damage * 1.15;
	elseif (main == 19838) then
		aftermath = 1;
		damage = damage * 1.3;
	elseif (main == 19967) then
		aftermath = 1;
		damage = damage * 1.3;
	elseif (main == 21137) then
		aftermath = 1;
		damage = damage * 1.3;
	elseif (main == 21138) then
		aftermath = 1;
		damage = damage * 1.3;
	end

	if (aftermath == 1) then
		if (tp == 300) then
			player:delStatusEffect(EFFECT_AFTERMATH_LV1);
			player:delStatusEffect(EFFECT_AFTERMATH_LV2);
			player:delStatusEffect(EFFECT_AFTERMATH_LV3);
			if (main == 18990) then
				duration = 120;
				player:addStatusEffect(EFFECT_AFTERMATH_LV3,14,0,duration,0,40);
			elseif (main == 19079 or main == 19099 or main == 19631) then
				duration = 180;
				player:addStatusEffect(EFFECT_AFTERMATH_LV3,14,0,duration,0,60);
			elseif (main == 19729 or main == 19838 or main == 19967) then
				duration = 180;
				player:addStatusEffect(EFFECT_AFTERMATH_LV3,15,0,duration,0,20);
			end
		elseif (tp >= 200) then
			if (player:hasStatusEffect(EFFECT_AFTERMATH_LV3) == false) then
				player:delStatusEffect(EFFECT_AFTERMATH_LV1);
				player:delStatusEffect(EFFECT_AFTERMATH_LV2);
				if (main == 18990) then
					duration = 180;
					subpower = math.floor((tp / 10) - 10);
				elseif (main == 19079 or main == 19099 or main == 19631) then
					duration = 270;
					subpower = math.floor((tp / 5) - 20);
				elseif (main == 19729 or main == 19838 or main == 19967) then
					duration = 270;
					subpower = math.floor((tp / 5) - 10);
				end
				player:addStatusEffect(EFFECT_AFTERMATH_LV2,16,0,duration,0,subpower);
			end
		else
			if (player:hasStatusEffect(EFFECT_AFTERMATH_LV3) == false) then
				if (player:hasStatusEffect(EFFECT_AFTERMATH_LV2) == false) then
					player:delStatusEffect(EFFECT_AFTERMATH_LV1);
					if (main == 18990) then
						duration = 180;
						subpower = math.floor(tp / 10);
					elseif (main == 19079 or main == 19099 or main == 19631) then
						duration = 270;
						subpower = math.floor(tp / 10);
					elseif (main == 19729 or main == 19838 or main == 19967) then
						duration = 270;
						subpower = math.floor(tp / 10);
					end
					player:addStatusEffect(EFFECT_AFTERMATH_LV1,15,0,duration,0,subpower);
				end
			end
		end
	end

	return tpHits, extraHits, criticalHit, damage;

end
