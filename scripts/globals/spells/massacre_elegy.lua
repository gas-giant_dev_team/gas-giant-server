-----------------------------------------
-- Spell: Massacre Elegy
-- It inflicts an effect similar to Slow
-- on the targeted enemy, increasing its
-- attack delay and magic recast timers.
-----------------------------------------

require("scripts/globals/status");
require("scripts/globals/magic");

-----------------------------------------
-- OnSpellCast
-----------------------------------------

function OnMagicCastingCheck(caster,target,spell)
	return 0;
end;

function onSpellCast(caster,target,spell)
	local duration = 120;
	local power = 768;

	local bonus = AffinityBonus(caster, spell:getElement());
	local pCHR = caster:getStat(MOD_CHR);
	local mCHR = target:getStat(MOD_CHR);
	local dCHR = (pCHR - mCHR);
	local resm = applyResistance(caster,spell,target,dCHR,SINGING_SKILL,bonus);
	if(resm < 0.5) then
		spell:setMsg(85);--resist message
		return 1;
	end

	if(100 * math.random() < target:getMod(MOD_SLOWRES)) then
		spell:setMsg(85); -- resisted spell
	else
        local iBoost = caster:getMod(MOD_ELEGY) + caster:getMod(MOD_ALL_SONGS);
        power = power + iBoost*10;
        
        if (caster:hasStatusEffect(EFFECT_SOUL_VOICE)) then
            power = power * 2;
        elseif (caster:hasStatusEffect(EFFECT_MARCATO)) then
            power = power * 1.5;
        end
        caster:delStatusEffect(EFFECT_MARCATO);
        
        duration = duration * ((iBoost * 0.1) + (caster:getMod(MOD_SONG_DURATION)/100) + 1);
        
        if (caster:hasStatusEffect(EFFECT_TROUBADOUR)) then
            duration = duration * 2;
	end

        -- Try to overwrite weaker elegy
        if(canOverwrite(target, EFFECT_ELEGY, power)) then
            -- overwrite them
            target:delStatusEffect(EFFECT_ELEGY);
            target:addStatusEffect(EFFECT_ELEGY,power,0,duration);
			spell:setMsg(237);
		else
			spell:setMsg(75); -- no effect
		end

	end

	return EFFECT_ELEGY;
end;