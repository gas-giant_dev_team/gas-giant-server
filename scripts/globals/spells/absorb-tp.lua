--------------------------------------
-- Spell: Absorb-TP
-- Steals an enemy's TP.
-- MP Cost: 33
--------------------------------------

require("scripts/globals/settings");
require("scripts/globals/status");
require("scripts/globals/magic");

-----------------------------------------
-- OnSpellCast
-----------------------------------------

function OnMagicCastingCheck(caster,target,spell)
	return 0;
end;

function onSpellCast(caster,target,spell)
	
    local amountMods = caster:getMod(MOD_ABSORB_POTENCY)/100;

	local dmg = math.random();
	dmg = math.random();
    dmg = math.random(10, 80);
    -- Get resist multiplier (1x if no resist)
    local resist = applyResistance(caster,spell,target,caster:getStat(MOD_INT)-target:getStat(MOD_INT),DARK_MAGIC_SKILL,1.0);
    -- Get the resisted damage
    dmg = dmg*resist;
    -- Add on bonuses (staff/day/weather/jas/mab/etc all go in this function)
    dmg = addBonuses(caster,spell,target,dmg);
    -- Add in target adjustment
    dmg = adjustForTarget(target,dmg,spell:getElement());
    -- Add in final adjustments

    local cap = 120;
    if (dmg > cap) then
        dmg = cap;
    end
    
    dmg = dmg + (dmg * amountMods); -- for Equipment Bonus above cap

    if(resist <= 0.125) then
        spell:setMsg(85);
    else
        spell:setMsg(454);

        if(target:getTP() < dmg) then
            dmg = target:getTP();
        end

        -- Drain
        caster:addTP(dmg);
        target:addTP(-dmg);

    end
    return dmg;
end;