-----------------------------------------
-- Spell: Auspice
-- Reduces TP dealt when striking an
-- enemy and bestows an accuracy bonus
-- when the target is missed for party
-- members within area of effect.
-- MP Cost: 48
-- TODO: Accuracy on miss with Afflatus
-- Misery
-----------------------------------------

require("scripts/globals/status");
require("scripts/globals/magic");

-----------------------------------------
-- OnSpellCast
-----------------------------------------

function OnMagicCastingCheck(caster,target,spell)
	return 0;
end;

function onSpellCast(caster,target,spell)
	local power = 10;
	local duration = 180;
	target:addStatusEffect(EFFECT_AUSPICE,power,0,duration);
	if(target:hasStatusEffect(EFFECT_AFFLATUS_MISERY)) then
		local effect = EFFECT_ENLIGHT;
		doEnspell(caster,target,spell,effect);
	end
	-- When utilized with Afflatus Misery active, Auspice grants an additional effect of light damage on melee attacks performed by the WHM, and cumulatively increases accuracy with each missed attack.
	-- The accuracy bonus resets if you switch to Afflatus Solace and back to Afflatus Misery but does not seem to reset if you reapply Auspice as long as you do not let it wear off.
	-- This is thought to cap at +30 Acc.
	return EFFECT_AUSPICE;
end;