-----------------------------------------
-- Bluemagic: Magic Fruit
-- Restores target party member's HP
-- and enhances attack and magic attack.
-- MP +5  STR +2
-- Lvl.: 76 MP Cost: 106 Blue Points: 4
-----------------------------------------

require("scripts/globals/settings");
require("scripts/globals/status");
require("scripts/globals/magic");

-----------------------------------------
-- OnMagicCastingCheck
-----------------------------------------

function OnMagicCastingCheck(caster,target,spell)
    return 0;
end;

-----------------------------------------
-- OnSpellCast
-----------------------------------------

function onSpellCast(caster,target,spell)
    local minCure = 360;

    local divisor = 0.6666;
    local constant = 130;
    local power = getCurePowerOld(caster);
    if(power > 559) then
        divisor = 2.8333;
        constant = 391.2
    elseif(power > 319) then
        divisor =  1;
        constant = 210;
    end

    local final = getCureFinal(caster,spell,getBaseCureOld(power,divisor,constant),minCure,true);

    final = final + (final * (target:getMod(MOD_CURE_POTENCY_RCVD)/100));

    if(target:getObjType() == TYPE_PC) then
        --Applying server mods....
        final = final * CURE_POWER;
    end

    local diff = (target:getMaxHP() - target:getHP());
    if(final > diff) then
        final = diff;
    end
    target:addHP(final);

    target:wakeUp();
    caster:updateEnmityFromCure(target,final);
    spell:setMsg(7);
    return final;
end;