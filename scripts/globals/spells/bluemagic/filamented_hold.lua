-----------------------------------------
-- Bluemagic: Filamented Hold
-- Reduces the attack speed of enemies
-- within a fan-shaped area originating
-- from the caster.
-- VIT +1
-- Lvl.: 52 MP Cost: 38 Blue Points: 3
-----------------------------------------

require("scripts/globals/status");
require("scripts/globals/magic");
require("scripts/globals/bluemagic");

-----------------------------------------
-- OnSpellCast
-----------------------------------------

function OnMagicCastingCheck(caster,target,spell)
	return 0;
end;

function onSpellCast(caster,target,spell)

	local dINT = caster:getStat(MOD_MND) - target:getStat(MOD_MND);
	local resist = applyResistance(caster,spell,target,dINT,37);
	if(resist > (1/16)) then
		-- resisted!
		spell:setMsg(85);
		return 0;
	end

	if(target:hasStatusEffect(EFFECT_SLOW)) then
		-- no effect
		spell:setMsg(75);
	else
		target:addStatusEffect(EFFECT_SLOW,15,0,60);
		spell:setMsg(236);
	end

	return EFFECT_SLOW;
end;