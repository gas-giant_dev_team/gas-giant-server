-----------------------------------------
-- Bluemagic: Quadratic Continuum
-- Delivers a fivefold attack. 
-- Accuracy varies with TP.
-- DEX+3  CHR-2 
-- Lvl.: 85 MP Cost: 91 Blue Points: 4
-----------------------------------------

require("scripts/globals/magic");
require("scripts/globals/status");
require("scripts/globals/bluemagic");

-----------------------------------------
-- OnSpellCast
-----------------------------------------

function OnMagicCastingCheck(caster,target,spell)
	return 0;
end;

function onSpellCast(caster,target,spell)
	local params = {};
	-- This data should match information on http://wiki.ffxiclopedia.org/wiki/Calculating_Blue_Magic_Damage
	params.tpmod = TPMOD_DAMAGE; params.dmgtype = DMGTYPE_PIERCE; params.scattr = SC_DISTORTION;
	params.numhits = 4;
	params.multiplier = 1.25; params.tp150 = 1.5; params.tp300 = 1.75; params.azuretp = 1.0; params.duppercap = 100; -- D upper >=69
	params.str_wsc = 0.32; params.dex_wsc = 0.0; params.vit_wsc = 0.32; params.agi_wsc = 0.0; params.int_wsc = 0.0; params.mnd_wsc = 0.0; params.chr_wsc = 0.0;
	damage = BluePhysicalSpell(caster, target, spell, params);
	damage = BlueFinalAdjustments(caster, target, spell, damage, params);

	return damage;
end;