-----------------------------------------
-- Bluemagic: Geist Wall
-- Removes 1 beneficial magic effect from
-- enemies within range
-- HP -5 MP +10
-- Lvl.: 46 MP Cost: 35 Blue Points: 3
-----------------------------------------

require("scripts/globals/magic");
require("scripts/globals/status");
require("scripts/globals/bluemagic");

-----------------------------------------
-- OnSpellCast
-----------------------------------------

function OnMagicCastingCheck(caster,target,spell)
	return 0;
end;

function onSpellCast(caster,target,spell)

	local dINT = (caster:getStat(MOD_INT) - target:getStat(MOD_INT));

	local resist = applyResistance(caster,spell,target,dINT,BLUE_SKILL);
	local effect = EFFECT_NONE;

	if(resist > 0.0625) then
		spell:setMsg(341);
		local effect = target:dispelStatusEffect();
		if(effect == EFFECT_NONE) then
			spell:setMsg(75);
		end
	else
		spell:setMsg(85);
	end

	return effect;
end;