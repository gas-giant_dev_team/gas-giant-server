-- -------------------------------------------------------------------
--                                                                  --
--        == GLOBAL SETTINGS ==                                     --
--                                                                  --
--  This is to allow server operators to further customize servers. --
--  As more features are added, the list will surely expand         --
--                                                                  --
--  Anything scripted can be customized with proper script editing. --
--                                                                  --
--  PLEASE REQUIRE THIS SCRIPT IN ANY SCRIPTS YOU DO:               --
--      ADD THIS LINE TO THE TOP!!!!                                --
--  require("scripts/globals/settings");                            --
--  With this added to your script, you can pull variables from it! --
--                                                                  --
--  Common functions                                                --
--  require("scripts/globals/common");                              --
--                                                                  --
--  Always include status.lua, which defines mods                   --
--  require("scripts/globals/status");                              --
--                                                                  --
-- -------------------------------------------------------------------

require("scripts/globals/common");

-- -------------------------------------------------------------------
--                                                                  --
--  Enable Extension (1= yes 0= no)                                 --
--                                                                  --
-- -------------------------------------------------------------------

ENABLE_COP     = 1;
ENABLE_TOAU    = 1;
ENABLE_WOTG    = 1;
ENABLE_ACP     = 1;
ENABLE_AMK     = 1;
ENABLE_ASA     = 1;
ENABLE_ABYSSEA = 1;
ENABLE_SOA     = 1;

-- -------------------------------------------------------------------
--                                                                  --
--  CHARACTER CONFIG                                                --
--                                                                  --
-- -------------------------------------------------------------------

INITIAL_LEVEL_CAP             = 50; -- The initial level cap for new players.  There seems to be a hardcap of 255.
MAX_LEVEL                     = 99; -- Level max of the server, lowers the attainable cap by disabling Limit Break quests.
START_INVENTORY               = 30; -- Starting inventory and satchel size.  Ignores values < 30.  Do not set above 80!
OPENING_CUTSCENE_ENABLE       = 0;  -- Set to 1 to enable opening cutscenes, 0 to disable.
SUBJOB_QUEST_LEVEL            = 18; -- Minimum level to accept either subjob quest. Default: 18 - Set to 0 to start the game with subjobs unlocked.
ADVANCED_JOB_LEVEL            = 30; -- Minimum level to accept advanced job quests. Default: 30 - Set to 0 to start the game with advanced jobs.
UNLOCK_MERITS                 = 0;  -- Default: 0 - Set to 1 to allow characters to change to gain limit points right away.
ALL_MAPS                      = 0;  -- Default: 0 - Set to 1 to give starting characters all the maps.
UNLOCK_OUTPOST_WARPS          = 0;  -- Default: 0 - Set to 1 to give starting characters all outpost warps.  2 to add Tu'Lia and Tavnazia.
UNLOCK_EXTRA_SPELLS           = 0;  -- Default: 0 - Set to 1 to give starting characters all the unattainable Spells.
UNLOCK_TELEPORTATION_ITEMS    = 0;  -- Default: 0 - Set to 1 to give starting characters all travel related items (Airship passes, chocobo license, teleport crystals, etc).
UNLOCK_CLAIM_SLIPS            = 0;  -- Default: 0 - Set to 1 to give starting characters all the Claim Slips.
UNLOCK_ALL_EVENT_ITEMS_STORED = 0;  -- Default: 0 - Set to 1 to give starting characters all the Items stored on the Event Item Storage NPC.
GM_SKIP_MOGHOUSE_SAVE         = 1;  -- Default: 1 - Allows GM Level characters to skip the save cutscene on zoning into a town or exiting a Mog House

-- -------------------------------------------------------------------
--                                                                  --
--  STARTING CURRENCY                                               --
--  Gil Default: 10     All else Default: 0                         --
--                                                                  --
-- -------------------------------------------------------------------

START_GIL               = 10; -- Amount of gil given to newly created characters.
START_CP                = 0;  -- Amount of Conquest Points given to newly created characters.
START_VALOR_POINTS      = 0;  -- Amount of Valor Points (Tabs) given to newly created characters.
START_IMPERIAL_STANDING = 0;  -- Amount of Imperial Standing given to newly created characters.
START_AP_LEUJAOAM       = 0;  -- Amount of Leujaom Assault Points given to newly created characters.
START_AP_MAMOOL         = 0;  -- Amount of Mamool Assault Points given to newly created characters.
START_AP_LEBROS         = 0;  -- Amount of Lebros Assault Points given to newly created characters.
START_AP_PERIQIA        = 0;  -- Amount of Periqia Assault Points given to newly created characters.
START_AP_ILRUSI         = 0;  -- Amount of Ilrusi Assault Points given to newly created characters.
START_AP_NYZUL          = 0;  -- Amount of Nyzul Assault Points given to newly created characters.
START_ZENI              = 0;  -- Amount of Zeni given to newly created characters.
START_CRUOR             = 0;  -- Amount of Cruor given to newly created characters.
START_B_SEALS           = 0;  -- Amount of Beastman's Seals given to newly created characters.
START_K_SEALS           = 0;  -- Amount of Kindred Seals given to newly created characters.
START_K_CRESTS          = 0;  -- Amount of Kindred Crests given to newly created characters.
START_HK_CRESTS         = 0;  -- Amount of High Kindred Crests given to newly created characters.
START_SK_CRESTS         = 0;  -- Amount of Sacred Kindred Crests given to newly created characters.
START_TAGS              = 0;  -- Amount of Assault Tags given to newly created characters.
START_TRAVERSER_STONES  = 0;  -- Amount of Traverser's Stones given to newly created characters.
START_VOIDSTONES        = 0;  -- Amount of Voidstones given to newly created characters.

-- -------------------------------------------------------------------
--                                                                  --
--  Multipliers - Fame - Gil - Exp      Default: 1.00               --
--                                                                  --
-- -------------------------------------------------------------------

SHOP_PRICE = 1.000; -- Multiplies prices in NPC shops.
GIL_RATE   = 1.000; -- Multiplies gil earned from quests. Will not always display in game.
EXP_RATE   = 1.000; -- Multiplies exp earned.
SAN_FAME   = 1.000; -- Multiplies fame earned from San d'Oria quests.
BAS_FAME   = 1.000; -- Multiplies fame earned from Bastok quests.
WIN_FAME   = 1.000; -- Multiplies fame earned from Windurst quests.
NORG_FAME  = 1.000; -- Multiplies fame earned from Norg and Tenshodo quests.
JEUNO_FAME = 1.000; -- Multiplies fame earned from Jeuno quests.

-- -------------------------------------------------------------------
--                                                                  --
--  Multipliers - Trade Guild Points        Default: 1.00           --
--                                                                  --
-- -------------------------------------------------------------------

FISHING_GUILD_POINTS      = 1.000; -- Multiplies guild points earned from fishermans' guild trades.
WOODWORKING_GUILD_POINTS  = 1.000; -- Multiplies guild points earned from carpenters' guild trades.
SMITHING_GUILD_POINTS     = 1.000; -- Multiplies guild points earned from blacksmiths' guild trades.
GOLDSMITHING_GUILD_POINTS = 1.000; -- Multiplies guild points earned from goldsmiths' guild trades.
CLOTHCRAFT_GUILD_POINTS   = 1.000; -- Multiplies guild points earned from weavers' guild trades.
LEATHERCRAFT_GUILD_POINTS = 1.000; -- Multiplies guild points earned from tanners' guild trades.
BONECRAFT_GUILD_POINTS    = 1.000; -- Multiplies guild points earned from boneworkers' guild trades.
ALCHEMY_GUILD_POINTS      = 1.000; -- Multiplies guild points earned from alchemists' guild trades.
COOKING_GUILD_POINTS      = 1.000; -- Multiplies guild points earned from culinarians' guild trades.

-- -------------------------------------------------------------------
--                                                                  --
--  Disable Contracts to join Guilds and earn Points                --
--      (0 - 1)     Default: 0                                      --
--                                                                  --
-- -------------------------------------------------------------------

DISABLE_GUILD_CONTRACTS = 0; -- Set to 1 to disable guild contracts, allowing players to accumulate guild points from all guilds at once.

-- -------------------------------------------------------------------
--                                                                  --
--  Multipliers - Item, Spell, TP       Default: 1.00               --
--                                                                  --
-- -------------------------------------------------------------------

CURE_POWER          = 1.000; -- Multiplies amount healed from Healing Magic, including the relevant Blue Magic.
SPELL_POWER         = 1.000; -- Multiplies damage dealt by Elemental and Divine Magic.
BLUE_POWER          = 1.000; -- Multiplies damage dealt by most Blue Magic.
DRAIN_POWER         = 1.000; -- Multiplies amount drained by Drain, Aspir, and relevant Blue Magic spells.
ITEM_POWER          = 1.000; -- Multiplies the effect of items such as Potions and Ethers.
PETFOOD_POWER       = 1.000; -- Multiplies the effect of pet foods biscuits and pet healing items.
WEAPON_SKILL_POWER  = 1.000; -- Multiplies damage dealt by Weapon Skills.
WEAPON_SKILL_POINTS = 1.000; -- Multiplies points earned during weapon unlocking.

-- -------------------------------------------------------------------
--                                                                  --
--  Hobbys - Item Break and Receive Item Chance                     --
--  Set between 0.00 and 1.00   Defaults: Break: 0.33 Receive 0.50  --
--                                                                  --
-- -------------------------------------------------------------------

HARVESTING_BREAK_CHANCE = 0.33; -- % chance for the sickle to break during harvesting.
EXCAVATION_BREAK_CHANCE = 0.33; -- % chance for the pickaxe to break during excavation.
LOGGING_BREAK_CHANCE    = 0.33; -- % chance for the hatchet to break during logging.
MINING_BREAK_CHANCE     = 0.33; -- % chance for the pickaxe to break during mining.
HARVESTING_RATE         = 0.50; -- % chance to recieve an item from haresting.
EXCAVATION_RATE         = 0.50; -- % chance to recieve an item from excavation.
LOGGING_RATE            = 0.50; -- % chance to recieve an item from logging.
MINING_RATE             = 0.50; -- % chance to recieve an item from mining.

-- -------------------------------------------------------------------
--                                                                  --
--  SE implemented coffer/chest illusion time in order to prevent   --
--  coffer farming. No-one in the same area can open a chest or     --
--  coffer for loot (gil, gems & items) till a random time between  --
--  MIN_ILLSION_TIME and MAX_ILLUSION_TIME. During this time        --
--  players can loot keyitem and item related to quests (AF,        --
--  maps... etc.)                                                   --
--              Time in seconds                                     --
--      Defaults: MAX - 3600    MIN - 1800                          --
--                                                                  --
-- -------------------------------------------------------------------

COFFER_MAX_ILLUSION_TIME = 3600; -- 1 hour
COFFER_MIN_ILLUSION_TIME = 1800; -- 30 minutes
CHEST_MAX_ILLUSION_TIME  = 3600; -- 1 hour
CHEST_MIN_ILLUSION_TIME  = 1800; -- 30 minutes

-- -------------------------------------------------------------------
--                                                                  --
--  Divisors to modify timers for:                                  --
--  Notorious Monster (NM), High Notorious Monster (HNM)            --
--  and also non HNMs with 20+ hr respawn times.                    --
--  Use numbers below 1.00 to increase time.                        --
--      Default: 1.00 - Do not set to exactly zero.                 --
--                                                                  --
-- -------------------------------------------------------------------

NM_TIMER_MOD     = 1.00;
HNM_TIMER_MOD    = 1.00;
LongNM_TIMER_MOD = 1.00;

-- -------------------------------------------------------------------
--                                                                  --
--  Sets spawn type for: Behemoth, Fafnir, Adamantoise,             --
--  King Behemoth, Nidhog, Aspidochelone.                           --
--  Use 0 for timed spawns, 1 for force pop only, 2 for both        --
--                                                                  --
-- -------------------------------------------------------------------

LandKingSystem_NQ = 2;
LandKingSystem_HQ = 2;

-- -------------------------------------------------------------------
--                                                                  --
--      DYNAMIS SETTINGS                                            --
--                                                                  --
-- -------------------------------------------------------------------

    BETWEEN_2DYNA_WAIT_TIME = 1;      -- Default & Minimum: 1 day. - Days between 2 Dynamis entries.
             DYNA_LEVEL_MIN = 65;     -- Default: 65 - Minimum Level for entering Dynamis.
    TIMELESS_HOURGLASS_COST = 500000; -- Default: 500000 - Cost of the timeless hourglass for Dynamis.
     CURRENCY_EXCHANGE_RATE = 100;    -- X Tier 1 ancient currency -> 1 Tier 2, and so on.  Certain values may conflict with shop items.  Not designed to exceed 198.
RELIC_2ND_UPGRADE_WAIT_TIME = 604800; -- Default: 604800s = 1 RL week. - Wait time for 2nd relic upgrade (stage 2 -> stage 3) in seconds.
RELIC_3RD_UPGRADE_WAIT_TIME = 295200; -- Default: 295200s = 82 hours. - Wait time for 3rd relic upgrade (stage 3 -> stage 4) in seconds.
FREE_COP_DYNAMIS            = 0;      -- Default: 1 - (1 = enable 0 = disable) Authorize player to enter inside CoP Dynamis without completing CoP missions.

-- -------------------------------------------------------------------
--                                                                  --
--  QUEST/MISSION SPECIFIC SETTINGS                                 --
--                                                                  --
-- -------------------------------------------------------------------

WSNM_LEVEL          = 70;    -- Default: 70 - Minimum Level to get WSNM Quests
WSNM_SKILL_LEVEL    = 240;   -- Default: 240 - Minimum Weapon Combat Skill to get WSNM Quests
AF1_QUEST_LEVEL     = 40;    -- Default: 40 - Minimum level to start AF1 quest
AF2_QUEST_LEVEL     = 50;    -- Default: 50 - Minimum level to start AF2 quest
AF3_QUEST_LEVEL     = 50;    -- Default: 50 - Minimum level to start AF3 quest
AF1_FAME            = 20;    -- Default: 20 - Base fame for completing an AF1 quest
AF2_FAME            = 40;    -- Default: 40 - Base fame for completing an AF2 quest
AF3_FAME            = 60;    -- Default: 60 - Base fame for completing an AF3 quest
DEBUG_MODE          = 0;     -- Default: 0 - Set to 1 to activate auto-warping to the next location (only supported by certain missions / quests). -- NON FUNCTIONAL
QM_RESET_TIME       = 300;   -- Default: 300 - Time (in seconds) you have from killing ??? spawned mission NMs to click again and get key item, until ??? resets.
OldSchoolG2         = false; -- Default: false (true/false) - Set true to require the NMs for "Atop the Highest Mountains" be dead to get KI like before SE changed it.
FrigiciteDuration   = 30;    -- Default: 30 - Time (in seconds) you have from killing "Atop the Highest Mountains" NMs to click the "???" target.

-- -------------------------------------------------------------------
--                                                                  --
--      FIELDS/GROUNDS OF VALOR SETTINGS                            --
--                                                                  --
-- -------------------------------------------------------------------

TABS_RATE           = 1.000; -- Default: 1.00 - Multiplies tabs earned from FoV.
REGIME_WAIT         = 1;     -- Default: 1 - (0 - there is no wait time) - Make people wait till 00:00 game time as in retail.
FIELD_MANUALS       = 1;     -- Default: 1 (1 - On, 0 - Off) - Enables Fields of Valor manuals.
LOW_LEVEL_REGIME    = 0;     -- Default: 0 (1 - On, 0 - Off) - Allow people to kill regime targets even if they give no exp, allowing people to farm regime targets at 75 in low level areas.
GROUNDS_TOMES       = 1;     -- Default: 1 (1 - On, 0 - Off) - Enables Grounds of Valor tomes.

-- -------------------------------------------------------------------
--                                                                  --
--      JOB ABILITY/TRAIT SPECIFIC SETTINGS                         --
--                                                                  --
-- -------------------------------------------------------------------

SCAVENGE_RATE            = 0.1; -- Default: 0.1 (0.1 - 1.0) - The chance of obtaining an item when you use the Ranger job ability Scavenge.
STATUS_RESIST_MULTIPLIER = 10;  -- Default: 10 - Sets the strength of status resist traits.
CIRCLE_DURATION          = 300; -- Default: 300 - Sets the duration of circle effects, in seconds.
CIRCLE_KILLER_EFFECT     = 20;  -- Default: 20 - Intimidation percentage granted by circle effects. (made up number)
KILLER_EFFECT            = 10;  -- Default: 10 - Intimidation percentage from killer job traits.
SNARL_JUG_ONLY           = 1;   -- 1 = Snarl can only be used on jug pets (retail), 2 = can only be used on charmed mobs (reverse retail), any other value = can be used on any pet.

-- -------------------------------------------------------------------
--                                                                  --
--      SPELL SPECIFIC SETTINGS                                     --
--                                                                  --
-- -------------------------------------------------------------------

MILK_OVERWRITE                  = 1;     -- Default: 1 - (0, 1) - Set to 1 to allow Milk and Regen to overwrite each other.
JUICE_OVERWRITE                 = 1;     -- Default: 1 - (0, 1) - Set to 1 to allow Juice and Refresh to overwrite each other.
DIA_OVERWRITE                   = 1;     -- Default: 1 - (0, 1) - Set to 1 to allow Bio to overwrite same tier Dia.
BIO_OVERWRITE                   = 0;     -- Default: 0 - (0, 1) - Set to 1 to allow Dia to overwrite same tier Bio.
BARELEMENT_OVERWRITE            = 1;     -- Default: 1 - (0, 1) - Set to 1 to allow Barelement spells to overwrite each other (prevent stacking).
BARSTATUS_OVERWRITE             = 1;     -- Default: 1 - (0, 1) - Set to 1 to allow Barstatus spells to overwrite each other (prevent stacking).
BARD_SONG_LIMIT                 = 1;     -- Default: 1 - Maximum amount of songs from a single Bard that can be granted to a single target at once.  Set between 1 and 31.
BARD_INSTRUMENT_LIMIT           = 2;     -- Default: 2 - Maximum amount of songs from a single Bard with an instrument that can be granted to a single target at once.  Set between 2 and 32.
ENHANCING_SONG_DURATION         = 120;   -- Duration of enhancing bard songs such as Minuets, Ballads, etc.
STONESKIN_CAP                   = 350;   -- Default: 350 - Soft cap for HP absorbed by Stoneskin
BLINK_SHADOWS                   = 2;     -- Default: 2 - Number of shadows supplied by Blink spell
ENSPELL_DURATION                = 180;   -- Default: 180 - Duration of RDM en-spells
SPIKE_EFFECT_DURATION           = 180;   -- Default: 180 - The duration of RDM, BLM spikes effects (not Reprisal)
ELEMENTAL_DEBUFF_DURATION       = 120;   -- Default: 120 - Base duration of elemental debuffs
STORM_DURATION                  = 180;   -- Default: 180 - Duration of Scholar storm spells
KLIMAFORM_MACC                  = 30;    -- Default: 30 - Magic accuracy added by Klimaform. (30 is just a guess)
AQUAVEIL_INTERR_RATE            = 25;    -- Default: 25 - Percent spell interruption rate reduction from Aquaveil (see http://www.bluegartrls.com/forum/82143-spell-interruption-down-cap-aquaveil-tests.html)
ABSORB_SPELL_AMOUNT             = 8;     -- Default: 8 - (expected to be a multiple of 8) - How much of a stat gets absorbed by DRK absorb spells.
ABSORB_SPELL_TICK               = 9;     -- Default: 9 - Duration of 1 absorb spell tick
SNEAK_INVIS_DURATION_MULTIPLIER = 1;     -- Default: 1 - (MUST BE INTEGER) - Multiplies duration of sneak/invis/deodorize to reduce player torture.
USE_OLD_CURE_FORMULA            = false; -- Default: false (true/false) - If true, uses older cure formula (3*MND + VIT + 3*(healing skill/5)) // cure 6 will use the newer formula

-- -------------------------------------------------------------------
--                                                                  --
--      MISC                                                        --
--                                                                  --
-- -------------------------------------------------------------------

HOMEPOINT_HEAL                  = 0;     -- Default: 0 - (0, 1) - Set to 1 if you want Home Points to heal you like in single-player Final Fantasy games.
HOMEPOINT_TELEPORT              = 0;     -- Default: 0 - (0, 1) - Set to 1 if you want the Home Point Teleport system available.
BrassDoor4Torches               = false; -- Set true to require all 4 torches be lit to open the Brass Door at top of Castle Oztroya
BrassDoorTorchTime              = 35;    -- Default: 35 - Time in seconds that the torches remain lit for the Brass Door at top of Castle Oztroja.
LANTERNS_STAY_LIT               = 1200;  -- Default: 1200 - Time in seconds that lanterns in the Den of Rancor stay lit.
RIVERNE_PORTERS                 = 120;   -- Default: 120 - Time in seconds that Unstable Displacements in Cape Riverne stay open after trading a scale.
ENABLE_COP_ZONE_CAP             = 0;     -- Default: 1 (0 - off, 1 - on) - Enable or disable level cap in previously restricted CoP zones.
TIMEZONE_OFFSET                 = 9.0;   -- Default: 9.0 (JST) - Offset from UTC used to determine when "JP Midnight" is for the server.
ALLOW_MULTIPLE_EXP_RINGS        = 0;     -- Default: 0 - (0, 1) - Set to 1 to remove ownership restrictions on the Chariot/Empress/Emperor Band trio.
BYPASS_EXP_RING_ONE_PER_WEEK    = 0;     -- Default: 0 - (0, 1) - Set to 1 to bypass the limit of one ring per Conquest Tally Week.
NUMBER_OF_DM_EARRINGS           = 1;     -- Default: 1 - Number of earrings players can simultaneously own from Divine Might before scripts start blocking them.
WILDCAT_WARP_ENABLE             = false; -- Default: false - (true, false) - Set to true to enable the warps to Aht Urgan Whitegate without meeting the normal quest and mission requirements.
WILDCAT_COMPLETED               = false; -- Default: false - (true, false) - Set to true if you want all new players to start with all 4 Lure of the Wildcat quest completed.
TRAVEL_SKIP                     = 0;     -- Default: 0 (0-99999999) - Zero is disabled. Otherwise, players can skip airship/boat rides by trading this much gil to the NPC that tracks arrival/departure.

-- -------------------------------------------------------------------
--                                                                  --
--      LIMBUS                                                      --
--                                                                  --
-- -------------------------------------------------------------------

BETWEEN_2COSMOCLEANSE_WAIT_TIME = 3;     -- Default: 3 - Days between 2 Limbus keyitems.
DIMENSIONAL_PORTAL_UNLOCK       = false; -- Default: false - (true, false) - Set to true if you want to use the crag entrances to sea for Limbus without the keyitem or missions required

-- -------------------------------------------------------------------
--                                                                  --
--      ASSAULT                                                     --
--                                                                  --
-- -------------------------------------------------------------------

MAX_TAGS = 3; -- Default: 3 - Maximum number of tags character can have.

-- -------------------------------------------------------------------
--                                                                  --
--      Abyssea                                                     --
--                                                                  --
-- -------------------------------------------------------------------

ABYSSEA_MIN_LV  = 30;    -- Default: 30 - (retail) - Sets minimum level allowed to Enter any Abyssea zone.
VISITANT_BONUS  = 1.00;  -- Default: 1.00 - (retail) - Multiplies the base time value of each Traverser Stone.
VISITANT_SYSTEM = false; -- Default: temporarily false - (true, false) - Set to false if want to fully disable Traverser stone related requirements and zone ejection.
FREE_CONFLUX    = false; -- Default: false - (true, false) - Set to true if you want Veridical Conflux activation/teleportation to cost 0 cruor.
ALL_CONFLUX     = false; -- Default: false - (true, false) - Set to true if you want all new players to start with all Veridical Conflux warps.

-- -------------------------------------------------------------------
--                                                                  --
--      CELEBRATIONS - EVENTS   Give items related to it.           --
--                                                                  --
--  These need a shitload of work for all of them to be complete.   --
--                                                                  --
-- -------------------------------------------------------------------

-- -------------------------------------------------------------------
--                                                                  --
--      Mog Tablets                                                 --
--                                                                  --
-- -------------------------------------------------------------------

EXPLORER_MOOGLE          = 1;  -- Enables Explorer Moogle teleports
EXPLORER_MOOGLE_LEVELCAP = 20; -- Default: 20 - Retail

-- -------------------------------------------------------------------
--                                                                  --
--      Summerfest / Sunbreeze Festival                             --
--                                                                  --
-- -------------------------------------------------------------------

SUMMERFEST_2004 = 0; -- Default: 0 - (0, 1) - Set to 1 to give starting characters Far East attire from 2004.  Ex: Onoko Yukata
JINX_MODE_2005  = 0; -- Default: 0 - (0, 1) - Set to 1 to give starting characters swimsuits from 2005.  Ex: Hume Top
JINX_MODE_2008  = 0; -- Default: 0 - (0, 1) - Set to 1 to give starting characters swimsuits from 2008.  Ex: Custom Top
JINX_MODE_2012  = 0; -- Default: 0 - (0, 1) - Set to 1 to give starting characters swimsuits from 2012.  Ex: Marine Top
SUNBREEZE_2009  = 0; -- Default: 0 - (0, 1) - Set to 1 to give starting characters Far East attire from 2009.  Ex: Otokogusa Yukata
SUNBREEZE_2011  = 0; -- Default: 0 - (0, 1) - Set to 1 to give starting characters Far East attire from 2011.  Ex: Hikogami Yukata

-- -------------------------------------------------------------------
--                                                                  --
--      Starlight Celebration                                       --
--                                                                  --
-- -------------------------------------------------------------------

CHRISTMAS = 0; -- Default: 0 - (0, 1) - Set to 1 to give starting characters Christmas attire.

-- -------------------------------------------------------------------
--                                                                  --
--      Harvest Festival                                            --
--                                                                  --
-- -------------------------------------------------------------------

HALLOWEEN            = 0; -- Default: 0 - (0, 1) - Set to 1 to give starting characters Halloween items (Does not start event).
HALLOWEEN_2005       = 0; -- Default: 1 - (0, 1) - Set to 1 to Enable the 2005 version of Harvest Festival, will start on Oct. 20 and end Nov. 1.
HALLOWEEN_YEAR_ROUND = 0; -- Default: 0 - (0, 1) - Set to 1 to have Harvest Festival initialize outside of normal times.

-- -------------------------------------------------------------------
--                                                                  --
--      Happy New Year                                              --
--                                                                  --
-- -------------------------------------------------------------------

-- TODO

-- -------------------------------------------------------------------
--                                                                  --
--      Doll Festival                                               --
--                                                                  --
-- -------------------------------------------------------------------

-- TODO

-- -------------------------------------------------------------------
--                                                                  --
--      Valentione's Day                                            --
--                                                                  --
-- -------------------------------------------------------------------

-- TODO

-- -------------------------------------------------------------------
--                                                                  --
--      Egg Hunt Extravaganza                                       --
--                                                                  --
-- -------------------------------------------------------------------

-- TODO

-- -------------------------------------------------------------------
--                                                                  --
--      Feast of Swords                                             --
--                                                                  --
-- -------------------------------------------------------------------

-- TODO

-- -------------------------------------------------------------------
--                                                                  --
--      Adventurer Appreciation Campaign                            --
--                                                                  --
-- -------------------------------------------------------------------

-- TODO

-- -------------------------------------------------------------------
--                                                                  --
--      Blazing Buffaloes / Buffalo Blitz                           --
--                                                                  --
-- -------------------------------------------------------------------

-- TODO

-- -------------------------------------------------------------------
--                                                                  --
--      Expansion Promotional Events                                --
--                                                                  --
-- -------------------------------------------------------------------

-- TODO: Crystal War Revisited

-- -------------------------------------------------------------------
--                                                                  --
--      XI Years of Final Fantasy XI                                --
--                                                                  --
-- -------------------------------------------------------------------

-- TODO

-- -------------------------------------------------------------------
--                                                                  --
--      Return Home to Vana'diel Campaign                           --
--                                                                  --
-- -------------------------------------------------------------------

-- TODO

-- -------------------------------------------------------------------
--                                                                  --
--      Mog Bonanza                                                 --
--                                                                  --
-- -------------------------------------------------------------------

-- TODO

-- -------------------------------------------------------------------
--                                                                  --
--      VanaFest Golden Gobbiebag Giveaway                          --
--                                                                  --
-- -------------------------------------------------------------------

-- TODO

-- -------------------------------------------------------------------
--                                                                  --
--      DQXI/FF14 Crossover                                         --
--                                                                  --
-- -------------------------------------------------------------------

-- TODO

-- -------------------------------------------------------------------
--                                                                  --
--      Repeat Login Campaign                                       --
--                                                                  --
-- -------------------------------------------------------------------

-- TODO

-- -------------------------------------------------------------------
--                                                                  --
--      Double EXP Campaigns                                        --
--                                                                  --
-- -------------------------------------------------------------------

-- TODO