---------------------------------------------------------------------------------------------------
-- func: @addvalor <amount> <player>
-- auth: Compmike, modified by Forgottenandlost
-- desc: Adds the specified amount of Valor Points to the player
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "is"
};

function onTrigger(player, amount, target)

    if (amount == nil) then
        player:PrintToPlayer( "You must enter a valid amount." );
        player:PrintToPlayer( "@addvalor <amount> <player>" );
        return;
    end

    if (target == nil) then
        player:addValorPoint(amount);
    else
        local targ = GetPlayerByName(target);
        if (targ ~= nil) then
            targ:addValorPoint(amount);
        else
            player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) );
            player:PrintToPlayer( "@addvalor <amount> <player>" );
        end
    end
end;