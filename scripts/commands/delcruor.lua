---------------------------------------------------------------------------------------------------
-- func: @delcruor <amount> <player>
-- auth: Forgottenandlost, but mostly copied from Compmike :)
-- desc: Deletes the specified amount of Cruor from the player
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "is"
};

function onTrigger(player,amount,target)

    if (amount == nil) then
        player:PrintToPlayer( "You must enter a valid amount." );
        player:PrintToPlayer( "@delcruor <amount> <player>" );
        return;
    end

    if (target == nil) then
        player:delCruor(amount);
    else
        local targ = GetPlayerByName(target);
        if (targ ~= nil) then
            targ:delCruor(amount);
        else
            player:PrintToPlayer( "You must enter a valid Player's Name." );
            player:PrintToPlayer( "@delcruor <amount> <player>" );
        end
    end
end;