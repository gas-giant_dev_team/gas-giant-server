--------------------------------------------------------------
-- func: @shop
-- auth: forgottenandlost
-- desc: opens a custom shop anywhere in the world
--------------------------------------------------------------

cmdprops =
{
    permission = 4,
    parameters = "i"
};

function onTrigger(player,page)
    if (page == 0 or page == nil) then
        player:PrintToPlayer( "1: Crystals, 2: Misc");
    elseif (page == 1) then
        local stock_1 =
        {
            4238,   5000,       -- HQ Fire Crystal
            4239,   5000,       -- HQ Ice Crystal
            4240,   5000,       -- HQ Wind Crystal
            4241,   5000,       -- HQ Earth Crystal
            4242,   5000,       -- HQ Lightning Crystal
            4243,   5000,       -- HQ Water Crystal
            4244,   5000,       -- HQ Light Crystal
            4245,   5000        -- HQ Dark Crystal
        };
        showShop(player, STATIC, stock_1);

    elseif ( page == 2) then
        local stock_2 =
        {
        4153,   2500,       -- Bottle of Antacid
        5314,   5000,       -- Toolbag (Shihei)
        5440,   25000,      -- Dusty Wing
        5853,   500000,     -- Flask of Primeval Brew
        1126,   10000,      -- Beastmen's Seal
        1127,   25000,      -- Kindred Seal
        2955,   30000,      -- Kindred Crest
        2956,   60000,      -- High Kindred Crest
        2957,   90000       -- Sacred Kindred Crest
        };
        showShop(player, STATIC, stock_2);

    else
        player:PrintToPlayer( string.format( "Page %i not found.", page ) );
    end
end;