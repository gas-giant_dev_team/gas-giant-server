---------------------------------------------------------------------------------------------------
-- func: @delvstone <amount> <player>
-- auth: Forgottenandlost
-- desc: Deletes the specified amount of Voidstones from the player's stock
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "is"
};

function onTrigger(player, amount, target)

    if (amount == nil) then
        player:PrintToPlayer( "You must enter a valid point amount." );
        player:PrintToPlayer( "@delvstone <amount> <player>" );
        return;
    end

    if (target == nil) then
        player:delVstone(amount);
    else
        local targ = GetPlayerByName(target);
        if (targ ~= nil) then
            targ:delVstone(amount);
        else
            player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) );
            player:PrintToPlayer( "@delvstone <amount> <player>" );
        end
    end
end;