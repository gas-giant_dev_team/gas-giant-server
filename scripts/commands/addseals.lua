---------------------------------------------------------------------------------------------------
-- func: @addSeals <amount> <type> <player>
-- auth: Compmike, modified by Forgottenandlost
-- desc: Adds the specified amount and type of Seals to the player
--       0 - Beastman's Seals, 1 - Kindred Seals, 2 - Kindred Crests
--       3 - High Kindred Crests, 4 - Sacred Kindred Crests
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "iis"
};

function onTrigger(player,amount,type,target)

	if (amount == nil) then
		player:PrintToPlayer( "You must enter a valid point amount." );
		player:PrintToPlayer( "@addseals <amount> <type> <player> " );
		return;
	end

	if (type == nil) then
		player:PrintToPlayer( "You must enter a valid Seal Type." );
		player:PrintToPlayer( "@addseals <amount> <type> <player>" );
		player:PrintToPlayer( "Types: 0 - Beastman's, 1 - Kindred, 2 - Kindred Crests" );
		player:PrintToPlayer( "Types: 3 - High Kindred Crests, 4 - Sacred Kindred Crests" );
		return;
	end

	if (target == nil) then
		player:addSeals(amount, type);
	else
		local targ = GetPlayerByName(target);
		if (targ ~= nil) then
			targ:addSeals(amount, type);
		else
			player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) );
			player:PrintToPlayer( "@addseals <amount> <type> <player>" );
		end
	end
end;