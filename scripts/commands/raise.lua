-----------------------------------------------------------------------
-- func: @raise <power> <player>
-- auth: Forgottenandlost
-- desc: Sends raise menu to GM or target player.
-----------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "is"
};

function onTrigger(player,power,target)
    local pow = 0;

    if (power == nil or power >=3) then
        pow = 3;
    elseif (power <=1) then
            pow = 1;
    else
        pow = power;
    end

    if (target == nil) then
        player:sendRaise(pow);
    else
        local targ = GetPlayerByName(target);
        if (targ ~= nil) then
            targ:sendRaise(pow);
        else
            player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) );
        end
    end
end;