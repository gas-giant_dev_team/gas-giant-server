---------------------------------------------------------------------------------------------------
-- func: @speed <value> <player>
-- auth: <Unknown>,
-- modified by: Gravenet, Katrinka, Forgottenandlost
-- desc: Sets the GMs or target players movement speed.
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "is"
};

function onTrigger(player,value,target)

    if (value == nil) then
        player:PrintToPlayer("You must enter a number for the speed value!");
        player:PrintToPlayer( "@speed <value> <player>" );
        return;
    end

    if (target == nil) then
        player:speed(value);
    else
        local targ = GetPlayerByName(target);
        if (targ ~= nil) then
            targ:speed(value);
        else
            player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) );
            player:PrintToPlayer( "@speed <value> <player>" );
        end
    end
end;