---------------------------------------------------------------------------------------------------
-- func: @completequest <logID> <questID> <player>
-- auth: <Unknown>, modified by Forgottenandlost
-- desc: Completes the given quest for the GM or target player.
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "iis"
};

function onTrigger(player, logId, questId, target)

    if (questId == nil or logId == nil) then
        player:PrintToPlayer( "You must enter a valid log ID and quest ID!" );
        player:PrintToPlayer( "@completequest <logID> <questID> <player>" );
        return;
    end

    if (target == nil) then
        player:completeQuest( logId, questId );
    else
        local targ = GetPlayerByName(target);
        if (targ ~= nil) then
            targ:completeQuest( logId, questId );
        else
            player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) );
            player:PrintToPlayer( "@completequest <logID> <questID> <player>" );
        end
    end
end;