---------------------------------------------------------------------------------------------------
-- func: @givexp <amount> <player>
-- auth: atom0s, modified by Forgottenandlost
-- desc: Gives the GM or target player experience points.
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "is"
};

function onTrigger(player, amount, target)

print( 'Exp amount: ' .. tostring( amount ) );

    if (amount == nil) then
        player:PrintToPlayer("You must enter a valid amount.");
        player:PrintToPlayer( "@givexp <amount> <player>" );
        return;
    end

    if (target == nil) then
        player:addExp(amount);
    else
        local targ = GetPlayerByName(target);
        if (targ ~= nil) then
            targ:addExp(amount);
        else
            player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) );
            player:PrintToPlayer( "givexp <amount> <player>" );
        end
    end
end;