---------------------------------------------------------------------------------------------------
-- func: @delmission <logID> <missionID> <player>
-- auth: <Unknown>, modified by Forgottenandlost
-- desc: Deletes the given mission from the GM or target player.
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "iis"
};

function onTrigger(player, logId, missionId, target)

    if (missionId == nil or logId == nil) then
        player:PrintToPlayer( "You must enter a valid log id and mission id!" );
        player:PrintToPlayer( "@delmission <logID> <missionID> <player>" );
        return;
    end

    if (target == nil) then
        player:delMission( logId, missionId );
    else
        local targ = GetPlayerByName(target);
        if (targ ~= nil) then
            targ:delMission( logId, missionId );
        else
            player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) );
            player:PrintToPlayer( "@delmission <logID> <missionID> <player>" );
        end
    end
end;