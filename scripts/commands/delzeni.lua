---------------------------------------------------------------------------------------------------
-- func: @delzeni <amount> <player>
-- auth: Compmike, modified by Forgottenandlost
-- desc: Deletes the specified amount of Zeni to the player
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "is"
};

function onTrigger(player, amount, target)

    if (amount == nil) then
        player:PrintToPlayer( "You must enter a valid point amount." );
        player:PrintToPlayer( "@delzeni <amount> <player>" );
        return;
    end

    if (target == nil) then
        player:delZeni(amount);
    else
        local targ = GetPlayerByName(target);
        if (targ ~= nil) then
            targ:delZeni(amount);
        else
            player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) );
            player:PrintToPlayer( "@delzeni <amount> <player>" );
        end
    end
end;