---------------------------------------------------------------------------------------------------
-- func: @givels <linkshell name> <player>
-- auth: forgottenandlost
-- desc: remotely gives the specified linkshell to the target player.
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "ss"
};

function onTrigger(player,ls,target)
    -- Load needed text ids for players current zone..
    local TextIDs = "scripts/zones/" .. player:getZoneName() .. "/TextIDs";
    package.loaded[TextIDs] = nil;
    require(TextIDs); 

    -- Yeah yeah I made an huge if/else block instead of using returns. >.>
    if (ls == nil) then
        player:PrintToPlayer( "You specify the LS by name." );
        player:PrintToPlayer( "@givels <linkshell name> <player>" );
    else
        if (isValidLS(ls) == true) then
            if (target == nil) then
                if (player:getFreeSlotsCount() >= 1) then
                    player:addLSpearl(ls);
                    player:messageSpecial( ITEM_OBTAINED, 515 );
                else
                    player:messageSpecial( ITEM_CANNOT_BE_OBTAINED, 515 );
                end
            else
                local targ = GetPlayerByName(target);
                if (targ ~= nil) then
                    if (player:getFreeSlotsCount() >= 1) then
                        targ:addLSpearl(ls);
                        targ:messageSpecial( ITEM_OBTAINED, 515 );
                    else
                        targ:messageSpecial( ITEM_CANNOT_BE_OBTAINED, 515 );
                        player:PrintToPlayer( string.format( "Player '%s' doesn't have a free slot for the item.", target ) );
                    end
                else
                    player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) );
                    player:PrintToPlayer( "@givels <linkshell name> <player>" );
                end
            end
        else
            player:PrintToPlayer( string.format( "LS named '%s' not found!", ls ) );
            player:PrintToPlayer( "@givels <linkshell name> <player>" );
        end
    end
end;