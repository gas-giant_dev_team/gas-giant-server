---------------------------------------------------------------------------------------------------
-- func: @reloadglobals
-- auth: Forgottenandlost
-- desc: Attempts to reload multiple global lua's without a restart.
--
-- Not all globals can be safely reloaded from this command.
-- New ones will be added only after very thorough testing.
-- This command expect the user to know wtf they are doing,
-- but has a default permission lv of 4 so that helpers or
-- less experienced GMs do not mistakenly misuse it.
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 4,
    parameters = ""
};

function onTrigger(player)
    -- DO NOT place player.lua into this list. You've been warned.
    package.loaded["scripts/globals/settings"] = nil;
    require("scripts/globals/settings");
    package.loaded["scripts/globals/status"] = nil;
    require("scripts/globals/status");
    package.loaded["scripts/globals/weaponskills"] = nil;
    require("scripts/globals/weaponskills");
    player:PrintToPlayer( "Global lua's have been reloaded." );
end;